<?php


$app->get('/api/level1/invoices', function ($request,  $response, array $args) {
    $sql = "select * from AR_AGING_LEVEL1  where 1 = 1  ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->execute();
    $invoices = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($invoices));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/level2/invoices', function ($request,  $response, array $args) {
    $sql = "select * from AR_AGING_L2_COMMS  where 1 = 1  ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->execute();
    $invoices = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($invoices));
    return $response
        ->withHeader('Content-type', 'application/json');
});


$app->get('/api/level3/invoices', function ($request,  $response, array $args) {
    $sql = "select * from AR_AGING_L3_COMMS  where 1 = 1  ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->execute();
    $invoices = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $newData = [];
    foreach ($invoices as  $value) {

        $value['RC'] = htmlspecialchars($value['RC'], ENT_QUOTES | ENT_IGNORE);
        $value['RC'] = htmlspecialchars($value['RC'], ENT_QUOTES | ENT_IGNORE);
        array_push($newData, $value);
    }


    $response->write(json_encode($invoices));
    return $response
        ->withHeader('Content-type', 'application/json');
});


$app->get('/api/level4/invoices[/{account_no}[/{resort}]]', function ($request,  $response, array $args) {
    $sql = "select * from AR_AGING_L4_COMMS  where 1 = 1  ";
    if ($args['resort']) {
        $sql = $sql .  "  and  resort = :resort ";
    }
    if ($args['account_no']) {
        $sql = $sql .  "  and  account_no = :account_no ";
    }
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    if ($args['resort']) {
        $stmt->bindParam(':resort', $args['resort'], PDO::PARAM_STR);
    }
    if ($args['account_no']) {
        $stmt->bindParam(':account_no', $args['account_no'], PDO::PARAM_STR);
    }
    $stmt->execute();
    $invoices = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($invoices));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/level4/ownerinvoices/{eid}', function ($request,  $response, array $args) {
    $sql = "select * from AR_AGING_L4_COMMS  where 1 = 1  ";
    if ($args['eid'] && $args['eid'] !== 'ALL') {
        $sql = $sql .  "  and  eid = :eid ";
    }
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::ATTR_PREFETCH => 1000));
    if ($args['eid']) {
        $stmt->bindParam(':eid', $args['eid'], PDO::PARAM_STR);
    }
    $stmt->execute();
    $invoices = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($invoices));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/level4/ownerinvoices_new/{eid}', function ($request,  $response, array $args) {
    $sql = "select * from AR_AGING_L4_COMMS_new  where 1 = 1  ";
    if ($args['eid'] && $args['eid'] !== 'ALL') {
        $sql = $sql .  "  and  eid = :eid ";
    }
    // $sql = $sql .  "  and  resort = :resort ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::ATTR_PREFETCH => 1000));
    if ($args['eid']) {
        $stmt->bindParam(':eid', $args['eid'], PDO::PARAM_STR);
    }
    // $stmt->bindParam(':resort', $args['resort'], PDO::PARAM_STR);
    $stmt->execute();
    $invoices = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($invoices));
    return $response
        ->withHeader('Content-type', 'application/json');
});


$app->get('/api/level4/ownerinvoicesTEST/{eid}', function ($request,  $response, array $args) {
    $time = microtime(TRUE);
    $mem = memory_get_usage();
    $sql = "select * from AR_AGING_L4_COMMS_new  where 1 = 1  ";
    // echo $sql;
    if ($args['eid'] && $args['eid'] !== 'ALL') {
        $sql = $sql .  "  and  eid = :eid ";
    }
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY, PDO::ATTR_PREFETCH => 1000));
    if ($args['eid']) {
        $stmt->bindParam(':eid', $args['eid'], PDO::PARAM_STR);
    }
    $stmt->execute();
    // echo "fetch invoices";
    $invoices = [];
    $count = 0;

    $invoices = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // var_dump($invoices);
    // while ($inv = $stmt->fetch(PDO::FETCH_ASSOC)) {

    //     array_push($invoices,$inv);

    // }
    // var_dump(sizeof($invoices));
    // print_r($invoices);
    // print_r(array('memory' => (memory_get_usage() - $mem) / (1024 * 1024), 'seconds' => microtime(TRUE) - $time));

    // $invoices = $stmt->fetch(PDO::FETCH_ASSOC);

    // $response->write(json_encode($invoices));

    // $json = json_encode($invoices);
    print_r(array('memory' => (memory_get_usage() - $mem) / (1024 * 1024), 'seconds' => microtime(TRUE) - $time));
    // echo $response;
    var_dump($invoices);
    print_r(array('memory' => (memory_get_usage() - $mem) / (1024 * 1024), 'seconds' => microtime(TRUE) - $time));
    // echo $json;


    // if ($json)
    //     echo $json;
    // else
    //     echo json_last_error_msg();

    // return $response
    //     ->withHeader('Content-type', 'application/json')
    //     ;
    echo "Peak: " . (memory_get_peak_usage() / 1024 / 1024) . "<br>";
});


$app->get('/api/mapping[/{account_no}[/{resort}]]', function ($request,  $response, array $args) {
    // $sql = "select * from AR_ACCOUNT_MAP  where 1 = 1  ";
    $sql = "select am.* ,aum.eid from ar_account_map am
            left outer join ar_account_user_map aum
            on (am.account_no_to = aum.account_no)
            where 1 = 1 ";
    if ($args['resort']) {
        $sql = $sql .  "  and  am.resort = :resort ";
    }
    if ($args['account_no']) {
        $sql = $sql .  "  and  am.account_no_from = :account_no ";
    }
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    if ($args['resort']) {
        $stmt->bindParam(':resort', $args['resort'], PDO::PARAM_STR);
    }
    if ($args['account_no']) {
        $stmt->bindParam(':account_no', $args['account_no'], PDO::PARAM_STR);
    }
    $stmt->execute();
    $mapping = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($mapping));
    return $response
        ->withHeader('Content-type', 'application/json');
});


$app->get('/api/mappingV3[/{account_no}[/{resort}]]', function ($request,  $response, array $args) {
    // $sql = "select * from AR_ACCOUNT_MAP  where 1 = 1  ";
    $sql = "SELECT * FROM AR_ACCOUNT_MAP_VW
            where 1 = 1 ";
    if ($args['resort']) {
        $sql = $sql .  "  and  resort = :resort ";
    }
    if ($args['account_no']) {
        $sql = $sql .  "  and  account_no_from = :account_no ";
    }
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    if ($args['resort']) {
        $stmt->bindParam(':resort', $args['resort'], PDO::PARAM_STR);
    }
    if ($args['account_no']) {
        $stmt->bindParam(':account_no', $args['account_no'], PDO::PARAM_STR);
    }
    $stmt->execute();
    $mapping = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($mapping));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/mappingV4[/{account_no}[/{resort}]]', function ($request,  $response, array $args) {
    // $sql = "select * from AR_ACCOUNT_MAP  where 1 = 1  ";
    $sql = "SELECT * FROM AR_ACCOUNT_MAP_VW
            where 1 = 1 ";
    if ($args['resort']) {
        $sql = $sql .  "  and  resort = :resort ";
    }
    if ($args['account_no']) {
        $sql = $sql .  "  and  account_no_from = :account_no ";
    }
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    if ($args['resort']) {
        $stmt->bindParam(':resort', $args['resort'], PDO::PARAM_STR);
    }
    if ($args['account_no']) {
        $stmt->bindParam(':account_no', $args['account_no'], PDO::PARAM_STR);
    }
    $stmt->execute();
    $mapping = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($mapping));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/owners[/{account_no}]', function ($request,  $response, array $args) {
    $sql = "select * from ar_account_user_map where 1 = 1 ";
    if ($args['account_no']) {
        $sql = $sql .  "  and  am.account_no_from = :account_no ";
    }
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    if ($args['account_no']) {
        $stmt->bindParam(':account_no', $args['account_no'], PDO::PARAM_STR);
    }
    $stmt->execute();
    $mapping = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($mapping));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->post('/api/eid_perf', function ($request,  $response, array $args) {
    $data = ($request->getParsedBody());

    if (strlen($data['filter_from_date']) == 0) {
        $data['filter_from_date'] = '2000-01-01';
    }
    $date = new DateTime(date("Y-m-d"));
    $today = new DateTime(date("Y-m-d"));
    $todayStr = $today->format('Y-m-d');
    if (strlen($data['filter_to_date']) == 0) {
        $data['filter_to_date'] = $todayStr;
    }

    $a = '';
    $b = '';
    $c = '';
    if ($data['a'] == 'a_actions' || $data['a'] == 'a_dispatched_invoices' || $data['a'] == 'a_followup_invoices') {

        $a =
            $a . "SELECT EID DIM,
        COUNT(INVOICE_NO) FACT_COUNT FROM (
        select AC.account_no,UM.EID,ac.resort,AC.invoice_no,count(*) from AR_COMMENTS AC
        LEFT OUTER JOIN AR_ACCOUNT_USER_MAP UM
                    ON (AC.ACCOUNT_NO = UM.ACCOUNT_NO)
        where AC.INVOICE_NO is not null ";

        if ($data['a'] == 'a_followup_invoices') {
            $a =
                $a . " AND (AC.CALL_OR_EMAIL_DATE IS NOT NULL
                    OR FOLLOW_UP_10_DAY_DATE IS NOT NULL)";
        }
        if ($data['a'] == 'a_dispatched_invoices') {
            $a =
                $a . " AND AC.DISPATCH_DATE IS NOT NULL";
        }

        $a =
            $a . " AND (AC.ACCOUNT_NO,AC.RESORT,AC.INVOICE_NO) IN
        (SELECT ACCOUNT_NO,RESORT,INVOICE_NO FROM (
        (SELECT ACCOUNT_NO,RESORT,INVOICE_NO,COUNT(*)
        FROM AR_INVOICES_ALL_MV
        WHERE TO_CHAR(NVL(CHECK_OUT_DATE,POST_DATE),'YYYY-MM-DD') BETWEEN :filter_from_date AND :filter_to_date ";
        if (($data['b'] == 'b_open_invoices' && $data['c'] == 'c_null') || $data['c'] == 'c_open_invoices') {
            $a =
                $a . " AND TOTAL <> 0 ";
        }
        if (($data['b'] == 'b_overdue_invoices_30' && $data['c'] == 'c_null') || $data['c'] == 'c_overdue_invoices_30') {
            $a =
                $a . " AND INVOICE_AGE > 30 ";
        }
        if (($data['b'] == 'b_overdue_invoices_60' && $data['c'] == 'c_null') || $data['c'] == 'c_overdue_invoices_60') {
            $a =
                $a . " AND INVOICE_AGE > 60 ";
        }
        $a =
            $a . " GROUP BY ACCOUNT_NO,RESORT,INVOICE_NO)))
        group by AC.account_no,UM.EID,ac.resort,invoice_no
        )
        GROUP BY
        EID ";
    }
    if ($data['a'] == 'a_invoices' || $data['a']  == 'a_open_invoices' || $data['a'] == 'a_overdue_invoices_30' || $data['a'] == 'a_overdue_invoices_60') {
        $a =
            $a . "SELECT EID DIM,
        COUNT(INVOICE_NO) FACT_COUNT FROM (
        SELECT EID,RESORT,INVOICE_NO,COUNT(*)
        FROM AR_INVOICES_ALL_MV
        WHERE TO_CHAR(NVL(CHECK_OUT_DATE,POST_DATE),'YYYY-MM-DD') BETWEEN :filter_from_date AND :filter_to_date";

        if ($data['a'] == 'a_open_invoices') {
            $a =
                $a . " AND TOTAL <> 0 ";
        }
        if ($data['a'] == 'a_overdue_invoices_30') {
            $a =
                $a . " AND INVOICE_AGE > 30 ";
        }
        if ($data['a'] == 'a_overdue_invoices_60') {
            $a =
                $a . " AND INVOICE_AGE > 60 ";
        }
        $a =
            $a . " GROUP BY EID,RESORT,INVOICE_NO
        )
        GROUP BY
        EID";
    }

    if ($data['b'] == 'b_actions' || $data['b'] == 'b_followup_invoices' || $data['b'] == 'b_dispatched_invoices') {

        $b =
            $b . "SELECT EID DIM,
        COUNT(INVOICE_NO) FACT_COUNT FROM (
        select AC.account_no,UM.EID,ac.resort,AC.invoice_no,count(*) from AR_COMMENTS AC
        LEFT OUTER JOIN AR_ACCOUNT_USER_MAP UM
                    ON (AC.ACCOUNT_NO = UM.ACCOUNT_NO)
        where AC.INVOICE_NO is not null ";

        if ($data['b'] == 'b_followup_invoices') {
            $b =
                $b . " AND (AC.CALL_OR_EMAIL_DATE IS NOT NULL
                    OR FOLLOW_UP_10_DAY_DATE IS NOT NULL)";
        }
        if ($data['b'] == 'b_dispatched_invoices') {
            $b =
                $b . " AND AC.DISPATCH_DATE IS NOT NULL";
        }

        $b =
            $b . " AND (AC.ACCOUNT_NO,AC.RESORT,AC.INVOICE_NO) IN
        (SELECT ACCOUNT_NO,RESORT,INVOICE_NO FROM (
        (SELECT ACCOUNT_NO,RESORT,INVOICE_NO,COUNT(*)
        FROM AR_INVOICES_ALL_MV
        WHERE TO_CHAR(NVL(CHECK_OUT_DATE,POST_DATE),'YYYY-MM-DD') BETWEEN :filter_from_date AND :filter_to_date ";
        if (($data['a'] == 'a_open_invoices' && $data['c'] == 'c_null') || $data['c'] == 'c_open_invoices') {
            $b =
                $b . " AND TOTAL <> 0 ";
        }
        if (($data['a'] == 'a_overdue_invoices_30' && $data['c'] == 'c_null') || $data['c'] == 'c_overdue_invoices_30') {
            $a =
                $a . " AND INVOICE_AGE > 30 ";
        }
        if (($data['a'] == 'a_overdue_invoices_60' && $data['c'] == 'c_null') || $data['c'] == 'c_overdue_invoices_60') {
            $a =
                $a . " AND INVOICE_AGE > 60 ";
        }
        $b =
            $b . " GROUP BY ACCOUNT_NO,RESORT,INVOICE_NO)))
        group by AC.account_no,UM.EID,ac.resort,invoice_no
        )
        GROUP BY
        EID ";
    }

    if ($data['b'] == 'b_invoices' || $data['b'] == 'b_open_invoices'  || $data['b'] == 'b_overdue_invoices_30' || $data['b'] == 'b_overdue_invoices_60') {
        $b =
            $b . "SELECT EID DIM,
        COUNT(INVOICE_NO) FACT_COUNT FROM (
        SELECT EID,RESORT,INVOICE_NO,COUNT(*)
        FROM AR_INVOICES_ALL_MV
        WHERE TO_CHAR(NVL(CHECK_OUT_DATE,POST_DATE),'YYYY-MM-DD') BETWEEN :filter_from_date AND :filter_to_date";

        if ($data['b'] == 'b_open_invoices') {
            $b =
                $b . " AND TOTAL <> 0 ";
        }
        if ($data['b'] == 'b_overdue_invoices_30') {
            $b =
                $b . " AND INVOICE_AGE > 30 ";
        }
        if ($data['b'] == 'b_overdue_invoices_60') {
            $b =
                $b . " AND INVOICE_AGE > 60 ";
        }
        $b =
            $b . " GROUP BY EID,RESORT,INVOICE_NO
        )
        GROUP BY
        EID";
    }

    //fact c
    if ($data['c'] != 'c_null') {
        if ($data['c'] == 'c_actions' || $data['c'] == 'c_followup_invoices' || $data['c'] == 'c_dispatched_invoices') {

            $c =
                $c . "SELECT EID DIM,
            COUNT(INVOICE_NO) FACT_COUNT FROM (
            select AC.account_no,UM.EID,ac.resort,AC.invoice_no,count(*) from AR_COMMENTS AC
            LEFT OUTER JOIN AR_ACCOUNT_USER_MAP UM
                        ON (AC.ACCOUNT_NO = UM.ACCOUNT_NO)
            where AC.INVOICE_NO is not null ";

            if ($data['c'] == 'c_followup_invoices') {
                $c =
                    $c . " AND (AC.CALL_OR_EMAIL_DATE IS NOT NULL
                        OR FOLLOW_UP_10_DAY_DATE IS NOT NULL)";
            }
            if ($data['c'] == 'c_dispatched_invoices') {
                $c =
                    $c . " AND AC.DISPATCH_DATE IS NOT NULL";
            }

            $c =
                $c . " AND (AC.ACCOUNT_NO,AC.RESORT,AC.INVOICE_NO) IN
            (SELECT ACCOUNT_NO,RESORT,INVOICE_NO FROM (
            (SELECT ACCOUNT_NO,RESORT,INVOICE_NO,COUNT(*)
            FROM AR_INVOICES_ALL_MV
            WHERE TO_CHAR(NVL(CHECK_OUT_DATE,POST_DATE),'YYYY-MM-DD') BETWEEN :filter_from_date AND :filter_to_date ";
            if (($data['a'] == 'a_open_invoices') || ($data['b'] == 'b_open_invoices')) {
                $c =
                    $c . " AND TOTAL <> 0 ";
            }
            if ($data['b'] == 'b_overdue_invoices_30' || $data['a'] == 'a_overdue_invoices_30') {
                $a =
                    $a . " AND INVOICE_AGE > 30 ";
            }
            if ($data['b'] == 'b_overdue_invoices_60' || $data['a'] == 'a_overdue_invoices_60') {
                $a =
                    $a . " AND INVOICE_AGE > 60 ";
            }
            $c =
                $c . " GROUP BY ACCOUNT_NO,RESORT,INVOICE_NO)))
            group by AC.account_no,UM.EID,ac.resort,invoice_no
            )
            GROUP BY
            EID ";
        }

        if ($data['c'] == 'c_invoices' || $data['c'] == 'c_open_invoices'  || $data['c'] == 'c_overdue_invoices_30' || $data['c'] == 'c_overdue_invoices_60') {
            $c =
                $c . "SELECT EID DIM,
            COUNT(INVOICE_NO) FACT_COUNT FROM (
            SELECT EID,RESORT,INVOICE_NO,COUNT(*)
            FROM AR_INVOICES_ALL_MV
            WHERE TO_CHAR(NVL(CHECK_OUT_DATE,POST_DATE),'YYYY-MM-DD') BETWEEN :filter_from_date AND :filter_to_date";

            if ($data['c'] == 'c_open_invoices') {
                $c =
                    $c . " AND TOTAL <> 0 ";
            }
            if ($data['c'] == 'c_overdue_invoices_30') {
                $c =
                    $c . " AND INVOICE_AGE > 30 ";
            }
            if ($data['c'] == 'c_overdue_invoices_60') {
                $c =
                    $c . " AND INVOICE_AGE > 60 ";
            }

            $c =
                $c . " GROUP BY EID,RESORT,INVOICE_NO
            )
            GROUP BY
            EID";
        }
    }




    $sql = "select a.DIM,a.FACT_COUNT FACT1, b.FACT_COUNT FACT2 ,";

    if ($data['c'] != 'c_null') {
        $sql = $sql . " c.FACT_COUNT FACT3 , ";
    }


    if ($data['c'] != 'c_null') {
        $sql = $sql . "round((a.FACT_COUNT/ c.FACT_COUNT)*100,2) PERCENTAGE, round((b.FACT_COUNT/ c.FACT_COUNT)*100,2) PERCENTAGE2 ";
    } else {
        $sql = $sql . "round((a.FACT_COUNT/ b.FACT_COUNT)*100,2) PERCENTAGE ";
    }
    $sql = $sql . " from
        ( $a
        ) a
    INNER JOIN (
        $b) b
    on (a.DIM = b.DIM)
    ";
    if ($data['c'] != 'c_null') {
        $sql = $sql .
            " LEFT OUTER JOIN (
            $c) c
        on (a.DIM = c.DIM
            or b.DIM = c.DIM) ";
    }
    // echo $sql;
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':filter_from_date', $data['filter_from_date'], PDO::PARAM_STR);
    $stmt->bindParam(':filter_to_date', $data['filter_to_date'], PDO::PARAM_STR);

    // if ($data['a_overdue_invoices'] =='Y'   ) {
    //     $stmt->bindParam(':a_overdue_days', $data['a_overdue_days'], PDO::PARAM_STR);
    // }
    // if ($data['b_overdue_invoices'] =='Y'   ) {
    //     $stmt->bindParam(':b_overdue_days', $data['b_overdue_days'], PDO::PARAM_STR);
    // }
    $this->logger->addInfo($sql);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/eid_perf/{filter_from_date}/{filter_to_date}/{percentage}/{total_invoices}/{open_invoices}/{actioned_invoices}/{dispatched_invoices}/{followup_invoices}', function ($request,  $response, array $args) {
    if ($args['total_invoices'] == 'Y') {
        $sql = "SELECT EID,COUNT(INVOICE_NO) FROM (
            SELECT EID,RESORT,INVOICE_NO,COUNT(*)
            FROM AR_INVOICES_ALL_MV
            WHERE TO_CHAR(NVL(CHECK_OUT_DATE,POST_DATE),'YYYY-MM-DD') BETWEEN :filter_from_date AND :filter_to_date
            GROUP BY EID,RESORT,INVOICE_NO
            )
            GROUP BY EID";
    }
    $sql = "select * from AR_EID_PERFORMANCE_DAILY where to_char(snapshot_date,'YYYY-MM-DD') = :filter_date
             ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':filter_from_date', $args['filter_from_date'], PDO::PARAM_STR);
    $stmt->bindParam(':filter_to_date', $args['filter_to_date'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/eid_aging/{filter_from_date}/{filter_to_date}/{DIM}', function ($request,  $response, array $args) {

    $sql =          "  SELECT DIM,
                    AGE_BUCKET1,AGE_BUCKET2,AGE_BUCKET3,AGE_BUCKET456,TOTAL
                    FROM (
                    select ";
    if ($args['DIM'] == 'EID') {
        $sql = $sql .   "NVL(EID,'UNASSIGNED') ";
    } else {
        $sql = $sql .   "NVL(RESORT,'UNASSIGNED')";
    }
    $sql = $sql .       " DIM, sum(age_bucket1) AGE_BUCKET1,sum(age_bucket2) AGE_BUCKET2,sum(age_bucket3) AGE_BUCKET3,sum(age_bucket4)+sum(age_bucket5)+sum(age_bucket6) AGE_BUCKET456,sum(total) TOTAL
                    from AR_INVOICES_ALL_MV
                    WHERE 1=1
                    AND TOTAL <> 0
                    AND TO_CHAR(NVL(CHECK_OUT_DATE,POST_DATE),'YYYY-MM-DD') BETWEEN :filter_from_date AND :filter_to_date
                    group by ";
    if ($args['DIM'] == 'EID') {
        $sql = $sql .   "NVL(EID,'UNASSIGNED') ";
    } else {
        $sql = $sql .   "NVL(RESORT,'UNASSIGNED') ";
    }
    $sql = $sql .      "
                    ) ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':filter_from_date', $args['filter_from_date'], PDO::PARAM_STR);
    $stmt->bindParam(':filter_to_date', $args['filter_to_date'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    // $response->write($sql);
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/eid_aging_perc/{filter_from_date}/{filter_to_date}/{DIM}', function ($request,  $response, array $args) {

    $sql = "  SELECT  DIM,AGE_BUCKET1_PERC,AGE_BUCKET2_PERC,AGE_BUCKET3_PERC,AGE_BUCKET456_PERC
                FROM (
                select ";
    if ($args['DIM'] == 'EID') {
        $sql = $sql .   "NVL(EID,'UNASSIGNED') ";
    } else {
        $sql = $sql .   "NVL(RESORT,'UNASSIGNED')";
    }
    $sql = $sql .   " DIM,
                round(
                DECODE(sum(total),0,0,(sum(age_bucket1)/sum(total))*100)
                ,2) AGE_BUCKET1_PERC,
                round(
                DECODE(sum(total),0,0,(sum(age_bucket2)/sum(total))*100)
                ,2) AGE_BUCKET2_PERC,
                round(
                DECODE(sum(total),0,0,(sum(age_bucket3)/sum(total))*100)
                ,2) AGE_BUCKET3_PERC,
                round(
                DECODE(sum(total),0,0,((sum(age_bucket4)+sum(age_bucket5)+sum(age_bucket6))/sum(total))*100)
                ,2) AGE_BUCKET456_PERC
                from AR_INVOICES_ALL_MV
                WHERE 1=1
                AND TOTAL <> 0
                AND TO_CHAR(NVL(CHECK_OUT_DATE,POST_DATE),'YYYY-MM-DD') BETWEEN :filter_from_date AND :filter_to_date
                group by ";
    if ($args['DIM'] == 'EID') {
        $sql = $sql .   "NVL(EID,'UNASSIGNED') ";
    } else {
        $sql = $sql .   "NVL(RESORT,'UNASSIGNED')";
    }
    $sql = $sql .   "
                ) ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':filter_from_date', $args['filter_from_date'], PDO::PARAM_STR);
    $stmt->bindParam(':filter_to_date', $args['filter_to_date'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/eid_aging_limits/{TARGET_TYPE}/{EID}', function ($request,  $response, array $args) {
    $sql = "select target_description AGING, target_value PERC, order_by from AR_EID_TARGETS
            where target_type = :TARGET_TYPE
            and eid = :EID
            order by order_by";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':EID', $args['EID'], PDO::PARAM_STR);
    $stmt->bindParam(':TARGET_TYPE', $args['TARGET_TYPE'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/resort_aging/{filter_date}', function ($request,  $response, array $args) {
    $sql = "select * from ar_resort_aging_daily  where to_char(snapshot_date,'YYYY-MM-DD') = :filter_date ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':filter_date', $args['filter_date'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/resort_aging_perc/{filter_date}', function ($request,  $response, array $args) {
    $sql = "select * from ar_resort_aging_daily_perc_vw  where to_char(snapshot_date,'YYYY-MM-DD') = :filter_date ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':filter_date', $args['filter_date'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/ssc_aging/{filter_from_date}/{filter_to_date}[/{EID}[/{RESORT}]]', function ($request,  $response, array $args) {
    $sql = "SELECT ACCOUNT_NAME,";
    $sql = $sql . " SUM(AGE_BUCKET1) AGE_BUCKET1,SUM(AGE_BUCKET2) AGE_BUCKET2,SUM(AGE_BUCKET3) AGE_BUCKET3,SUM(AGE_BUCKET4)
    AGE_BUCKET4,SUM(AGE_BUCKET5) AGE_BUCKET5,SUM(AGE_BUCKET6) AGE_BUCKET6,SUM(TOTAL) TOTAL,MAX(CREDIT_LIMIT) CREDIT_LIMIT ";
    $sql = $sql . " from AR_INVOICES_ALL_MV
    WHERE 1=1
    AND TOTAL <> 0
    AND TO_CHAR(NVL(CHECK_OUT_DATE,POST_DATE),'YYYY-MM-DD') BETWEEN :filter_from_date AND :filter_to_date ";
    if ($args['EID'] && $args['EID'] != 'ALL') {
        $sql = $sql . " and EID = :EID  ";
    }
    if ($args['RESORT'] && $args['RESORT'] != 'ALL') {
        $sql = $sql . " and RESORT = :RESORT  ";
    }
    $sql = $sql . " GROUP BY ACCOUNT_NAME  ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':filter_from_date', $args['filter_from_date'], PDO::PARAM_STR);
    $stmt->bindParam(':filter_to_date', $args['filter_to_date'], PDO::PARAM_STR);
    if ($args['EID'] && $args['EID'] != 'ALL') {
        $stmt->bindParam(':EID', $args['EID'], PDO::PARAM_STR);
    }
    if ($args['RESORT'] && $args['RESORT'] != 'ALL') {
        $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    }
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/current_period_snapshot/{RESORT}/{SNAPSHOT_DATE}', function ($request,  $response, array $args) {
    $sqlSS = "select RESORT, SNAPSHOT_DATE,TO_CHAR(CREATED_DATE,'YYYY-MM-DD HH24:MI') CREATED_DATE,CREATED_USER,LOCKED_YN from AR_PUBLISH_DATES where RESORT = :RESORT and SNAPSHOT_DATE = :SNAPSHOT_DATE";
    $stmtSS = $this->db->prepare($sqlSS, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmtSS->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmtSS->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmtSS->execute();
    $currentPeriodSS = $stmtSS->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($currentPeriodSS));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->post('/api/lock_snapshot_date/{RESORT}/{SNAPSHOT_DATE}', function ($request,  $response, array $args) {
    $sqlSS = "UPDATE AR_PUBLISH_DATES SET LOCKED_YN = 'Y' where RESORT = :RESORT and SNAPSHOT_DATE = :SNAPSHOT_DATE";
    $stmtSS = $this->db->prepare($sqlSS, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmtSS->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmtSS->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmtSS->execute();

    $response->write(json_encode($currentPeriodSS));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/aging_template/{RESORT}/{SNAPSHOT_DATE}', function ($request,  $response, array $args) {

    $sql = "SELECT *  FROM AR_AGING_TEMP_SNAPSHOTS
            WHERE RESORT = :RESORT
            AND SNAPSHOT_DATE = :SNAPSHOT_DATE";

    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/aging_templateTest/{RESORT}/{SNAPSHOT_DATE}', function ($request,  $response, array $args) {

    $sql = "SELECT *  FROM AR_AGING_TEMP_SNAPSHOTS
            WHERE RESORT = :RESORT
            AND SNAPSHOT_DATE = :SNAPSHOT_DATE";

    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($sql));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/aging_templateOLD[/{RESORT}]', function ($request,  $response, array $args) {
    $sql1 = "SELECT SNAPSHOT_DATE FROM AR_AGING_PUBLISHED WHERE RESORT = :RESORT
    and SNAPSHOT_DATE = to_char(add_months(sysdate,-1),'YYYY-MM')";
    $stmt1 = $this->db->prepare($sql1, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt1->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt1->execute();
    $snapshotDate = $stmt1->fetchAll(PDO::FETCH_ASSOC)[0]['SNAPSHOT_DATE'];

    $sql = "SELECT I.PMS_ACCOUNT_NO ACCOUNT_NO,I.ACCOUNT_NAME,
                    SUM(I.AGE_BUCKET1) AGE_BUCKET1,
                    SUM(I.AGE_BUCKET2) AGE_BUCKET2,
                    SUM(I.AGE_BUCKET3) AGE_BUCKET3,
                    SUM(I.AGE_BUCKET4) AGE_BUCKET4,
                    SUM(I.AGE_BUCKET5) AGE_BUCKET5,
                    SUM(I.AGE_BUCKET6) AGE_BUCKET6,
                    SUM(I.TOTAL) TOTAL,
                    MAX(I.CREDIT_LIMIT) CREDIT_LIMIT ,
                    SUM(I.AGE_BUCKET6)+(SUM(I.AGE_BUCKET5)/2) PROVISION ,
                    S.AGE_BUCKET6 PREV120,
                    R.REMARKS


                    from AR_INVOICES_ALL_MV I
                    LEFT OUTER JOIN AR_AGING_TEMP_REMARKS R
                    ON (I.RESORT = R.RESORT
                    AND I.PMS_ACCOUNT_NO = R.PMS_ACCOUNT_NO)
                    LEFT OUTER JOIN AR_AGING_PUBLISHED S
                    ON (I.RESORT = S.RESORT
                    AND I.PMS_ACCOUNT_NO = S.PMS_ACCOUNT_NO
                    AND S.SNAPSHOT_DATE = :SNAPSHOT_DATE )
                    WHERE 1=1
                    AND I.TOTAL <> 0
                    AND I.RESORT = :RESORT
                    GROUP BY I.PMS_ACCOUNT_NO,I.ACCOUNT_NAME ,R.REMARKS,S.AGE_BUCKET6 ";


    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    if ($args['RESORT'] && $args['RESORT'] != 'ALL') {
        $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    }
    $stmt->bindParam(':SNAPSHOT_DATE', $snapshotDate, PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});


$app->get('/api/publish_aging_template/{RESORT}/{SNAPSHOT_DATE}', function ($request,  $response, array $args) {

    $user = $_SESSION['user'];
    //Check if there is a snapshot date for the current period
    $sqlc = "select * from AR_PUBLISH_DATES where RESORT = :RESORT and SNAPSHOT_DATE = :SNAPSHOT_DATE";
    $stmtc = $this->db->prepare($sqlc, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmtc->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmtc->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmtc->execute();
    $currentPeriodSS = $stmtc->fetchAll(PDO::FETCH_ASSOC)[0]['SNAPSHOT_DATE'];
    if ($currentPeriodSS) {
        //Delete the date for this resort for the current period
        $sqld = "DELETE FROM AR_PUBLISH_DATES
                 WHERE RESORT = :RESORT
                 AND SNAPSHOT_DATE = :SNAPSHOT_DATE
                 ";
        $stmtd = $this->db->prepare($sqld, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmtd->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
        $stmtd->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
        $stmtd->execute();

        $sqle = "DELETE FROM AR_AGING_PUBLISHED
                 WHERE RESORT = :RESORT
                 AND SNAPSHOT_DATE = :SNAPSHOT_DATE
                 ";
        $stmte = $this->db->prepare($sqle, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmte->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
        $stmte->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
        $stmte->execute();
    }

    //insert current period into AR_PUBLISH_DATES
    $sqlSs = "INSERT INTO AR_PUBLISH_DATES(RESORT,SNAPSHOT_DATE,CREATED_DATE,CREATED_USER)
                    VALUES(:RESORT,:SNAPSHOT_DATE,systimestamp,:USERNAME)";
    $stmtSs = $this->db->prepare($sqlSs, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmtSs->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmtSs->bindParam(':USERNAME', $user, PDO::PARAM_STR);
    $stmtSs->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmtSs->execute();



    $sql = "INSERT INTO AR_AGING_PUBLISHED(SNAPSHOT_DATE,RESORT,PMS_ACCOUNT_NO,ACCOUNT_NAME,
            CREDIT_LIMIT,AGE_BUCKET1,AGE_BUCKET2,AGE_BUCKET3,AGE_BUCKET4,AGE_BUCKET5,AGE_BUCKET6,
            TOTAL,PROVISION,PREV120DAYS,REMARKS,CREATED_DATE,CREATED_USER)
            (SELECT  SNAPSHOT_DATE,RESORT,PMS_ACCOUNT_NO,ACCOUNT_NAME,
            CREDIT_LIMIT,AGE_BUCKET1,AGE_BUCKET2,AGE_BUCKET3,AGE_BUCKET4,AGE_BUCKET5,AGE_BUCKET6,
            TOTAL,PROVISION,PREV120DAYS,REMARKS,CREATED_DATE,CREATED_USER
            FROM AR_AGING_TEMP_SNAPSHOTS
            WHERE RESORT = :RESORT
            AND SNAPSHOT_DATE = :SNAPSHOT_DATE)
     ";

    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt->bindParam(':USERNAME', $user, PDO::PARAM_STR);
    $stmt->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmt->execute();

    $response->write(json_encode($sql));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/snapshots/{RESORT}', function ($request,  $response, array $args) {

    $sql = "SELECT SNAPSHOT_DATE
            FROM AR_SNAPSHOT_DATES
            WHERE RESORT = :RESORT
            ORDER BY SNAPSHOT_DATE DESC";


    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/published_snapshots/{RESORT}', function ($request,  $response, array $args) {

    $sql = "SELECT SNAPSHOT_DATE,COUNT(*)  FROM AR_PUBLISH_DATES
            WHERE RESORT = :RESORT
            GROUP BY SNAPSHOT_DATE
            ORDER BY SNAPSHOT_DATE DESC
            ";

    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // $result = [];
    // if (is_array($report) && !empty($report)) {
    //     $result = $report;
    // } else {
    //     $result['SNAPSHOT_DATE'] = 'Not published yet';
    // }
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/aging_published/{RESORT}/{SNAPSHOT_DATE}', function ($request,  $response, array $args) {

    $sql = "SELECT *  FROM AR_AGING_PUBLISHED
            WHERE RESORT = :RESORT
            AND SNAPSHOT_DATE = :SNAPSHOT_DATE";

    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->get('/api/credit_meeting_minutes/{RESORT}/{SNAPSHOT_DATE}', function ($request,  $response, array $args) {
    $sql = "SELECT * FROM AR_CREDIT_MEETING_MINUTES WHERE RESORT = :RESORT AND SNAPSHOT_DATE = :SNAPSHOT_DATE ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->post('/api/update_credit_meeting_minutes/{RESORT}/{SNAPSHOT_DATE}', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];

    $sqlAll = '';
    $sql1 = "SELECT * FROM AR_CREDIT_MEETING_MINUTES WHERE RESORT = :RESORT AND SNAPSHOT_DATE = :SNAPSHOT_DATE ";
    $stmt1 = $this->db->prepare($sql1, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt1->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt1->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmt1->execute();
    $report = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    $sqlAll .= $sql1 . "<br>";

    if (is_array($report) && !empty($report)) {
        $sql2 = "UPDATE AR_CREDIT_MEETING_MINUTES
            SET
            CR_MEETING_DATE=:CR_MEETING_DATE,
            CR_MEETING_TIME=:CR_MEETING_TIME,
            CR_MEETING_NEXT_DATE=:CR_MEETING_NEXT_DATE,
            CR_MEETING_PRESENT=:CR_MEETING_PRESENT,
            CR_MEETING_NOT_PRESENT=:CR_MEETING_NOT_PRESENT,";
        for ($x = 1; $x <= 8; $x++) { //repeat for each table in Credit Minutes Meeting HTML
            for ($y = 1; $y <= 6; $y++) { //repeat for each possible row in each table
                for ($z = 2; $z <= 4; $z++) {  //repeat for colums 2,3 and 4 of eat row in each table
                    $sql2 = $sql2 . "ITEM" . $x . "_ROW" . $y . "_COL" . $z . "=:ITEM" . $x . "_ROW" . $y . "_COL" . $z . ",";
                }
            }
        }
        $sql2 .=
            "UPDATED_DATE=SYSTIMESTAMP,
         UPDATED_USER=:UPDATED_USER
         where RESORT = :RESORT
         and SNAPSHOT_DATE = :SNAPSHOT_DATE";

        $stmt2 = $this->db->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt2->bindParam(':UPDATED_USER', $user, PDO::PARAM_STR);
        $stmt2->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
        $stmt2->bindParam(':SNAPSHOT_DATE',  $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_DATE',  $request->getParam('CR_MEETING_DATE'), PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_TIME', $request->getParam('CR_MEETING_TIME'), PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_HOTEL', $request->getParam('CR_MEETING_HOTEL'), PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_NEXT_DATE', $request->getParam('CR_MEETING_NEXT_DATE'), PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_PRESENT', $request->getParam('CR_MEETING_PRESENT'), PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_NOT_PRESENT', $request->getParam('CR_MEETING_NOT_PRESENT'), PDO::PARAM_STR);
        for ($x = 1; $x <= 8; $x++) { //repeat for each table in Credit Minutes Meeting HTML
            for ($y = 1; $y <= 6; $y++) { //repeat for each possible row in each table
                for ($z = 2; $z <= 4; $z++) {  //repeat for colums 2,3 and 4 of eat row in each table
                    $stmt2->bindParam(':ITEM' . $x . '_ROW' . $y . '_COL' . $z, $request->getParam('ITEM' . $x . '_ROW' . $y . '_COL' . $z), PDO::PARAM_STR);
                }
            }
        }



        $stmt2->execute();
        $sqlAll .= $sql2 . "<br>";
    } else {

        $sql2 = "INSERT INTO AR_CREDIT_MEETING_MINUTES(RESORT,SNAPSHOT_DATE,CREATED_DATE,CREATED_USER,CR_MEETING_DATE,CR_MEETING_TIME,
        CR_MEETING_NEXT_DATE,CR_MEETING_PRESENT,CR_MEETING_NOT_PRESENT ";
        for ($x = 1; $x <= 8; $x++) { //repeat for each table in Credit Minutes Meeting HTML
            for ($y = 1; $y <= 6; $y++) { //repeat for each possible row in each table
                for ($z = 2; $z <= 4; $z++) {  //repeat for colums 2,3 and 4 of eat row in each table
                    $sql2 .= ",ITEM" . $x . "_ROW" . $y . "_COL" . $z;
                }
            }
        }
        $sql2 .= ")";
        $sql2 .= " VALUES(  :RESORT ,:SNAPSHOT_DATE,SYSTIMESTAMP,:CREATED_USER,:CR_MEETING_DATE,:CR_MEETING_TIME,:CR_MEETING_NEXT_DATE,:CR_MEETING_PRESENT,
        :CR_MEETING_NOT_PRESENT ";
        for ($x = 1; $x <= 8; $x++) { //repeat for each table in Credit Minutes Meeting HTML
            for ($y = 1; $y <= 6; $y++) { //repeat for each possible row in each table
                for ($z = 2; $z <= 4; $z++) {  //repeat for colums 2,3 and 4 of eat row in each table
                    $sql2 .= ",:ITEM" . $x . "_ROW" . $y . "_COL" . $z;
                }
            }
        }
        $sql2 .= ") ";

        $stmt2 = $this->db->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt2->bindParam(':CREATED_USER', $user, PDO::PARAM_STR);
        $stmt2->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
        $stmt2->bindParam(':SNAPSHOT_DATE',  $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_DATE',  $request->getParam('CR_MEETING_DATE'), PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_TIME', $request->getParam('CR_MEETING_TIME'), PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_HOTEL', $request->getParam('CR_MEETING_HOTEL'), PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_NEXT_DATE', $request->getParam('CR_MEETING_NEXT_DATE'), PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_PRESENT', $request->getParam('CR_MEETING_PRESENT'), PDO::PARAM_STR);
        $stmt2->bindParam(':CR_MEETING_NOT_PRESENT', $request->getParam('CR_MEETING_NOT_PRESENT'), PDO::PARAM_STR);
        for ($x = 1; $x <= 8; $x++) { //repeat for each table in Credit Minutes Meeting HTML
            for ($y = 1; $y <= 6; $y++) { //repeat for each possible row in each table
                for ($z = 2; $z <= 4; $z++) {  //repeat for colums 2,3 and 4 of eat row in each table
                    $stmt2->bindParam(':ITEM' . $x . '_ROW' . $y . '_COL' . $z, $request->getParam('ITEM' . $x . '_ROW' . $y . '_COL' . $z), PDO::PARAM_STR);
                }
            }
        }



        $stmt2->execute();
        $sqlAll .= $sql2 . "<br>";
    }

    $response->write(json_encode($sql2));
    // $response->write(json_encode($existingTargets));

    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});

$app->get('/api/eid_targets/{EID}/{TARGET_TYPE}', function ($request,  $response, array $args) {
    $sql = "SELECT * FROM AR_EID_TARGETS WHERE EID = :EID AND TARGET_TYPE  = :TARGET_TYPE ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':EID', $args['EID'], PDO::PARAM_STR);
    $stmt->bindParam(':TARGET_TYPE', $args['TARGET_TYPE'], PDO::PARAM_STR);
    $stmt->execute();
    $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response->write(json_encode($report));
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->post('/api/update_aging_temp_remarks/{RESORT}/{SNAPSHOT_DATE}', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];

    $sql = "UPDATE AR_AGING_TEMP_SNAPSHOTS
                SET REMARKS = :REMARKS,
                PROVISION = :PROVISION,
                PREV120DAYS = :PREV120DAYS,
                UPDATED_DATE = SYSTIMESTAMP,
                UPDATED_USER = :USERNAME
                where RESORT = :RESORT
                and PMS_ACCOUNT_NO = :PMS_ACCOUNT_NO
                and SNAPSHOT_DATE = :SNAPSHOT_DATE";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt->bindParam(':PMS_ACCOUNT_NO', $request->getParam("REMARKS_ACCOUNT_NO"), PDO::PARAM_STR);
    $stmt->bindParam(':USERNAME', $user, PDO::PARAM_STR);
    $stmt->bindParam(':REMARKS', $request->getParam("REMARKS"), PDO::PARAM_STR);
    $stmt->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
    $stmt->bindParam(':PROVISION', $request->getParam("PROVISION"), PDO::PARAM_STR);
    $stmt->bindParam(':PREV120DAYS', $request->getParam("PREV120DAYS"), PDO::PARAM_STR);
    $stmt->execute();


    $response->write(json_encode($sql));
    // $response->write(json_encode($existingTargets));

    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});



$app->post('/api/delete_aging_temp_remarks/{RESORT}', function ($request,  $response, array $args) {

    $sql = "DELETE FROM AR_AGING_TEMP_REMARKS
        WHERE RESORT = :RESORT
        AND PMS_ACCOUNT_NO = :PMS_ACCOUNT_NO
    ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
    $stmt->bindParam(':PMS_ACCOUNT_NO', $request->getParam("REMARKS_ACCOUNT_NO"), PDO::PARAM_STR);
    $stmt->execute();

    $response->write(json_encode($sql));
    // $response->write(json_encode($existingTargets));

    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});

$app->post('/api/updateTargets/{EID}/{TARGET_TYPE}', function ($request,  $response, array $args) {


    $user = $_SESSION['user'];
    $dims = ['DIM1', 'DIM2', 'DIM3', 'DIM4'];

    $queries = '';
    foreach ($dims as  $dim) {

        $sql1 = "select target_code from AR_EID_TARGETS where target_type = :TARGET_TYPE and target_code = :TARGET_CODE and eid = :EID ";
        $stmt1 = $this->db->prepare($sql1, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt1->bindParam(':EID', $args['EID'], PDO::PARAM_STR);
        $stmt1->bindParam(':TARGET_TYPE', $args['TARGET_TYPE'], PDO::PARAM_STR);
        $stmt1->bindParam(':TARGET_CODE', $dim, PDO::PARAM_STR);
        $stmt1->execute();
        $existingTargets = $stmt1->fetchAll(PDO::FETCH_ASSOC);

        if (is_array($existingTargets) && !empty($existingTargets)) {

            $sql = "UPDATE AR_EID_TARGETS
        SET TARGET_VALUE = :TARGET_VALUE
        WHERE EID = :EID
        AND TARGET_TYPE = :TARGET_TYPE
        AND TARGET_CODE = :TARGET_CODE ";
            $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $stmt->bindParam(':EID', $args['EID'], PDO::PARAM_STR);
            $stmt->bindParam(':TARGET_TYPE', $args['TARGET_TYPE'], PDO::PARAM_STR);
            $stmt->bindParam(':TARGET_CODE', $dim, PDO::PARAM_STR);
            switch ($dim) {
                case 'DIM1':
                    $stmt->bindParam(':TARGET_VALUE', $request->getParam("DIM1"), PDO::PARAM_STR);
                    break;
                case 'DIM2':
                    $stmt->bindParam(':TARGET_VALUE', $request->getParam("DIM2"), PDO::PARAM_STR);
                    break;
                case 'DIM3':
                    $stmt->bindParam(':TARGET_VALUE', $request->getParam("DIM3"), PDO::PARAM_STR);
                    break;
                case 'DIM4':
                    $stmt->bindParam(':TARGET_VALUE', $request->getParam("DIM4"), PDO::PARAM_STR);
                    break;
            }

            $stmt->execute();
            $queries  = $queries . " " . $dim . " : "   . $sql;
        } else {
            $sql = "INSERT INTO AR_EID_TARGETS(EID,TARGET_TYPE,TARGET_CODE,TARGET_DESCRIPTION,TARGET_VALUE,MEASURE,ORDER_BY)
        VALUES(:EID,:TARGET_TYPE,:TARGET_CODE,:TARGET_DESCRIPTION,:TARGET_VALUE,'PERCENTAGE',:ORDER_BY) ";
            $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $stmt->bindParam(':EID', $args['EID'], PDO::PARAM_STR);
            $stmt->bindParam(':TARGET_TYPE', $args['TARGET_TYPE'], PDO::PARAM_STR);
            $stmt->bindParam(':TARGET_CODE', $dim, PDO::PARAM_STR);
            switch ($dim) {
                case 'DIM1':
                    $stmt->bindParam(':TARGET_VALUE', $request->getParam("DIM1"), PDO::PARAM_STR);
                    $targetDesc =  '0 - 30 Days';
                    $orderBy = 1;
                    break;
                case 'DIM2':
                    $stmt->bindParam(':TARGET_VALUE', $request->getParam("DIM2"), PDO::PARAM_STR);
                    $targetDesc =  '31 - 60 Days';
                    $orderBy = 2;
                    break;
                case 'DIM3':
                    $stmt->bindParam(':TARGET_VALUE', $request->getParam("DIM3"), PDO::PARAM_STR);
                    $targetDesc =  '61 - 90 Days';
                    $orderBy = 3;
                    break;
                case 'DIM4':
                    $stmt->bindParam(':TARGET_VALUE', $request->getParam("DIM4"), PDO::PARAM_STR);
                    $targetDesc =  '91 - Older';
                    $orderBy = 4;
                    break;
            }
            $stmt->bindParam(':TARGET_DESCRIPTION', $targetDesc, PDO::PARAM_STR);
            $stmt->bindParam(':ORDER_BY', $orderBy, PDO::PARAM_STR);

            $stmt->execute();
            $queries  = $queries . " " . $dim . " : "   . $sql;
        }
    }


    $response->write(json_encode($request->getParsedBody()));
    // $response->write(json_encode($existingTargets));

    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});


$app->post('/updateMapping', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];

    $sql = "update AR_ACCOUNT_MAP set
        ACCOUNT_NO_TO = UPPER(:ACCOUNT_NO_TO),
        ACCOUNT_NAME = :ACCOUNT_NAME,
        CONTACT = :CONTACT,
        PAYMENT_METHOD = UPPER(:PAYMENT),
        MAPPED = 'MAPPED',
        UPDATE_DATE = SYSTIMESTAMP,
        UPDATE_USER = :USERNAME
        WHERE RESORT = UPPER(:RESORT)
        AND ACCOUNT_NO_FROM = UPPER(:ACCOUNT_NO_FROM) ";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':ACCOUNT_NO_FROM', $request->getParam('FROM_ACCOUNT'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NO_TO', $request->getParam('TO_ACCOUNT'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NAME', $request->getParam('ACCOUNT_NAME'), PDO::PARAM_STR);
    $stmt->bindParam(':RESORT', $request->getParam('RESORT'), PDO::PARAM_STR);
    $stmt->bindParam(':CONTACT', $request->getParam('CONTACT'), PDO::PARAM_STR);
    $stmt->bindParam(':PAYMENT', $request->getParam('PAYMENT'), PDO::PARAM_STR);
    $stmt->bindParam(':USERNAME', $user, PDO::PARAM_STR);
    $stmt->execute();

    $response->write(json_encode($exception));
    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});

$app->post('/updateMappingV3', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];

    $sql = "update AR_ACCOUNT_MAP set
        ACCOUNT_NO_TO = UPPER(:ACCOUNT_NO_TO),
        ACCOUNT_NAME = :ACCOUNT_NAME,
        ACCOUNT_TYPE = :ACCOUNT_TYPE,
        CONTACT = :CONTACT,
        PAYMENT_METHOD = UPPER(:PAYMENT),
        MAPPED = 'MAPPED',
        REQUIRED_DOCS = :REQUIRED_DOCS,
        UPDATE_DATE = SYSTIMESTAMP,
        UPDATE_USER = :USERNAME
        WHERE RESORT = UPPER(:RESORT)
        AND ACCOUNT_NO_FROM = UPPER(:ACCOUNT_NO_FROM) ";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':ACCOUNT_NO_FROM', $request->getParam('FROM_ACCOUNT'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NO_TO', $request->getParam('TO_ACCOUNT'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_TYPE', $request->getParam('ACCOUNT_TYPE'), PDO::PARAM_STR);
    $stmt->bindParam(':REQUIRED_DOCS', $request->getParam('REQUIRED_DOCS'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NAME', $request->getParam('ACCOUNT_NAME'), PDO::PARAM_STR);
    $stmt->bindParam(':RESORT', $request->getParam('RESORT'), PDO::PARAM_STR);
    $stmt->bindParam(':CONTACT', $request->getParam('CONTACT'), PDO::PARAM_STR);
    $stmt->bindParam(':PAYMENT', $request->getParam('PAYMENT'), PDO::PARAM_STR);
    $stmt->bindParam(':USERNAME', $user, PDO::PARAM_STR);
    $stmt->execute();

    $response->write(json_encode($exception));
    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});

$app->post('/updateMappingV4', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];

    $fileUploaded = null;
    $newFileAdded = false;
    $errors = [];
    $path = 'uploads/mapping/';
    $extensions = ['pdf'];
    $file_name = $_FILES['files']['name'][0];
    $file_tmp = $_FILES['files']['tmp_name'][0];
    $file_type = $_FILES['files']['type'][0];
    $file_size = $_FILES['files']['size'][0];
    $file_ext = strtolower(end(explode('.', $file_name)));
    $file = $path . $file_name;
    if (!in_array($file_ext, $extensions)) {
        $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
    }

    if ($file_size > 2097152) {
        $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
    }
    if (empty($errors)) {
        move_uploaded_file($file_tmp, $file);
        $fileUploaded = '<a href="uploads\mapping\\' . $file_name . '" target="_blank">' . $file_name . '</a>';
        $newFileAdded = true;
    }
    if ($errors) {
        $this->flash->addMessage('fileuploadfailed', 'No new file uploaded for account ' . $request->getParam('TO_ACCOUNT') . '. PDF files of maximum size 2MB can be attached for each account.');
    };

    $sql = "update AR_ACCOUNT_MAP set
        ACCOUNT_NO_TO = UPPER(:ACCOUNT_NO_TO),
        ACCOUNT_NAME = :ACCOUNT_NAME,
        ACCOUNT_TYPE = :ACCOUNT_TYPE,
        CONTACT = :CONTACT,
        PAYMENT_METHOD = UPPER(:PAYMENT),
        MAPPED = 'MAPPED',
        REQUIRED_DOCS = :REQUIRED_DOCS,";

    if ($newFileAdded) {
        $sql = $sql . " DOCS_UPLOADED = :DOCS_UPLOADED,";
    }
    $sql = $sql . "
        UPDATE_DATE = SYSTIMESTAMP,
        UPDATE_USER = :USERNAME
        WHERE RESORT = UPPER(:RESORT)
        AND ACCOUNT_NO_FROM = UPPER(:ACCOUNT_NO_FROM) ";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':ACCOUNT_NO_FROM', $request->getParam('FROM_ACCOUNT'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NO_TO', $request->getParam('TO_ACCOUNT'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_TYPE', $request->getParam('ACCOUNT_TYPE'), PDO::PARAM_STR);
    $stmt->bindParam(':REQUIRED_DOCS', $request->getParam('REQUIRED_DOCS'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NAME', $request->getParam('ACCOUNT_NAME'), PDO::PARAM_STR);
    $stmt->bindParam(':RESORT', $request->getParam('RESORT'), PDO::PARAM_STR);
    $stmt->bindParam(':CONTACT', $request->getParam('CONTACT'), PDO::PARAM_STR);
    $stmt->bindParam(':PAYMENT', $request->getParam('PAYMENT'), PDO::PARAM_STR);
    $stmt->bindParam(':USERNAME', $user, PDO::PARAM_STR);
    $stmt->bindParam(':DOCS_UPLOADED', $fileUploaded, PDO::PARAM_STR);
    $stmt->execute();

    $response->write(json_encode($exception));
    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});



$app->post('/updateOwners', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];

    $sql = "UPDATE AR_ACCOUNT_USER_MAP
                SET EID = :EID
                ,ACCOUNT_NAME = :ACCOUNT_NAME
                ,UPDATE_DATE = SYSTIMESTAMP
                ,UPDATE_USER = :USERNAME
                WHERE ACCOUNT_NO = UPPER(:ACCOUNT_NO) ";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':EID', $request->getParam('OWNER_EID'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NAME', $request->getParam('OWNER_ACCOUNT_NAME'), PDO::PARAM_STR);
    $stmt->bindParam(':USERNAME', $user, PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NO', $request->getParam('OWNER_ACCOUNT'), PDO::PARAM_STR);
    $stmt->execute();

    $response->write(json_encode($exception));
    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});

$app->post('/deleteMapping', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];
    // try {
    $sql = "delete from AR_ACCOUNT_MAP
        where ACCOUNT_NO_FROM = UPPER(:ACCOUNT_NO_FROM)
        and ACCOUNT_NO_TO = UPPER(:ACCOUNT_NO_TO)
        and RESORT = UPPER(:RESORT) ";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':ACCOUNT_NO_FROM', $request->getParam('FROM_ACCOUNT'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NO_TO', $request->getParam('TO_ACCOUNT'), PDO::PARAM_STR);
    $stmt->bindParam(':RESORT', $request->getParam('RESORT'), PDO::PARAM_STR);
    $stmt->execute();
    // } catch (PDOException $exception) {
    //     // Output expected PDOException.
    // }
    // catch (Exception $exception) {
    //     // Output unexpected Exceptions.
    // }
    $response->write(json_encode($exception));
    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});

$app->post('/deleteOwners', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];

    $sql = "DELETE FROM AR_ACCOUNT_USER_MAP
                WHERE ACCOUNT_NO = UPPER(:ACCOUNT_NO)
                AND EID = :EID ";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(':EID', $request->getParam('OWNER_EID'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NO', $request->getParam('OWNER_ACCOUNT'), PDO::PARAM_STR);
    $stmt->execute();

    $response->write(json_encode($exception));
    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});

// $app->post('/addMapping',function ( $request,  $response, array $args) {
//     $user = $_SESSION['user'];



//         $sql = "insert into AR_ACCOUNT_MAP(RESORT,ACCOUNT_NO_FROM,ACCOUNT_NO_TO,ACCOUNT_NAME,CONTACT,PAYMENT_METHOD,INSERT_DATE,INSERT_USER,MAPPED )
//         values
//         (UPPER(:RESORT),UPPER(:ACCOUNT_NO_FROM),UPPER(:ACCOUNT_NO_TO),:ACCOUNT_NAME,:CONTACT,UPPER(:PAYMENT),systimestamp,:USERNAME , 'MAPPED') ";
//         $stmt = $this->db->prepare($sql);
//         $stmt->bindParam(':ACCOUNT_NO_FROM', $request->getParam('FROM_ACCOUNT_NEW'), PDO::PARAM_STR);
//         $stmt->bindParam(':ACCOUNT_NO_TO', $request->getParam('TO_ACCOUNT_NEW'), PDO::PARAM_STR);
//         $stmt->bindParam(':ACCOUNT_NAME', $request->getParam('ACCOUNT_NAME_NEW'), PDO::PARAM_STR);
//         $stmt->bindParam(':RESORT', $request->getParam('RESORT_NEW'), PDO::PARAM_STR);
//         $stmt->bindParam(':CONTACT', $request->getParam('CONTACT_NEW'), PDO::PARAM_STR);
//         $stmt->bindParam(':PAYMENT', $request->getParam('PAYMENT_NEW'), PDO::PARAM_STR);
//         $stmt->bindParam(':USERNAME', $user, PDO::PARAM_STR);
//         $stmt->execute();

//     $response->write(json_encode($exception));
//     if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
//         return $response;
//     }

// });


$app->post('/addMappingV3', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];


    $sql = "insert into AR_ACCOUNT_MAP(RESORT,ACCOUNT_NO_FROM,ACCOUNT_NO_TO,ACCOUNT_NAME,CONTACT,PAYMENT_METHOD,INSERT_DATE,INSERT_USER,MAPPED,ACCOUNT_TYPE,REQUIRED_DOCS )
        values
        (UPPER(:RESORT),UPPER(:ACCOUNT_NO_FROM),UPPER(:ACCOUNT_NO_TO),:ACCOUNT_NAME,:CONTACT,UPPER(:PAYMENT),systimestamp,:USERNAME , 'MAPPED',:ACCOUNT_TYPE,:REQUIRED_DOCS) ";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(
        ':ACCOUNT_NO_FROM',
        $request->getParam('FROM_ACCOUNT_NEW'),
        PDO::PARAM_STR
    );
    $stmt->bindParam(':ACCOUNT_NO_TO', $request->getParam('TO_ACCOUNT_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_TYPE', $request->getParam('ACCOUNT_TYPE_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':REQUIRED_DOCS', $request->getParam('REQUIRED_DOCS_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NAME', $request->getParam('ACCOUNT_NAME_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':RESORT', $request->getParam('RESORT_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':CONTACT', $request->getParam('CONTACT_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':PAYMENT', $request->getParam('PAYMENT_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':USERNAME', $user, PDO::PARAM_STR);
    $stmt->execute();

    $response->write(json_encode($exception));
    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});

$app->post('/getStuff', function ($request,  $response, array $args) {

    $response->write("Stuff");
    return $response
        ->withHeader('Content-type', 'application/json');
});

$app->post('/addMappingV4', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];

    $fileUploaded = null;
    $errors = [];
    $path = 'uploads/mapping/';
    $extensions = ['pdf'];
    $file_name = $_FILES['files']['name'][0];
    $file_tmp = $_FILES['files']['tmp_name'][0];
    $file_type = $_FILES['files']['type'][0];
    $file_size = $_FILES['files']['size'][0];
    $file_ext = strtolower(end(explode('.', $file_name)));
    $file = $path . $file_name;
    if (!in_array($file_ext, $extensions)) {
        $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
    }

    if ($file_size > 2097152) {
        $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
    }
    if (empty($errors)) {
        move_uploaded_file($file_tmp, $file);
        $fileUploaded = '<a href="uploads\mapping\\' . $file_name . '" target="_blank">' . $file_name . '</a>';
    }
    if ($errors) {
        $this->flash->addMessage('fileuploadfailed', 'No file uploaded for account ' . $request->getParam('TO_ACCOUNT_NEW') . '. PDF files of maximum size 2MB can be attached for each account.');
    };
    // $this->flash->addMessage('fileuploadfailed',$file_name);

    $sql = "insert into AR_ACCOUNT_MAP(RESORT,ACCOUNT_NO_FROM,ACCOUNT_NO_TO,ACCOUNT_NAME,CONTACT,PAYMENT_METHOD,INSERT_DATE,INSERT_USER,MAPPED,ACCOUNT_TYPE,REQUIRED_DOCS,DOCS_UPLOADED )
        values
        (UPPER(:RESORT),UPPER(:ACCOUNT_NO_FROM),UPPER(:ACCOUNT_NO_TO),:ACCOUNT_NAME,:CONTACT,UPPER(:PAYMENT),systimestamp,:USERNAME , 'MAPPED',:ACCOUNT_TYPE,:REQUIRED_DOCS,:DOCS_UPLOADED) ";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(
        ':ACCOUNT_NO_FROM',
        $request->getParam('FROM_ACCOUNT_NEW'),
        PDO::PARAM_STR
    );
    $stmt->bindParam(':ACCOUNT_NO_TO', $request->getParam('TO_ACCOUNT_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_TYPE', $request->getParam('ACCOUNT_TYPE_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':REQUIRED_DOCS', $request->getParam('REQUIRED_DOCS_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':ACCOUNT_NAME', $request->getParam('ACCOUNT_NAME_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':RESORT', $request->getParam('RESORT_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':CONTACT', $request->getParam('CONTACT_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':PAYMENT', $request->getParam('PAYMENT_NEW'), PDO::PARAM_STR);
    $stmt->bindParam(':USERNAME', $user, PDO::PARAM_STR);
    $stmt->bindParam(':DOCS_UPLOADED', $fileUploaded, PDO::PARAM_STR);
    $execute = $stmt->execute();

    $response->write($execute);
    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
    // $response->write("Stuff");
    // return $response
    //     ->withHeader('Content-type', 'application/json')
    //     ;

});


$app->post('/addOwners', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];

    $sqlu = "insert into AR_ACCOUNT_USER_MAP(ACCOUNT_NO,ACCOUNT_NAME,EID,INSERT_DATE,INSERT_USER)
                VALUES (UPPER(:ACCOUNT_NO), :ACCOUNT_NAME, :EID, systimestamp,:USERNAME) ";
    $stmtu = $this->db->prepare($sqlu);
    $stmtu->bindParam(':ACCOUNT_NO', $request->getParam('OWNER_ACCOUNT_NEW'), PDO::PARAM_STR);
    $stmtu->bindParam(':ACCOUNT_NAME', $request->getParam('OWNER_ACCOUNT_NAME_NEW'), PDO::PARAM_STR);
    $stmtu->bindParam(':EID', $request->getParam('OWNER_EID_NEW'), PDO::PARAM_STR);
    $stmtu->bindParam(':USERNAME', $user, PDO::PARAM_STR);
    $stmtu->execute();



    $response->write(json_encode($exception));
    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});




$app->post('/commentAdd', function ($request,  $response, array $args) {
    $user = $_SESSION['user'];
    try {
        $sql = "insert into AR_COMMENTS (comment_id,detail_level,account_no,resort,invoice_no,c_owner,dispatch_date,ptp,review_comment,rc_eid,rc_date,call_or_email_date,level1,level2,level3,coll_comment,cc_id,cc_date,action_date,id)
        (select COMMENT_SEQ.nextval,:DETAIL_LEVEL, UPPER(:ACCOUNT_NO) ";
        if (strlen($request->getParam('RESORT')) > 0) {
            $sql = $sql . ",UPPER(:RESORT) ";
        } else {
            $sql = $sql . ",null ";
        }
        if (strlen($request->getParam('INVOICE_NO')) > 0) {
            $sql = $sql . ",:INVOICE_NO ";
        } else {
            $sql = $sql . ",null ";
        }
        if (strlen($request->getParam('OWNER')) > 0) {
            $sql = $sql . ",:OWNER ";
        } else {
            $sql = $sql . ",null ";
        }
        if (strlen($request->getParam('DISPATCH_DATE')) > 0) {
            $sql = $sql . ",to_date(:DISPATCH_DATE,'YYYY-MM-DD') ";
        } else {
            $sql = $sql . ",null ";
        }
        if (strlen($request->getParam('PTP')) > 0) {
            $sql = $sql . ",to_date(:PTP,'YYYY-MM-DD') ";
        } else {
            $sql = $sql . ",null ";
        }
        if (strlen($request->getParam('REVIEW_COMMENT')) > 0) {
            $sql = $sql . ",:REVIEW_COMMENT  ";
        } else {
            $sql = $sql . ",null ";
        }
        if (strlen($request->getParam('REVIEW_COMMENT')) > 0 ||  strlen($request->getParam('DISPATCH_DATE')) > 0 ||  strlen($request->getParam('OWNER')) > 0) {
            $sql = $sql . ",'$user',systimestamp ";
        } else {
            $sql = $sql . ",null,null ";
        }
        if (strlen($request->getParam('EMAIL_CALL_DATE')) > 0) {
            $sql = $sql . ",to_date(:EMAIL_CALL_DATE,'YYYY-MM-DD') ";
        } else {
            $sql = $sql . ",null ";
        }
        if (strlen($request->getParam('LEVEL1')) > 0) {
            $sql = $sql . ",to_date(:LEVEL1,'YYYY-MM-DD') ";
        } else {
            $sql = $sql . ",null ";
        }
        if (strlen($request->getParam('LEVEL2')) > 0) {
            $sql = $sql . ",to_date(:LEVEL2,'YYYY-MM-DD') ";
        } else {
            $sql = $sql . ",null ";
        }
        if (strlen($request->getParam('LEVEL3')) > 0) {
            $sql = $sql . ",to_date(:LEVEL3,'YYYY-MM-DD') ";
        } else {
            $sql = $sql . ",null ";
        }
        if (strlen($request->getParam('COLL_COMMENT')) > 0) {
            $sql = $sql . ",:COLL_COMMENT  ";
        } else {
            $sql = $sql . ",null ";
        }
        if (
            strlen($request->getParam('EMAIL_CALL_DATE')) > 0 ||
            strlen($request->getParam('LEVEL1')) > 0 ||
            strlen($request->getParam('LEVEL2')) > 0 ||
            strlen($request->getParam('LEVEL3')) > 0 ||
            strlen($request->getParam('COLL_COMMENT')) > 0
        ) {
            $sql = $sql . ",'$user',systimestamp ";
        } else {
            $sql = $sql . ",null,null ";
        }
        $sql = $sql . ",systimestamp,upper('$user') ";
        $sql = $sql .  " from dual ) ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':DETAIL_LEVEL', $request->getParam('LEVEL'), PDO::PARAM_STR);
        $stmt->bindParam(':ACCOUNT_NO', $request->getParam('ACCOUNT_NO'), PDO::PARAM_STR);
        $stmt->bindParam(':RESORT', $request->getParam('RESORT'), PDO::PARAM_STR);
        $stmt->bindParam(':INVOICE_NO', $request->getParam('INVOICE_NO'), PDO::PARAM_STR);
        $stmt->bindParam(':OWNER', $request->getParam('OWNER'), PDO::PARAM_STR);
        $stmt->bindParam(':DISPATCH_DATE', $request->getParam('DISPATCH_DATE'), PDO::PARAM_STR);
        $stmt->bindParam(':PTP', $request->getParam('PTP'), PDO::PARAM_STR);
        $stmt->bindParam(':EMAIL_CALL_DATE', $request->getParam('EMAIL_CALL_DATE'), PDO::PARAM_STR);
        $stmt->bindParam(':LEVEL1', $request->getParam('LEVEL1'), PDO::PARAM_STR);
        $stmt->bindParam(':LEVEL2', $request->getParam('LEVEL2'), PDO::PARAM_STR);
        $stmt->bindParam(':LEVEL3', $request->getParam('LEVEL3'), PDO::PARAM_STR);
        $stmt->bindParam(':REVIEW_COMMENT', $request->getParam('REVIEW_COMMENT'), PDO::PARAM_STR);
        $stmt->bindParam(':COLL_COMMENT', $request->getParam('COLL_COMMENT'), PDO::PARAM_STR);

        $stmt->execute();
    } catch (PDOException $exception) {
        // Output expected PDOException.

    } catch (Exception $exception) {
        // Output unexpected Exceptions.

    }
    $response->write(json_encode($sql));

    if ($request->getHeader('X-Requested-With') === 'XMLHttpRequest') {
        return $response;
    }
});