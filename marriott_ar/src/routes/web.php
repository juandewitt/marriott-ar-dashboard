<?php


$app->get('/',function ( $request,  $response, array $args){
   return $response->withRedirect($this->router->pathFor('signin'));
})->setName('home');

$app->get('/signin',function ( $request,  $response, array $args){
    $response = $this->view->render($response, 'signin.twig');
})->setName('signin');

$app->post('/signin', 'App\Controllers\MainController:login')->setName('signin');
$app->get('/logout','App\Controllers\MainController:logout')->setName('logout');

$app->get('/changepassword',function ($request, $response, array $args){
    $response = $this->view->render($response, 'change_password.twig'
);
})->setName('changepassword');

$app->post('/changepassword', 'App\Controllers\MainController:updateUserPassword')->setName('changepassword');

/**
 * Route group for all top level tabs(Market, Customers, Cust/Hotels, Invoice, Mapping,Owners,Data Upload )
 */
$app->group('/', function () use ($app) {

    /**
     * Tab -> Market
     * Aging Summary by Account Type
     * Renders view: D:\Apache24\htdocs\marriott_ar\src\resources\views\level1Subtotals.twig
     * Table ID:level1Table
     *
     *
     */
    $app->get('level1',function ($request, $response, array $args){
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['accessPerformanceYN'];
        $hotelAccess = $attr['hotelAccess'];
            $response = $this->view->render($response, 'level1Subtotals.twig',
            [
                'perfAccessYN' => $perfAccessYN ,
                'hotelAccess' => $hotelAccess,
            ]);

    })->setName('level1');
    /**
     * Tab -> Customers
     * Aging Summary by Account
     * Renders view: D:\Apache24\htdocs\marriott_ar\src\resources\views\level2SubtotalsNew.twig
     * Table ID:level2TableV2
     *
     *
     */
    $app->get('level2',function ($request, $response, array $args){
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['accessPerformanceYN'];
        $hotelAccess = $attr['hotelAccess'];
        $response = $this->view->render($response, 'level2SubtotalsNew.twig',
    [
        'perfAccessYN' => $perfAccessYN ,
        'hotelAccess' => $hotelAccess,
    ]);

    })->setName('level2');
    /**
     * Tab -> Cust/Hotel
     * Aging Summary by Account & Hotel
     * Renders view: D:\Apache24\htdocs\marriott_ar\src\resources\views\level3Subtotals.twig
     * Table ID:level3Table
     *
     */
    $app->get('level3',function ($request,  $response, array $args){
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['accessPerformanceYN'];
        $hotelAccess = $attr['hotelAccess'];
        $response = $this->view->render($response, 'level3Subtotals.twig',
        [
            'perfAccessYN' => $perfAccessYN ,
            'hotelAccess' => $hotelAccess,
        ]);
    })->setName('level3');
/**
     * Tab -> Invoice
     * Aging Detail by Account & Hotel
     * Renders view: D:\Apache24\htdocs\marriott_ar\src\resources\views\level4test.twig
     * Table ID:level4test
     *
     */
    $app->get('level4',function ( $request,  $response, array $args){
    $sql = "select eid,count(*) from ar_account_user_map group by eid   ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->execute();
    $eids = $stmt->fetchAll(PDO::FETCH_OBJ);
    $attr = $request->getAttribute('attr');
    $perfAccessYN = $attr['accessPerformanceYN'];
    $hotelAccess = $attr['hotelAccess'];
    $response = $this->view->render($response, 'level4test.twig',
    [
        'eids' => $eids,
        'session_user' => $_SESSION['user'],
        'perfAccessYN' => $perfAccessYN ,
        'hotelAccess' => $hotelAccess,
    ]);

})->setName('level4');

    /**
     * Tab -> Mapping
     * Table that shows mapping of PMS AR number to central AR number
     * Renders view: D:\Apache24\htdocs\marriott_ar\src\resources\views\mappingV4.twig
     * Table ID:level4test
     *
     */
    $app->get('mapping',function ($request, $response, array $args){
        $sql = "select EID from AR_EID_ACCESS
                where function = 'MAPPING'
                and active_yn = 'Y'
                and UPPER(eid) = UPPER(:eid)  ";
        $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->bindParam(':eid', $_SESSION['user'], PDO::PARAM_STR);
        $stmt->execute();
        $access = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $accessAllowed = $access ? 'Y' : 'N';


        $sqlat = "  select  code,description
                    from ar_configuration
                    where config_type = 'ACCOUNT_TYPE'  ";
        $stmtat = $this->db->prepare($sqlat, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmtat->execute();
        $at = $stmtat->fetchAll(PDO::FETCH_ASSOC);
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['accessPerformanceYN'];
        $hotelAccess = $attr['hotelAccess'];
        $response = $this->view->render($response, 'mappingV4.twig',
        [
            'session_user' => $_SESSION['user'],
            'access_allowed' => $accessAllowed,
            'account_types' => $at,
            'perfAccessYN' => $perfAccessYN ,
            'hotelAccess' => $hotelAccess,
        ]
        );

    })->setName('mapping');

    $app->get('owners',function ($request, $response, array $args){
        $sql = "select EID from AR_EID_ACCESS
                    where function = 'OWNERS'
                    and active_yn = 'Y'
                    and UPPER(eid) = UPPER(:eid)  ";
            $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $stmt->bindParam(':eid', $_SESSION['user'], PDO::PARAM_STR);
            $stmt->execute();
            $access = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $accessAllowed = $access ? 'Y' : 'N';
            $attr = $request->getAttribute('attr');
            $perfAccessYN = $attr['accessPerformanceYN'];
            $hotelAccess = $attr['hotelAccess'];
            $response = $this->view->render($response, 'owners.twig',
            [
                'session_user' => $_SESSION['user'],
                'access_allowed' => $accessAllowed,
                'perfAccessYN' => $perfAccessYN ,
                'hotelAccess' => $hotelAccess,
            ]
            );
        })->setName('owners');

    $app->get('extract_dates',function ( $request,  $response, array $args){
        $sql = "select * from AR_EXTRACT_DATE ";
        $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute();
        $extract_dates = $stmt->fetchAll(PDO::FETCH_OBJ);
        /**
         * Check if all CSV files were received.
         * If not received the status will be STALE
         */
        $sqlStatus = "select count(*) total_stale from AR_EXTRACT_DATE where status = 'STALE' ";
        $stmtStatus = $this->db->prepare($sqlStatus, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmtStatus->execute();
        $staleCount = $stmtStatus->fetchAll(PDO::FETCH_OBJ);
        /**
         * Check if MV refresh was succesfull
         */
        $sqlMV =    "select status
                    from ALL_SCHEDULER_JOB_RUN_DETAILS
                    where job_name = 'REFRESH_AR_AGING_DETAILED_MV'
                    and log_date = (
                    select max(log_date) from ALL_SCHEDULER_JOB_RUN_DETAILS
                    where job_name = 'REFRESH_AR_AGING_DETAILED_MV')
                    ";
        $stmtMV = $this->db->prepare($sqlMV, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmtMV->execute();
        $staleMV = $stmtMV->fetchAll(PDO::FETCH_OBJ);

        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['accessPerformanceYN'];
        $hotelAccess = $attr['hotelAccess'];
            $response = $this->view->render($response, 'extract_dates.twig',
            [
                'extract_dates' => $extract_dates,
                'staleCount' => $staleCount[0],
                'staleMV' => $staleMV[0],
                'perfAccessYN' => $perfAccessYN ,
                'hotelAccess' => $hotelAccess,
            ]);

    })->setName('extract_dates');

})->add(function ($request, $response, $next) {
    $controller = new App\Controllers\MainController($this);
    $perfAccessYN = $controller->hasAccess($_SESSION['user'],'PERFORMANCE');
    $hotelAccess = $controller->hasAccess($_SESSION['user'],'HOTEL');
    $attr['accessPerformanceYN'] = $perfAccessYN;
    $attr['hotelAccess'] = $hotelAccess;
    $request = $request->withAttribute('attr',$attr);
    if (isset($_SESSION['userID'])) {
            $response = $next($request, $response);
    } else {
        return $response->withRedirect($this->router->pathFor('signin'));
    }
    return $response;
});

$app->group('/performance', function () use ($app) {

    $app->get('/eid_performance',function ( $request,  $response, array $args){
        /*Fetch EID codes from ar.ar_account_user_map */
        $sql = "select eid,count(*) from ar_account_user_map group by eid   ";
        $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute();
        $eids = $stmt->fetchAll(PDO::FETCH_OBJ);
        /* */
        /*Fetch target config types from ar.ar_configuration */
        $sqlTarget = "select  code, description from ar_configuration where config_type = 'TARGET' order by 2  ";
        $stmtTarget = $this->db->prepare($sqlTarget, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmtTarget->execute();
        $targetTypes = $stmtTarget->fetchAll(PDO::FETCH_OBJ);
        /* */
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['perfAccessYN'];
        $hotelAccess = $attr['hotelAccess'];
    $response = $this->view->render($response, 'eid_perf.twig',[
        'perfAccessYN' => $perfAccessYN ,
        'eids' => $eids,
        'targetTypes' => $targetTypes,
        'hotelAccess' => $hotelAccess,
        ]);
    })->setName('eid_performance');

    $app->get('/eid_aging',function ( $request,  $response, array $args){
        /*Fetch EID codes from ar.ar_account_user_map */
        $sql = "select eid,count(*) from ar_account_user_map group by eid   ";
        $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute();
        $eids = $stmt->fetchAll(PDO::FETCH_OBJ);
        /* */
        /*Fetch target config types from ar.ar_configuration */
        $sqlTarget = "select  code, description from ar_configuration where config_type = 'TARGET' order by 2  ";
        $stmtTarget = $this->db->prepare($sqlTarget, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmtTarget->execute();
        $targetTypes = $stmtTarget->fetchAll(PDO::FETCH_OBJ);
        /* */

        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['perfAccessYN'];
        $hotelAccess = $attr['hotelAccess'];
        $response = $this->view->render($response, 'eid_aging.twig',[
            'perfAccessYN' => $perfAccessYN ,
            'eids' => $eids,
            'targetTypes' => $targetTypes  ,
            'hotelAccess' => $hotelAccess,
            ]);
    })->setName('eid_aging');
    // $app->get('/resort_aging',function ( $request,  $response, array $args){
    //     $attr = $request->getAttribute('attr');
    //     $perfAccessYN = $attr['perfAccessYN'];
    //     $hotelAccess = $attr['hotelAccess'];
    //     $response = $this->view->render($response, 'resort_aging.twig',[
    //         'perfAccessYN' => $perfAccessYN ,
    //         'hotelAccess' => $hotelAccess,
    //         ]);
    // })->setName('resort_aging');
    $app->get('/ssc_aging',function ( $request,  $response, array $args){
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['perfAccessYN'];
        $hotelAccess = $attr['hotelAccess'];
        $sql = "select eid,count(*) from ar_account_user_map group by eid   ";
        $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute();
        $eids = $stmt->fetchAll(PDO::FETCH_OBJ);

        $sqlResorts = "select CODE,DESCRIPTION from AR_CONFIGURATION WHERE CONFIG_TYPE = 'RESORT_NAME'  ";
        $stmtResorts = $this->db->prepare($sqlResorts, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmtResorts->execute();
        $resorts = $stmtResorts->fetchAll(PDO::FETCH_OBJ);
        $response = $this->view->render($response, 'ssc_aging.twig',[
            'eids' => $eids,
            'resorts' => $resorts,
            'perfAccessYN' => $perfAccessYN ,
            'hotelAccess' => $hotelAccess,
        ]);

    })->setName('ssc_aging');

    $app->get('/aging_template',function ( $request,  $response, array $args){
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['perfAccessYN'];
        $hotelAccess = $attr['hotelAccess'];
        $sql = "select eid,count(*) from ar_account_user_map group by eid   ";
        $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute();
        $eids = $stmt->fetchAll(PDO::FETCH_OBJ);

        $sqlResorts = "select CODE,DESCRIPTION from AR_CONFIGURATION WHERE CONFIG_TYPE = 'RESORT_NAME'  ";
        $stmtResorts = $this->db->prepare($sqlResorts, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmtResorts->execute();
        $resorts = $stmtResorts->fetchAll(PDO::FETCH_OBJ);
        $response = $this->view->render($response, 'aging_template.twig',[
            'eids' => $eids,
            'resorts' => $resorts,
            'perfAccessYN' => $perfAccessYN ,
            'hotelAccess' => $hotelAccess,
        ]);

    })->setName('aging_template');



    $app->get('/aging_templateTest/{RESORT}/{SNAPSHOT_DATE}',function ( $request,  $response, array $args){

        $sql = "SELECT *  FROM AR_AGING_TEMP_SNAPSHOTS WHERE RESORT = :RESORT";

        $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->bindParam(':RESORT', $args['RESORT'], PDO::PARAM_STR);
        $stmt->bindParam(':SNAPSHOT_DATE', $args['SNAPSHOT_DATE'], PDO::PARAM_STR);
        $stmt->execute();
        $report = $stmt->fetchAll(PDO::FETCH_ASSOC);
        var_dump($report);
        // $response->write(json_encode($sql));
        // return $response
        //     ->withHeader('Content-type', 'application/json')
        //     ;
    });

    $app->get('/aging_published',function ( $request,  $response, array $args){
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['perfAccessYN'];
        $hotelAccess = $attr['hotelAccess'];
        $hotel_code = $attr['hotel_code'];
        $hotel_desc = $attr['hotel_desc'];
        $user = $_SESSION['user'];


        $sqlResorts = "select CODE,DESCRIPTION from AR_CONFIGURATION WHERE CONFIG_TYPE = 'RESORT_NAME'  ";
        $stmtResorts = $this->db->prepare($sqlResorts, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmtResorts->execute();
        $resorts = $stmtResorts->fetchAll(PDO::FETCH_OBJ);
        $response = $this->view->render($response, 'aging_published.twig',[
            'resorts' => $resorts,
            'perfAccessYN' => $perfAccessYN ,
            'hotelAccess' => $hotelAccess,
            'hotel_code' => $hotel_code,
            'hotel_desc' => $hotel_desc,
            'user' => $user,
            'hotels' => $attr['hotels'],
            // 'snapshots' => $snapshots
        ]);

    })->setName('aging_published');


    $app->get('/credit_meeting_minutes',function ( $request,  $response, array $args){
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['perfAccessYN'];
        $hotelAccess = $attr['hotelAccess'];
        $hotel_code = $attr['hotel_code'];
        $hotel_desc = $attr['hotel_desc'];



        $sql = "select CODE,DESCRIPTION from AR_CONFIGURATION WHERE CONFIG_TYPE = 'RESORT_NAME'  ";
        $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute();
        $resorts = $stmt->fetchAll(PDO::FETCH_OBJ);

        $response = $this->view->render($response, 'credit_meeting_minutes.twig',[
            'resorts' => $resorts,
            'perfAccessYN' => $perfAccessYN ,
            'hotelAccess' => $hotelAccess,
            'hotel_code' => $hotel_code,
            'hotel_desc' => $hotel_desc,
            'hotels' => $attr['hotels'],

        ]);

    })->setName('credit_meeting_minutes');


    $app->get('/credit_meeting_minutes/print',function ( $request,  $response, array $args){
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['perfAccessYN'];
        $hotelAccess = $attr['hotelAccess'];
        $hotel_code = $attr['hotel_code'];
        $hotel_desc = $attr['hotel_desc'];



        $sql = "select CODE,DESCRIPTION from AR_CONFIGURATION WHERE CONFIG_TYPE = 'RESORT_NAME'  ";
        $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute();
        $resorts = $stmt->fetchAll(PDO::FETCH_OBJ);

        $response = $this->view->render($response, 'credit_minutes_print.twig',[
            'resorts' => $resorts,
            'perfAccessYN' => $perfAccessYN ,
            'hotelAccess' => $hotelAccess,
            'hotel_code' => $hotel_code,
            'hotel_desc' => $hotel_desc,
            'hotels' => $attr['hotels'],

        ]);

    })->setName('credit_meeting_minutes');

    $app->get('/artest',function ( $request,  $response, array $args){
        $attr = $request->getAttribute('attr');
        $perfAccessYN = $attr['perfAccessYN'];
        $hotelAccess = $attr['hotelAccess'];
        $hotel_code = $attr['hotel_code'];
        $hotel_desc = $attr['hotel_desc'];
        // var_dump($attr['hotels']);




        $sql = "select CODE,DESCRIPTION from AR_CONFIGURATION WHERE CONFIG_TYPE = 'RESORT_NAME'  ";
        $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute();
        $resorts = $stmt->fetchAll(PDO::FETCH_OBJ);

        $response = $this->view->render($response, 'test.twig',[
            'resorts' => $resorts,
            'perfAccessYN' => $perfAccessYN ,
            'hotelAccess' => $hotelAccess,
            'hotel_code' => $hotel_code,
            'hotel_desc' => $hotel_desc,
            'attrs' => $attr,
            'hotels' => $attr['hotels'],

        ]);

    })->setName('credit_meeting_minutes');

})->add(function ($request, $response, $next) {
    $controller = new App\Controllers\MainController($this);
    $perfAccessYN = $controller->hasAccess($_SESSION['user'],'PERFORMANCE');
    $hotelAccess = $controller->hasAccess($_SESSION['user'],'HOTEL');
    $hotel_code = $controller->getHotel($_SESSION['user'])['HOTEL_CODE'];
    $hotel_desc = $controller->getHotel($_SESSION['user'])['HOTEL_DESC'];
    $hotels = $controller->getHotelTest($_SESSION['user']);
    $attr['perfAccessYN'] = $perfAccessYN;
    $attr['hotelAccess'] = $hotelAccess;
    $attr['hotel_code'] = $hotel_code;
    $attr['hotel_desc'] = $hotel_desc;
    $attr['hotels'] = $hotels;
    $request = $request->withAttribute('attr',$attr);
    if (isset($_SESSION['userID'])) {
        if ($perfAccessYN == 'Y') {
            $response = $next($request, $response);
        } else {
            $controller->addFlash('accessDenied','Unfortunately you do not have access to the Performance Dashboard. If you believe that you should have access, please contact your administrator.');
            return $response->withRedirect($this->router->pathFor('access_denied'));
        }
    } else {
        return $response->withRedirect($this->router->pathFor('signin'));
    }
    return $response;
});


$app->get('/access_denied',function ( $request,  $response, array $args){
    $response = $this->view->render($response, 'access_denied.twig');
})->setName('access_denied');;


$app->get('/uploadfile',function ( $request,  $response, array $args){
    $response = $this->view->render($response, 'uploadfile.twig');
});

$app->post('/uploadfile', 'App\Controllers\MainController:uploadfile')->setName('uploadfile');


// $app->get('/level1',function ($request, $response, array $args){
//     if (isset($_SESSION['userID'])) {
//         $response = $this->view->render($response, 'level1Subtotals.twig');
//     } else {
//         return $response->withRedirect($this->router->pathFor('signin'));
//     }
// })->setName('level1');

// $app->get('/level2',function ($request, $response, array $args){
//     if (isset($_SESSION['userID'])) {
//     $response = $this->view->render($response, 'level2SubtotalsNew.twig');
//     } else {
//         return $response->withRedirect($this->router->pathFor('signin'));
//     }
// })->setName('level2');


// $app->get('/level3',function ($request,  $response, array $args){
//     if (isset($_SESSION['userID'])) {
//     $response = $this->view->render($response, 'level3Subtotals.twig');
//     } else {
//         return $response->withRedirect($this->router->pathFor('signin'));
//     }
// })->setName('level3');


// $app->get('/level4',function ( $request,  $response, array $args){
//     $sql = "select eid,count(*) from ar_account_user_map group by eid   ";
//     $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
//     $stmt->execute();
//     $eids = $stmt->fetchAll(PDO::FETCH_OBJ);

//     if (isset($_SESSION['userID'])) {
//     $response = $this->view->render($response, 'level4test.twig',
//     [
//         'eids' => $eids,
//         'session_user' => $_SESSION['user']
//     ]);
//     } else {
//         return $response->withRedirect($this->router->pathFor('signin'));
//     }
// })->setName('level4');




// $app->get('/mapping',function ($request, $response, array $args){
//     $sql = "select EID from AR_EID_ACCESS
//             where function = 'MAPPING'
//             and active_yn = 'Y'
//             and UPPER(eid) = UPPER(:eid)  ";
//     $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
//     $stmt->bindParam(':eid', $_SESSION['user'], PDO::PARAM_STR);
//     $stmt->execute();
//     $access = $stmt->fetchAll(PDO::FETCH_ASSOC);
//     $accessAllowed = $access ? 'Y' : 'N';


//     $sqlat = "  select  code,description
//                 from ar_configuration
//                 where config_type = 'ACCOUNT_TYPE'  ";
//     $stmtat = $this->db->prepare($sqlat, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
//     $stmtat->execute();
//     $at = $stmtat->fetchAll(PDO::FETCH_ASSOC);

//     if (isset($_SESSION['userID'])) {
//         $response = $this->view->render($response, 'mappingV4.twig',
//     [
//         'session_user' => $_SESSION['user'],
//         'access_allowed' => $accessAllowed,
//         'account_types' => $at
//     ]
//     );
//     } else {
//         return $response->withRedirect($this->router->pathFor('signin'));
//     }
// })->setName('mapping');

// $app->get('/owners',function ($request, $response, array $args){
// $sql = "select EID from AR_EID_ACCESS
//             where function = 'OWNERS'
//             and active_yn = 'Y'
//             and UPPER(eid) = UPPER(:eid)  ";
//     $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
//     $stmt->bindParam(':eid', $_SESSION['user'], PDO::PARAM_STR);
//     $stmt->execute();
//     $access = $stmt->fetchAll(PDO::FETCH_ASSOC);
//     $accessAllowed = $access ? 'Y' : 'N';


//     if (isset($_SESSION['userID'])) {
//         $response = $this->view->render($response, 'owners.twig',
//     [
//         'session_user' => $_SESSION['user'],
//         'access_allowed' => $accessAllowed
//     ]
//     );
//     } else {
//         return $response->withRedirect($this->router->pathFor('signin'));
//     }
// })->setName('owners');


// $app->get('/extract_dates',function ( $request,  $response, array $args){

//     $sql = "select * from AR_EXTRACT_DATE ";
//     $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
//     $stmt->execute();
//     $extract_dates = $stmt->fetchAll(PDO::FETCH_OBJ);

//     $sqlStatus = "select count(*) total_stale from AR_EXTRACT_DATE where status = 'STALE' ";
//     $stmtStatus = $this->db->prepare($sqlStatus, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
//     $stmtStatus->execute();
//     $staleCount = $stmtStatus->fetchAll(PDO::FETCH_OBJ);
//     if (isset($_SESSION['userID'])) {
//         $response = $this->view->render($response, 'extract_dates.twig',
//         [
//             'extract_dates' => $extract_dates,
//             'staleCount' => $staleCount[0]
//         ]);
//     } else {
//         return $response->withRedirect($this->router->pathFor('signin'));
//     }
// })->setName('extract_dates');




//Testing Routes//



$app->get('/test','App\Controllers\MainController:login')->setName('test');

$app->get('/testlevel3',function($request,$response,array $args) {
    $sql = "select * from AR_AGING_L3_COMMS  where 1 = 1  ";
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    $stmt->execute();
    $invoices = $stmt->fetchAll(PDO::FETCH_ASSOC);
    var_dump($invoices );

});


$app->get('/level4Subtotals',function ( $request,  $response, array $args){
    $response = $this->view->render($response, 'level4Subtotals.twig');
});

$app->get('/level3Subtotals',function ( $request,  $response, array $args){
    $response = $this->view->render($response, 'level3Subtotals.twig');
});






$app->get('/level4ajax',function ( $request,  $response, array $args){
    // $invoices = $this->db->query("select * from AR_AGING_LEVEL4  ")->fetchAll(PDO::FETCH_OBJ);
    // $totals = $this->db->query("select * from AR_TOTALS  ")->fetchAll(PDO::FETCH_OBJ);
    $response = $this->view->render($response, 'level4ajax.twig');
});

$app->get('/level5[/{resort}]',function ( $request,  $response, array $args){
        $sql = "select * from AR_AGING_LEVEL4  where 1 = 1  ";
    if ( $args['resort'] ) {$sql = $sql .  "  and  resort = :resort "; }
    $stmt = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    if ( $args['resort']  ) {$stmt->bindParam(':resort', $args['resort'], PDO::PARAM_STR);}
    $stmt->execute();
    $invoices = $stmt->fetchAll(PDO::FETCH_OBJ);

    $comments = $this->db->query("select * from AR_COMMENTS where detail_level = 4 ")->fetchAll(PDO::FETCH_OBJ);
    $response = $this->view->render($response, 'level5.twig',[
        'invoices' => $invoices,
        'comments' => $comments

    ]
);
});


// $app->get('/level1',function ($request,$response, array $args){
//     $invoices = $this->db->query("select * from AR_AGING_LEVEL1 ")->fetchAll(PDO::FETCH_OBJ);
//     $response = $this->view->render($response, 'level1.twig',[
//         'invoices' => $invoices

//     ]);
// })->setName('level1');







// $app->get('/level2',function ( $request,  $response, array $args){
//     $invoices = $this->db->query("select * from AR_AGING_LEVEL2 ")->fetchAll(PDO::FETCH_OBJ);
//     // $totals = $this->db->query("select * from AR_TOTALS  ")->fetchAll(PDO::FETCH_OBJ);
//     $comments = $this->db->query("select * from AR_COMMENTS where detail_level = 2 ")->fetchAll(PDO::FETCH_OBJ);
//     $response = $this->view->render($response, 'level2.twig',[
//         'invoices' => $invoices,
//         'comments' => $comments,
//     ]);
// })->setName('level2');





 ?>