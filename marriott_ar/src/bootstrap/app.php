<?php


ini_set('max_execution_time', 300);
session_cache_limiter(false);
session_start();

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Controllers\MainController;
use App\Models\HConnect;
use Slim\Middleware\SessionCookie;



$config['displayErrorDetails'] = false;
$config['addContentLengthHeader'] = false;
$tns = '(DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = HCOMM-XE)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = XE)
    )
  )';

$config['db']['host']       = '197.85.190.141';
$config['db']['user']       = 'ar';
$config['db']['pass']       = 'ar';
$config['db']['dbname']  = 'HSDEMO';
$config['db']['sid']         = 'oci:dbname=' . $tns;
$config['hconnect']['HCONNECT_HOST'] = 'http://197.85.184.59';
$config['hconnect']['HCONNECT_USER'] = 'LmarriottAR';
$config['hconnect']['HCONNECT_PASS'] = 'I300k1ng5AR';
// $config['hconnect']['HCONNECT_USER'] = 'onlinebookings';
// $config['hconnect']['HCONNECT_PASS'] = 'I300k1ng5';

$app = new \Slim\App(['settings' => $config]);

// $app->add(new SessionCookie(array(
//     'expires' => '20 minutes',
//     'path' => '/',
//     'domain' => null,
//     'secure' => false,
//     'httponly' => false,
//     'name' => 'slim_session',
//     'secret' => 'CHANGE_ME',
//     'cipher' => MCRYPT_RIJNDAEL_256,
//     'cipher_mode' => MCRYPT_MODE_CBC
// )));
$container = $app->getContainer();

// Register provider
$container['flash'] = function ($container) {
    return new \Slim\Flash\Messages();
};



$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler('../../logs/app.log');
    $logger->pushHandler($file_handler);
    return $logger;
};

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO($db['sid'] , $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->setAttribute(PDO::ATTR_TIMEOUT , 60);
    return $pdo;
};

$container['csrf'] = function ($container) {
    
    $guard = new \Slim\Csrf\Guard;
    $guard->setPersistentTokenMode(true);
    return $guard;
};





// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../resources/views');

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $basePath = rtrim(str_ireplace('index.php','',$container['request']->getUri()->getBasePath()),'/');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $basePath));

    $view->getEnvironment()->addGlobal('flash',$container['flash']);

    return $view;
};


$app->add(new \App\Middleware\CsrfViewMiddleware($container));
$app->add($container->csrf);



require __DIR__ . '/../routes/web.php';
require __DIR__ . '/../routes/api.php';




?>