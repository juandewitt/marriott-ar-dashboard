testArray = [];
setDate();
setDates();

/**
 * Individual/Hotel Aging
 * When clicking on Save Changes in Edit Targets modal
 * 
 */
$('#saveTargetsBtn').click(function () {

  if ($('#target_eid').val() != 'ALL' &&
      $('#DIM4').val() >= 0 && $('#DIM4').val() <= 100 &&
      $('#DIM3').val() >= 0 && $('#DIM3').val() <= 100 &&
      $('#DIM2').val() >= 0 && $('#DIM2').val() <= 100 &&
      $('#DIM1').val() >= 0 && $('#DIM1').val() <= 100 
   ) {
    $("#target_flash").css({ "display": "none" });
    console.log($("#targetsForm").serializeArray());
    $.post( "https://marriottssc.secure-app.co.za/api/updateTargets/"  + $("#target_eid").val() + "/" + $("#target_type").val() ,
    $("#targetsForm").serializeArray() ,
    function(data) {
      console.log(data);
    }
    )
    .done(function( msg ) {
      console.log(msg);
      updateEIDAgingPerc();
      $('#performanceInputEditModal').modal('hide');

      
    })
    .fail(function(error) {
      
    })
    .always(function(data,status,xhr) {
      console.log(data);
      console.log(status);
      console.log(xhr);
    })
    ;
   } else {
    $("#target_flash").css({ "display": "block" });
   }
 
 
  
  });


$('#editTargetsBtn').click(function () {
  $('#performanceInputEditModal').modal('show');
})
$('#editTargetsModalBtn').click(function () {
  $('#performanceInputEditModal').modal('show');
})

$('#dim_aging').change(function() {
  updateEIDAging();
  updateEIDAgingPerc()
})

$('#target_eid').change(function() {
  updateEIDTargets();
})

$('#target_type').change(function() {
  console.log("target changes");
  console.log($("#target_type").val());

  // $.post( "https://marriottssc.secure-app.co.za/api/getEidTargets",  {} ,
  // function(data) {
  //   console.log(data);
  // })
  // .done(function( data ) {

  // })
  // .fail(function(error,result) {
  //   console.log(error);
  //   console.log(result);
  // })
  // .always(function(data,status,result) {
  //   console.log(data);
  //   console.log(result);

  // });




  $( ".user-config" ).remove();
  if ($("#target_type").val() == 'INDIVIDUAL_AGING' || $("#target_type").val() == 'MARRIOTT_AGING_POLICY' ) {
    $( "#input_targets" ).append( 
      
      '<div class="col-sm-4 form-group user-config">\
        <div class="form-group">\
        <label for="DIM1" class="col-form-label">0 - 30 Days </label>\
        <input type="number" class="form-control"  id="DIM1" name="DIM1"  value="" min="0" max="100"  >\
        </div>\
    </div>  \
    <div class="col-sm-4 form-group user-config">\
        <div class="form-group">\
        <label for="DIM2" class="col-form-label">31 - 60 Days </label>\
        <input type="number" class="form-control"  id="DIM2" name="DIM2"   value="" min="0" max="100"   >\
        </div>\
    </div> \
    <div class="col-sm-4 form-group user-config">\
        <div class="form-group">\
        <label for="DIM3" class="col-form-label">61 - 90 Days </label>\
        <input type="number" class="form-control"  id="DIM3"  name="DIM3" value="" min="0" max="100"   >\
        </div>\
    </div>\
    <div class="col-sm-4 form-group user-config">\
        <div class="form-group">\
        <label for="DIM4" class="col-form-label">91 - Older</label>\
        <input type="number" class="form-control"  id="DIM4" name="DIM4"  value=""  min="0" max="100"  >\
        </div>\
    </div> ');
  }
  updateEIDTargets();
})

function DIM1(target) {
  return target.TARGET_CODE === 'DIM1';
}
function DIM2(target) {
  return target.TARGET_CODE === 'DIM2';
}
function DIM3(target) {
  return target.TARGET_CODE === 'DIM3';
}
function DIM4(target) {
  return target.TARGET_CODE === 'DIM4';
}

/**
 * 
 */

 

function updateEIDTargets() {
  $.get( "https://marriottssc.secure-app.co.za/api/eid_targets/" + $("#target_eid").val() + "/" + $("#target_type").val(),
        function(data) {
        }
        )
        .done(function( data ) {
         if(data.length > 0) {
          console.log(data.filter(DIM1)[0].TARGET_VALUE);
          $("#DIM1").val(data.filter(DIM1)[0].TARGET_VALUE);
           $("#DIM2").val(data.filter(DIM2)[0].TARGET_VALUE);
           $("#DIM3").val(data.filter(DIM3)[0].TARGET_VALUE);
           $("#DIM4").val(data.filter(DIM4)[0].TARGET_VALUE);
         } else {
          $("#DIM1").val('');
          $("#DIM2").val('');
          $("#DIM3").val('');
          $("#DIM4").val('');
         }
          
          })
         .fail(function(data) {

         } ) 
         .always(function(data) {
           console.log(data);
         } ) 
}


  

$.fn.dataTable.render.percentage = function (  ) {
  return function ( data, type, row ) {
      // console.log(data + "%");
      return data + "%";
  };
};   

if (document.querySelector("table[data-id-table=eid_perf_table]")) {

  var margin = {left:100, right:10, top:10, bottom:100};
  var width = 600 - margin.left - margin.right;
  var height  = 400 - margin.top - margin.bottom;
  var barPadding = .2;
  var axisTicks = {qty: 5, outerSize: 0, dateFormat: '%m-%d'};
  var t = d3.transition().duration(1000);

  var tooltip = d3.select("body").append("div")	
                         .attr("class", "tooltip")				
                         .style("opacity", 0);
  
  /*Graph 1 */
  var g1 = d3.select("#eid_perf_chart1")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
    /*Graph 1 */

     /*Graph 2 */
  var g2 = d3.select("#eid_perf_chart2")
  .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
        .append("g")
          .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
  /*Graph 2 */

  var xScale0 = d3.scaleBand().range([0, width ]).padding(barPadding)
  /*Graph 1 */
  var xScale1 = d3.scaleBand()
  var yScale = d3.scaleLinear().range([height , 0])
  /*Graph 1 */

  /*Graph 2 */
  var xScale1_g2 = d3.scaleBand()
  var yScale_g2 = d3.scaleLinear().range([height , 0])
  /*Graph 2 */
 
  
/*Graph 1 */ 
  // Add the X Axis
  var xAxisGroupG1 = g1.append("g")
       .attr("class", "x g1-axis")
       .attr("transform", `translate(0,${height })`)
       
  // Add the Y Axis
  var yAxisGroupG1 = g1.append("g")
       .attr("class", "y g1-axis")
/*Graph 1 */ 


/*Graph 2 */ 
  // Add the X Axis
  var xAxisGroupG2 = g2.append("g")
       .attr("class", "x g2-axis")
       .attr("transform", `translate(0,${height })`)
       
  // Add the Y Axis
  var yAxisGroupG2 =g2.append("g")
       .attr("class", "y g2-axis")
/*Graph 2 */ 



console.log("eid_perf_table");
var $eid_perf_table = $('#eid_perf_table')
// console.log($eid_perf_table);
updateEIDPerformance(g1,g2);
}
if (document.querySelector("table[data-id-table=eid_aging_table]")) {
  // console.log("eid_aging_table");
  var $eid_aging_table = $('#eid_aging_table')
  // console.log($eid_aging_table);
  updateEIDAging();
}
if (document.querySelector("table[data-id-table=eid_aging_perc_table]")) {
  /*Define only once for entire page*/
  var margin = {left:80, right:10, top:20, bottom:80};
  var width = 340 - margin.left - margin.right;
  var height  = 330 - margin.top - margin.bottom;
  var barPadding = .2;
  var axisTicks = {qty: 5, outerSize: 0, dateFormat: '%m-%d'};
  var t = d3.transition().duration(1000);

  // X Scale
  var x = d3.scaleBand()
  .domain(["0 - 30 Days","31 - 60 Days","61 - 90 Days","91 - Older"])
  .range([0, width])
  .padding(0.2);
    
  // Y Scale
  var y = d3.scaleLinear()
      .domain([0,100])
      .range([height, 0]);

 
  /*Define only once for entire page*/


  // console.log("eid_aging_perc_table");
  var $eid_aging_perc_table = $('#eid_aging_perc_table')
  // console.log($eid_aging_perc_table);
  updateEIDAgingPerc();
}

if (document.querySelector("table[data-id-table=ssc_aging_table]")) {
  // console.log("ssc_aging_table");
  var $ssc_aging_table = $('#ssc_aging_table')
  // console.log($ssc_aging_table);
  updateSSCAging();
}

if (document.querySelector("table[data-id-table=aging_template]")) {
  var $aging_template = $('#aging_template')
  updateAgingTemplate();
}

if (document.querySelector("table[data-id-table=aging_published]")) {
  var $aging_published = $('#aging_published')
  getSnapshotDates();
  // updateAgingPublished();
}


if (document.querySelector("table[data-id-table=resort_aging_table]")) {
   /*Define only once for entire page*/
   var margin = {left:80, right:10, top:20, bottom:80};
   var width = 340 - margin.left - margin.right;
   var height  = 330 - margin.top - margin.bottom;
   var barPadding = .2;
   var axisTicks = {qty: 5, outerSize: 0, dateFormat: '%m-%d'};
   var t = d3.transition().duration(1000);
 
   // X Scale
   var x = d3.scaleBand()
   .domain(["0 - 30 days","31 - 60 days","61 - 90 days","91 - over"])
   .range([0, width])
   .padding(0.2);
     
   // Y Scale
   var y = d3.scaleLinear()
       .domain([0,100])
       .range([height, 0]);
 
  
   /*Define only once for entire page*/
  // console.log("resort_aging_table");
  var $resort_aging_table = $('#resort_aging_table')
  // console.log($resort_aging_table);
  updateResortAging();
}
if (document.querySelector("table[data-id-table=resort_aging_perc_table]")) {
  // console.log("resort_aging_perc_table");
  var $resort_aging_perc_table = $('#resort_aging_perc_table')
  // console.log($resort_aging_perc_table);
  updateResortPercAging();
}

function setDates() {
  var date = new Date();
  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
  var dd = String(firstDay.getDate()).padStart(2, '0');
  var mm = String(firstDay.getMonth() + 1).padStart(2, '0');
  var yyyy = firstDay.getFullYear();
  dateStr = yyyy + "-" + mm + "-" + dd ;
  $("#filter_from_date").val(dateStr);
  

  var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  var dd = String(lastDay.getDate()).padStart(2, '0');
  var mm = String(lastDay.getMonth() + 1).padStart(2, '0');
  var yyyy = lastDay.getFullYear();
  dateStr = yyyy + "-" + mm + "-" + dd ;
  $("#filter_to_date").val(dateStr);
  

  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();
  dateStr = yyyy + "-" + mm + "-" + dd ;
  $("#filter_from_date").attr("max",dateStr);
  $("#filter_to_date").attr("max",dateStr);

}



function setDate() {
  var date = new Date();
  var dd = String(date.getDate()).padStart(2, '0');
  var mm = String(date.getMonth() + 1).padStart(2, '0');
  var yyyy = date.getFullYear();
  dateStr = yyyy + "-" + mm + "-" + dd ;
  $("#filter_date").val(dateStr);
  $("#filter_date").attr("max",dateStr);
}





$('#filter_from_date').change(function() {
  // reportLoading.style.display = 'block';
  // console.log($(this).val());
  if (document.querySelector("table[data-id-table=eid_perf_table]")) {
    updateEIDPerformance(g1,g2);
    }
  if (document.querySelector("table[data-id-table=eid_aging_table]")) {
    updateEIDAging();
  }
  if (document.querySelector("table[data-id-table=eid_aging_perc_table]")) {
    updateEIDAgingPerc();
  }
  if (document.querySelector("table[data-id-table=ssc_aging_table]")) {
    updateSSCAging();
  }
  if (document.querySelector("table[data-id-table=resort_aging_table]")) {
    updateResortAging();
  }
  if (document.querySelector("table[data-id-table=resort_aging_perc_table]")) {
  updateResortPercAging();
}
  });

  $('#filter_to_date').change(function() {
    // reportLoading.style.display = 'block';
    // console.log($(this).val());
    if (document.querySelector("table[data-id-table=eid_perf_table]")) {
      updateEIDPerformance(g1,g2);
      }
    if (document.querySelector("table[data-id-table=eid_aging_table]")) {
      updateEIDAging();
    }
    if (document.querySelector("table[data-id-table=eid_aging_perc_table]")) {
      updateEIDAgingPerc();
    }
    if (document.querySelector("table[data-id-table=ssc_aging_table]")) {
      updateSSCAging();
    }
    if (document.querySelector("table[data-id-table=resort_aging_table]")) {
      updateResortAging();
    }
    if (document.querySelector("table[data-id-table=resort_aging_perc_table]")) {
    updateResortPercAging();
  }
    });

    $('#a').change(function() {
      updateEIDPerformance(g1,g2);
    })
    $('#b').change(function() {
      updateEIDPerformance(g1,g2);
    })
    $('#c').change(function() {
      updateEIDPerformance(g1,g2);
    })


  $('#filter_eid').change(function() {
    reportLoading.style.display = 'block';
    // console.log("filter_eid changed")
    // console.log($(this).val());
    if (document.querySelector("table[data-id-table=ssc_aging_table]")) {
      updateSSCAging();
    }
    });

  $('#filter_resort').change(function() {
    reportLoading.style.display = 'block';
    // console.log("filter_resort changed")
    // console.log($(this).val());
    if (document.querySelector("table[data-id-table=ssc_aging_table]")) {
      updateSSCAging();
    }
    if (document.querySelector("table[data-id-table=aging_template]")) {
      $('#aging_template').DataTable().clear()
    .draw();
      updateAgingTemplate();
    }
    
    });


    

function updateEIDPerformance(g1,g2) {

  $a = $("#a option:selected").val();
  $b = $("#b option:selected").val();
  $c = $("#c option:selected").val();
 
  if ($c != 'c_null') {
    g1_x_axis_text = $("#a option:selected").text() + " vs " + $("#b option:selected").text() + " vs " + $("#c option:selected").text();
  } else {
    g1_x_axis_text = $("#a option:selected").text() + " vs " + $("#b option:selected").text();
  }

  /*Graph 1 */
  //remove existing labels if any
  $( ".x.g1-axis-label" ).remove();
  $( ".y.g1-axis-label" ).remove();
  //X label
  g1.append("text")
    .attr("class", "x g1-axis-label")
    .attr("x",width/2)
    .attr("y", height + 80)
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .text(g1_x_axis_text);
  
  //Y label
  g1.append("text")
    .attr("class", "y g1-axis-label")
    .attr("x", -(height/2))  
    .attr("y", -60)
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .attr("transform", "rotate(-90)")
    .text("No of Invoices");
/*Graph 1 */

if ($c != 'c_null') {
  g2_x_axis_text = $("#a option:selected").text() + " vs " + $("#c option:selected").text() + " % & "  + $("#b option:selected").text() + " vs " + $("#c option:selected").text() + " % " ;
} else {
  g2_x_axis_text = $("#a option:selected").text() + " vs " + $("#b option:selected").text() + " %";
}

/*Graph 2 */
//remove existing labels if any
  $( ".x.g2-axis-label" ).remove();
  $( ".y.g2-axis-label" ).remove(); 
  //X label
  g2.append("text")
    .attr("class", "x g2-axis-label")
    .attr("x",width/2)
    .attr("y", height + 80)
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .text(g2_x_axis_text);
  
  //Y label
  g2.append("text")
    .attr("class", "y g2-axis-label")
    .attr("x", -(height/2))  
    .attr("y", -60)
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .attr("transform", "rotate(-90)")
    .text("%");
/*Graph 2 */

/*Graph 1 Legend */ 
var elementColor = d3.scaleOrdinal();
if ($c != 'c_null') {
  elementColor.domain([$("#a option:selected").text() , $("#b option:selected").text(),$("#c option:selected").text() ])
  .range(["rgb(154, 154, 249)","rgb(245, 199, 116)","rgb(245, 166, 166)"]);
  var keys =  [$("#a option:selected").text() , $("#b option:selected").text(), $("#c option:selected").text()];
} else {
  elementColor.domain([$("#a option:selected").text() , $("#b option:selected").text() ])
  .range(["rgb(154, 154, 249)","rgb(245, 199, 116)"]);  
  var keys =  [$("#a option:selected").text() , $("#b option:selected").text()];
}
 
var legend =   g1.append("g")   
          .attr("transform", "translate(-60"  + "," + (height + 25)  + ")")
 
// Add one dot in the legend for each name.
$( ".g1LegendDots" ).remove();
$( ".g1LegendText" ).remove();
var size = 10
legend.selectAll("mydots")
 .data(keys)
 .enter()
 .append("rect")
    .attr("class", "g1LegendDots")
    .attr("x", 0)
    .attr("y", function(d,i){ return 10 + i*(size+5)}) 
    .attr("width", size)
    .attr("height", size)
    .style("fill", function(d){ return elementColor(d)})

legend.selectAll("legend")  
 .data(keys)
 .enter()
 .append("text")
 .attr("class", "g1LegendText")
 .attr("x", 0 + size*1.2)
 .attr("y", function(d,i){ return 10 + i*(size+5) + (size/2)}) 
 .style("fill", function(d){ return elementColor(d)})
 .text(function(d){ return d})
 .attr("text-anchor", "left")
 .style("alignment-baseline", "middle")    
/*Graph 1 Legend */ 

/*Graph 2 Legend */ 
var g2_elementColor = d3.scaleOrdinal();
if ($c != 'c_null') {
  g2_elementColor.domain(["% of " + $("#a option:selected").text() + " vs " + $("#c option:selected").text() ,
  "% of " + $("#b option:selected").text() + " vs " + $("#c option:selected").text() ])
  .range(["rgb(154, 154, 249)","rgb(188, 188, 193)" ]); 
  var keys =  ["% of " + $("#a option:selected").text() + " vs " + $("#c option:selected").text() ,
  "% of " + $("#b option:selected").text() + " vs " + $("#c option:selected").text()
]; 
} else {
  g2_elementColor.domain(['% of ' + $("#a option:selected").text() + " vs " + $("#b option:selected").text()])
  .range(["rgb(154, 154, 249)"]);  
  var keys =  ['% of ' + $("#a option:selected").text() + " vs " + $("#b option:selected").text()];
}
 



var legend =   g2.append("g")   
          .attr("transform", "translate(-60"  + "," + (height + 25)  + ")")
 
// Add one dot in the legend for each name.
$( ".g2LegendDots" ).remove();
$( ".g2LegendText" ).remove();
var size = 10
legend.selectAll("mydots")
 .data(keys)
 .enter()
 .append("rect")
    .attr("class", "g2LegendDots")
    .attr("x", 0)
    .attr("y", function(d,i){ return 10 + i*(size+5)}) 
    .attr("width", size)
    .attr("height", size)
    .style("fill", function(d){ return g2_elementColor(d)})

legend.selectAll("legend")  
 .data(keys)
 .enter()
 .append("text")
 .attr("class", "g2LegendText")
 .attr("x", 0 + size*1.2)
 .attr("y", function(d,i){ return 10 + i*(size+5) + (size/2)}) 
 .style("fill", function(d){ return g2_elementColor(d)})
 .text(function(d){ return d})
 .attr("text-anchor", "left")
 .style("alignment-baseline", "middle")    
/*Graph 2 Legend */ 

  filter = {
    filter_from_date: $("#filter_from_date").val(),
    filter_to_date: $("#filter_to_date").val(),
    csrf_name: $("#getTokenName").val(),
    csrf_value:$("#getTokenValue").val() ,
    a: $("#a option:selected").val(),
    b: $("#b option:selected").val(),
    c: $("#c option:selected").val(),
  }
  // console.log(g1);

  $('#eid_perf_table').DataTable().destroy();
  $('#eid_perf_table tbody').empty();

  if ($c != 'c_null') {
    if ($('#fact3').length == 0) {
      $( "#fact2" ).after( "<th  id='fact3' scope='col' data-sortable='true' data-field='FACT3'>Metric C</th>" );
      $( "#percentage" ).after( "<th  id='percentage2' scope='col' data-sortable='true' data-field='PERCENTAGE2'>%</th>" );
    }
    
    columns = [{ "data": "DIM"  },
    { "data": "FACT1" },
    { "data": "FACT2" },
     { "data": "FACT3" },
    { "data": "PERCENTAGE" ,render: $.fn.dataTable.render.percentage()  },
    { "data": "PERCENTAGE2" ,render: $.fn.dataTable.render.percentage()  }
  ];
  } else {
    if ($('#fact3').length > 0) {
      $( "#fact3" ).remove();
      $( "#percentage2" ).remove();
          }
    columns = [{ "data": "DIM"  },
  { "data": "FACT1" },
  { "data": "FACT2" },
  { "data": "PERCENTAGE" ,render: $.fn.dataTable.render.percentage()  }];
  }
  
  $.post( "https://marriottssc.secure-app.co.za/api/eid_perf",  filter ,
  function(data) {
    console.log(data);
  })
  .done(function( data ) {
  // console.log(data);
  $('#eid_perf_table').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                extension: '.pdf',
                filename: 'AR Account Owners',
                title: 'AR Account Owners'
            },
            'copy',
            {
          extend: 'csvHtml5',
          title: 'AR Account Owners',
          filename: 'AR Account Owners'
      },
      {
          extend: 'excelHtml5',
          filename: 'AR Account Owners',
          title: 'AR Account Owners'
      },
              'print'
        ],
    "paging": false,
    select: 'single',
    searching: false,
    scrollCollapse: true,
       data: data,
       "columns": columns,
    } );

    if ($c != 'c_null') {
      $("#percentage").text( $("#a option:selected").text() + " vs " + $("#c option:selected").text() + " %"  );
      $("#percentage2").text( $("#b option:selected").text() + " vs " + $("#c option:selected").text() + " %"  );
    } else {
      $("#percentage").text( $("#a option:selected").text() + " vs " + $("#b option:selected").text() + " %"  );
    }
    $("#fact1").text($("#a option:selected").text());
    $("#fact2").text($("#b option:selected").text());
    $("#fact3").text($("#c option:selected").text());
    

$("#eid_perf_table_filter input").addClass( "form-control" );
$("#eid_perf_table_filter input").attr("placeholder","Search");
 
  data.forEach(d => {
    d.FACT1 = +d.FACT1;
    d.FACT2 = +d.FACT2;
    d.FACT3 = +d.FACT3;
    d.PERCENTAGE = +d.PERCENTAGE;
    d.PERCENTAGE2 = +d.PERCENTAGE2;
  });
 


var dim = data.map(function(d) {
  return d.DIM;
})

//graph data 
data.sort(function(a, b) {
  return d3.ascending(a.DIM, b.DIM)
})
xScale0.domain(data.map(d => d.DIM))

/*Graph 1 */ 
if ($c != 'c_null') {
  xScale1.domain(['FACT1', 'FACT2','FACT3']).range([0, xScale0.bandwidth()])
  yScale.domain([0, d3.max(data, d => Math.max(d.FACT1, d.FACT2, d.FACT3)) ]);
} else {
  xScale1.domain(['FACT1', 'FACT2']).range([0, xScale0.bandwidth()])
  yScale.domain([0, d3.max(data, d => Math.max(d.FACT1, d.FACT2))]);
}

/*Graph 1 */ 

/*Graph 2 */ 
if ($c != 'c_null') {
  xScale1_g2.domain(['PERCENTAGE','PERCENTAGE2']).range([0, xScale0.bandwidth()])
  yScale_g2.domain([0, d3.max(data, d => Math.max(d.PERCENTAGE,d.PERCENTAGE2))]);
} else {
  xScale1_g2.domain(['PERCENTAGE']).range([0, xScale0.bandwidth()])
  yScale_g2.domain([0, d3.max(data, d => d.PERCENTAGE)]);
}

/*Graph 2 */ 

  /*Graph 1 */
  var xAxisG1 = d3.axisBottom(xScale0).tickSizeOuter(axisTicks.outerSize);
  var yAxisG1 = d3.axisLeft(yScale).ticks(axisTicks.qty).tickSizeOuter(axisTicks.outerSize);
  
  /*Graph 1 */

  /*Graph 2 */
  var xAxisG2 = d3.axisBottom(xScale0).tickSizeOuter(axisTicks.outerSize);
  var yAxisG2 = d3.axisLeft(yScale_g2).ticks(axisTicks.qty).tickSizeOuter(axisTicks.outerSize);
  
  /*Graph 2 */

updateChart(data,xAxisG1,yAxisG1,xAxisG2,yAxisG2);
reportLoading.style.display = 'none';
    
  })
  .fail(function(error,result) {
    console.log(error);
    console.log(result);
  })
  .always(function(data,status,result) {
    console.log(data);
    console.log(result);

  });


}

function updateChart(data,xAxisG1,yAxisG1,xAxisG2,yAxisG2) { 

  
  xAxisGroupG1.transition(t).call(xAxisG1);
  yAxisGroupG1.transition(t).call(yAxisG1);
  xAxisGroupG2.transition(t).call(xAxisG2);
  yAxisGroupG2.transition(t).call(yAxisG2);

    
  //JOIN NEW DATA WITH OLD ELEMENTS
  /*Graph 1 */
  var DIM1 = g1.selectAll(".DIM1")
  .data(data , function(d) {
    return d.DIM
  });
  console.log( DIM1);

  //EXIT old elements not present in new data
  DIM1
  .exit()
  .remove();

  //UPDATE old elements present in new data
  var rectGroupsExisting  = DIM1
   .attr("transform", d => `translate(${xScale0(d.DIM)},0)`);

  //ENTER new elements
  var rectGroupsEnter = 
  DIM1
  .enter()
  .append("g")
  .attr("class", "DIM1")
  .attr("transform", d => `translate(${xScale0(d.DIM)},0)`);

 
  
  
  /* Add FACT 1 bars */

  var fact1Rect = rectGroupsEnter.selectAll(".FACT1")
  .data(d => [d]);

  var fact1RectExisting = rectGroupsExisting.selectAll(".FACT1")
  .data(d => [d]);


    //Remove elements no longer in data
    fact1RectExisting
      .exit()
      .attr("fill","red")
      // .transition(t)
      .attr("y", yScale(0))
      .attr("height", 0)
      .remove();  


    fact1RectExisting
    .transition(t)
    .attr("x", d => xScale1('FACT1'))
    .attr("y", d => yScale(d.FACT1))
    .attr("width", xScale1.bandwidth())
    .attr("height", d => {
      return height -  yScale(d.FACT1)
    });
    // console.log(fact1Rect);


    fact1Rect.enter()
    .append("rect")
    .attr("class", "FACT1")
  .style("fill","rgb(154, 154, 249)")
    .attr("x", d => xScale1('FACT1'))   
    .attr("width", xScale1.bandwidth())  
    .attr("y", yScale(0))
    .attr("height", 0)
    // .transition(t)
    .attr("y", d => yScale(d.FACT1))
    .attr("height", d => {
      return height -  yScale(d.FACT1)
    })
    .on("mouseover", function(d) {
      tooltip.transition()		
            .duration(200)	
            .style("background", "rgb(154, 154, 249)")
            .style("color", "rgb(12, 12, 214)")
            .style("opacity", .7);		
            tooltip.html(d.DIM + "<br/>Actioned<br/>"  + d.FACT1 )	
            .style("left", ((d3.event.pageX) + 10)  + "px")		
            .style("top", ((d3.event.pageY)  -50 )  + "px");	
        })
        .on("mouseout", function(d) {		
          tooltip.transition()		
                .duration(500)		
                .style("opacity", 0);	
        })	;
        // console.log(fact1Rect);



/* Add fact 2 bars */
     var fact2Rect =  rectGroupsEnter.selectAll(".FACT2")
    .data(d => [d]);
    console.log(fact2Rect);
    var fact2RectExisting = rectGroupsExisting.selectAll(".FACT2")
  .data(d => [d]);

  fact2RectExisting 
    .exit()
    .attr("fill","red")
      .transition(t)
      .attr("y", yScale(0))
      .attr("height", 0)
      .remove();

      fact2RectExisting 
    .transition(t)
    .attr("x", d => xScale1('FACT2'))
    .attr("y", d => yScale(d.FACT2))
    .attr("width", xScale1.bandwidth())
    .attr("height", d => {
      return height -  yScale(d.FACT2)
    }); 


    fact2Rect.enter()
    .append("rect")
    .attr("class", "FACT2")
  .style("fill","rgb(245, 199, 116)")
    .attr("x", d => xScale1('FACT2'))
    .attr("width", xScale1.bandwidth())
    .attr("y", yScale(0))
    .attr("height", 0)
    // .transition(t)
    .attr("y", d => yScale(d.FACT2))
    .attr("height", d => {
      return height -  yScale(d.FACT2)
    })
    .on("mouseover", function(d) {
      tooltip.transition()		
            .duration(200)	
            .style("background", "rgb(245, 199, 116)")
            .style("color", "rgb(255, 87, 34)")
            .style("opacity", .7);		
            tooltip.html(d.DIM + "<br/>Invoices<br/>"  + d.FACT2 )	
            .style("left", ((d3.event.pageX) + 10)  + "px")		
            .style("top", ((d3.event.pageY)  -50 )+ "px");	
        })
        .on("mouseout", function(d) {		
          tooltip.transition()		
                .duration(500)		
                .style("opacity", 0);	
        })
    ; 

    /* Add fact 3 bars */

      

if ($c != 'c_null') {
  var fact3Rect =  rectGroupsEnter.selectAll(".FACT3")
  .data(d => [d]);
  console.log(fact3Rect);
  var fact3RectExisting = rectGroupsExisting.selectAll(".FACT3")
  .data(d => [d]);
  console.log(fact3RectExisting);     
  
fact3RectExisting 
.exit()
.attr("fill","red")
  .transition(t)
  .attr("y", yScale(0))
  .attr("height", 0)
  .remove();

  fact3RectExisting 
.transition(t)
.attr("x", d => xScale1('FACT3'))
.attr("y", d => yScale(d.FACT3))
.attr("width", xScale1.bandwidth())
.attr("height", d => {
  return height -  yScale(d.FACT3)
}); 


fact3RectExisting.enter()
.append("rect")
.attr("class", "FACT3")
.style("fill","rgb(245, 166, 166)")
.attr("x", d => xScale1('FACT3'))
.attr("width", xScale1.bandwidth())
.attr("y", yScale(0))
.attr("height", 0)
// .transition(t)
.attr("y", d => yScale(d.FACT3))
.attr("height", d => {
  return height -  yScale(d.FACT3)
})
.on("mouseover", function(d) {
  tooltip.transition()		
        .duration(200)	
        .style("background", "rgb(245, 166, 166)")
        .style("color", "rgb(239, 14, 14)")
        .style("opacity", .7);		
        tooltip.html(d.DIM + "<br/>Invoices<br/>"  + d.FACT3 )	
        .style("left", ((d3.event.pageX) + 10)  + "px")		
        .style("top", ((d3.event.pageY)  -50 )+ "px");	
    })
    .on("mouseout", function(d) {		
      tooltip.transition()		
            .duration(500)		
            .style("opacity", 0);	
    })
; 
} else {
  $(".FACT3").remove();
}

/*Graph 1 */ 

/*Graph 2 */ 
   
  var DIM2 = g2.selectAll(".DIM2")
  .data(data , function(d) {
    return d.DIM
  });

     //EXIT old elements not present in new data
  DIM2
  .exit()
  .remove();

  //UPDATE old elements present in new data
  var rectGroupsExisting2 = DIM2
   .attr("transform", d => `translate(${xScale0(d.DIM)},0)`);

  //ENTER new elements
  var rectGroupsEnter2 = DIM2.enter().append("g")
  .attr("class", "DIM2")
  .attr("transform", d => `translate(${xScale0(d.DIM)},0)`);
  
    /* Add Percentage count bars */
    var PercentageRect =  rectGroupsEnter2.selectAll(".bar.PERCENTAGE")
    .data(d => [d]);
    var PercentageRectExisting =  rectGroupsExisting2.selectAll(".bar.PERCENTAGE")
    .data(d => [d]);

    PercentageRectExisting
    .exit()
    .attr("fill","red")
      .transition(t)
      .attr("y", yScale_g2(0))
      .attr("height", 0)
      .remove();

      PercentageRectExisting
    .transition(t)
    .attr("x", d => xScale1_g2('PERCENTAGE'))
    .attr("y", d => yScale_g2(d.PERCENTAGE))
    .attr("width", xScale1_g2.bandwidth())
    .attr("height", d => {
      return height -  yScale_g2(d.PERCENTAGE)
    }); 


    PercentageRect.enter()
    .append("rect")
    .attr("class", "bar PERCENTAGE")
  .style("fill","rgb(154, 154, 249)")
    .attr("x", d => xScale1_g2('PERCENTAGE'))
    .attr("width", xScale1_g2.bandwidth())
    .attr("y", yScale_g2(0))
    .attr("height", 0)
    // .transition(t)
    .attr("y", d => yScale_g2(d.PERCENTAGE))
    .attr("height", d => {
      return height -  yScale_g2(d.PERCENTAGE)
    })
    .on("mouseover", function(d) {
      tooltip.transition()		
            .duration(200)	
            .style("background", "rgb(154, 154, 249)")
            .style("color", "rgb(13, 152, 8)")
            .style("opacity", .7);		
            tooltip.html(d.DIM + "<br/>Actioned<br/>"  + d.PERCENTAGE + "%" )	
            .style("left", ((d3.event.pageX) + 10)  + "px")		
            .style("top", ((d3.event.pageY)  -50 )  + "px");	
        })
        .on("mouseout", function(d) {		
          tooltip.transition()		
                .duration(500)		
                .style("opacity", 0);	
        });

if ($c != 'c_null') {
  /* Add Percentage count bars */
  var PercentageRect2 =  rectGroupsEnter2.selectAll(".bar.PERCENTAGE2")
  .data(d => [d]);
  var PercentageRect2Existing =  rectGroupsExisting2.selectAll(".bar.PERCENTAGE2")
  .data(d => [d]);

  PercentageRect2Existing
  .exit()
  .attr("fill","red")
    .transition(t)
    .attr("y", yScale_g2(0))
    .attr("height", 0)
    .remove();

    PercentageRect2Existing
  .transition(t)
  .attr("x", d => xScale1_g2('PERCENTAGE2'))
  .attr("y", d => yScale_g2(d.PERCENTAGE2))
  .attr("width", xScale1_g2.bandwidth())
  .attr("height", d => {
    return height -  yScale_g2(d.PERCENTAGE2)
  }); 


  PercentageRect2Existing.enter()
  .append("rect")
  .attr("class", "bar PERCENTAGE2")
.style("fill","rgb(188, 188, 193)")
  .attr("x", d => xScale1_g2('PERCENTAGE2'))
  .attr("width", xScale1_g2.bandwidth())
  .attr("y", yScale_g2(0))
  .attr("height", 0)
  // .transition(t)
  .attr("y", d => yScale_g2(d.PERCENTAGE2))
  .attr("height", d => {
    return height -  yScale_g2(d.PERCENTAGE2)
  })
  .on("mouseover", function(d) {
    tooltip.transition()		
          .duration(200)	
          .style("background", "rgb(188, 188, 193)")
          .style("color", "rgb(13, 152, 8)")
          .style("opacity", .7);		
          tooltip.html(d.DIM + "<br/>Actioned<br/>"  + d.PERCENTAGE2 + "%" )	
          .style("left", ((d3.event.pageX) + 10)  + "px")		
          .style("top", ((d3.event.pageY)  -50 )  + "px");	
      })
      .on("mouseout", function(d) {		
        tooltip.transition()		
              .duration(500)		
              .style("opacity", 0);	
      });
} else {
  $(".PERCENTAGE2").remove();
}

/*Graph 2 */  
  }

//Update the EID Aging table and chart
function updateEIDAging() {

  $('#eid_aging_table').DataTable().destroy();
  if ( $("#filter_from_date").val().length == 0) {
    filter_from_date = '2000-01-01';
    console.log(filter_from_date);
  } else {
    filter_from_date = $("#filter_from_date").val();
  }

$.get( "https://marriottssc.secure-app.co.za/api/eid_aging/" + filter_from_date + "/" + $("#filter_to_date").val() + "/" + $("#dim_aging").val() ,
function(data) {
  ownerData = data;
  console.log(data);
  reportLoading.style.display = 'none';

  $('#eid_aging_table').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                extension: '.pdf',
                filename: 'AR Account Owners',
                title: 'AR Account Owners'
            },
            'copy',
            {
          extend: 'csvHtml5',
          title: 'AR Account Owners',
          filename: 'AR Account Owners'
      },
      {
          extend: 'excelHtml5',
          filename: 'AR Account Owners',
          title: 'AR Account Owners'
      },
              'print'
        ],
    "paging": false,
    select: 'single',
    searching: false,
    scrollCollapse: true,
       data: ownerData,
       "columns": [
    { "data": "DIM"  },
    { "data": "AGE_BUCKET1" ,render: $.fn.dataTable.render.number( ',', '.', 2 )},
    { "data": "AGE_BUCKET2" ,render: $.fn.dataTable.render.number( ',', '.', 2 )},
    { "data": "AGE_BUCKET3" ,render: $.fn.dataTable.render.number( ',', '.', 2 )},
    { "data": "AGE_BUCKET456" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
    { "data": "TOTAL" ,render: $.fn.dataTable.render.number( ',', '.', 2 )  },
  ],
    } );

    $("#eid_aging_table_filter input").addClass( "form-control" );
$("#eid_aging_table_filter input").attr("placeholder","Search");

}
)
.done(function(data) {
          reportLoading.style.display = 'none';
          //draw the graph using the data from ajax call

        })
.fail(function(xhr, status, error) {
          reportLoading.style.display = 'none';
        })
.always(function(xhr, status, error) {
  console.log(xhr);

        })

}

/**
 * Update the EID Aging Percentage table and chart
 *  */
function updateEIDAgingPerc() {
  $('#eid_aging_perc_table').DataTable().destroy();
  if ( $("#filter_from_date").val().length == 0) {
    filter_from_date = '2000-01-01';
    console.log(filter_from_date);
  } else {
    filter_from_date = $("#filter_from_date").val();
  }

$.get( "https://marriottssc.secure-app.co.za/api/eid_aging_perc/" + filter_from_date + "/" + $("#filter_to_date").val() + "/" + $("#dim_aging").val(),
function(data) {
  ownerData = data;
  // console.log(data);


  reportLoading.style.display = 'none';

  $('#eid_aging_perc_table').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                extension: '.pdf',
                filename: 'AR Account Owners',
                title: 'AR Account Owners'
            },
            'copy',
            {
          extend: 'csvHtml5',
          title: 'AR Account Owners',
          filename: 'AR Account Owners'
      },
      {
          extend: 'excelHtml5',
          filename: 'AR Account Owners',
          title: 'AR Account Owners'
      },
              'print'
        ],
    "paging": false,
    select: 'single',
    searching: false,
    scrollCollapse: true,
       data: ownerData,
       "columns": [
    { "data": "DIM"  },
    { "data": "AGE_BUCKET1_PERC" ,render: $.fn.dataTable.render.percentage() },
    { "data": "AGE_BUCKET2_PERC" ,render: $.fn.dataTable.render.percentage() },
    { "data": "AGE_BUCKET3_PERC" ,render: $.fn.dataTable.render.percentage() },
    { "data": "AGE_BUCKET456_PERC" ,render: $.fn.dataTable.render.percentage() },
  ],
    } );

    $("#eid_aging_perc_table_filter input").addClass( "form-control" );
$("#eid_aging_perc_table_filter input").attr("placeholder","Search");

}
)
.done(function(data) {
  /**
   * Delete all the chart divs in order to rebuild them 
   * with fresh data.
   */
  $("#eid_aging_charts_row").empty();

  /**
   * Repeat for each owner/hotel
   */
  data.forEach(d => {

    eid_data = pivotData(data,d.DIM,'DIM') ;
    /**
     * Create a new div for each chart
     */
    $( "#eid_aging_charts_row" ).append( '<div id="' + d.DIM +'" class="col-sm-4" ></div>' );

              /**
               * Define the div for the tooltips
               */
              var line1_tooltip = d3.select("body").append("div")	
              .attr("class", "line1-tooltip")				
              .style("opacity", 0);

              var line2_tooltip = d3.select("body").append("div")	
              .attr("class", "line2-tooltip")				
              .style("opacity", 0);

              var rect_tooltip = d3.select("body").append("div")	
              .attr("class", "rect-tooltip")				
              .style("opacity", 0);

              /**
               * Draw Chart
               */
              var ownerChart = d3.select("#" + d.DIM)
              .append("svg")
                  .attr("width", width + margin.left + margin.right)
                  .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                      .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");     
              
    
                  //X label
                  ownerChart.append("text")
                  .attr("class", "x axis-label")
                  .attr("x",width/2)
                  .attr("y", height + 65)
                  .attr("font-size", "12px")
                  .attr("text-anchor", "middle")
                  .text("Aging by User - " + d.DIM);
    
                  //Y label
                  ownerChart.append("text")
                  .attr("class", "y axis-label")
                  .attr("x", -(height/2))  
                  .attr("y", -60)
                  .attr("font-size", "12px")
                  .attr("text-anchor", "middle")
                  .attr("transform", "rotate(-90)")
                  .text("% Outstanding");  
    
                  // X Axis
                  var xAxisCall = d3.axisBottom(x);
                  var xAxisGroup = ownerChart.append("g")
                      .attr("class", "x axis")
                      .attr("transform", "translate(0," + height +")")
                      .call(xAxisCall);
    
                  // Y Axis
                  var yAxisCall = d3.axisLeft(y)
                      .tickFormat(function(d){ return  d + "%"; });
                  var yAxisGroup =ownerChart.append("g")
                      .attr("class", "y axis")
                     .call(yAxisCall);

                     var elementColor = d3.scaleOrdinal()
                     .domain([d.DIM,"Marriott Aging Policy","Individual Target"])
                     .range(["#63abf2","red","green"])   

                     var keys =  [d.DIM,"Marriott Aging Policy","Individual Target"];

                     var legend =   ownerChart.append("g")   
                               .attr("transform", "translate(-60"  + "," + (height + 25)  + ")")
                      
                    /**
                     * Add one dot in the legend for each name.
                     * */ 
                    var size = 10
                    legend.selectAll("mydots")
                      .data(keys)
                      .enter()
                      .append("rect")
                        .attr("x", 0)
                        .attr("y", function(d,i){ return 0 + i*(size+5)}) 
                        .attr("width", size)
                        .attr("height", size)
                        .style("fill", function(d){ return elementColor(d)})

                     legend.selectAll("legend")  
                      .data(keys)
                      .enter()
                      .append("text")
                      .attr("x", 0 + size*1.2)
                      .attr("y", function(d,i){ return 0 + i*(size+5) + (size/2)}) 
                      .style("fill", function(d){ return elementColor(d)})
                      .text(function(d){ return d})
                      .attr("text-anchor", "left")
                      .style("alignment-baseline", "middle")        
                   

    eid_data.forEach(d => {
      d.PERC = +d.PERC;
  
    });
    
    updateEIDAgingChart(eid_data ,ownerChart, line1_tooltip,line2_tooltip, rect_tooltip )

  });


          reportLoading.style.display = 'none';
          //draw the graph using the data from ajax call

        })
.fail(function(xhr, status, error) {
  console.log(xhr);
  console.log(status);
  console.log(error);
          reportLoading.style.display = 'none';
        })
.always(function(xhr, status, error) {
  console.log(xhr);
  console.log(status);
  console.log(error);

  reportLoading.style.display = 'none';
})

}

function updateEIDAgingChart(data , element, line1_tooltip,line2_tooltip,rect_tooltip) { 


var EID =  data[0]['DIM'];
  // console.log("x.bandwidth");
  // console.log(x.bandwidth);
// Bars
var rects = element.selectAll(".bar")
    .data(data, function(d) {
      return d.LABEL;
    })

          
 /**
  *  Draw the rectangles of the bar graph 
  * */  
rects.enter()
    .append("rect")
        .attr("class", "bar")
        .attr("y", function(d){ return y(d.PERC); })
        .attr("x", function(d){ return x(d.LABEL) })
        .attr("height", function(d){ return height - y(d.PERC); })
        .attr("width", x.bandwidth)
        .attr("fill", "#63abf2")
        .on("mouseover", function(d) {
          rect_tooltip.transition()		
                .duration(200)		
                .style("opacity", .9);		
                rect_tooltip.html(d.LABEL + "<br/>"  + d.PERC + "%")	
                .style("left", (d3.event.pageX)  + "px")		
                .style("top", (d3.event.pageY)  + "px");	
            })					
        .on("mouseout", function(d) {		
          rect_tooltip.transition()		
                .duration(500)		
                .style("opacity", 0);	
        });
      
        
        /*
         * Ajax call to obtain data for line graph 1
         * */
        $.get( "https://marriottssc.secure-app.co.za/api/eid_aging_limits/MARRIOTT_AGING_POLICY/" + EID ,
        function(data) {
        }
        )
        .done(function(data) {
          /*
          *If call is successfull, draw the line.
          */
          console.log(data);
          data.forEach(d => {
            d.PERC = +d.PERC;
          });

          var line = d3.line()
          .x(function(d) { 
            return x(d.AGING) + x.bandwidth() / 2;
          })
          .y(function(d) { return y(d.PERC); })

          element.append("path")
          .attr("class", "line")
          .attr("fill", "none")
          .attr("stroke" , "red")
          .attr("stroke-with", "3px")
          .attr("d", line(data)); 


            // Add the scatterplot
            element.selectAll("dot")	
            .data(data)			
            .enter().append("circle")	
            .attr("id",function(d) { return d.AGING} )							
            .attr("r", 3)		
            .attr("cx", function(d) { return x(d.AGING) + x.bandwidth() / 2; })		 
            .attr("cy", function(d) { return y(d.PERC); })	
            .attr("fill", "red")	
            .on("mouseover", function(d) {
              line1_tooltip.transition()		
                    .duration(200)		
                    .style("opacity", .9);		
                    line1_tooltip.html(d.AGING + "<br/>"  + d.PERC + "%")	
                    .style("left", (d3.event.pageX) + 5 + "px")		
                    .style("top", (d3.event.pageY) +5 + "px");	
                })					
            .on("mouseout", function(d) {		
              line1_tooltip.transition()		
                    .duration(500)		
                    .style("opacity", 0);	
            });
        
        })
        .fail(function(result) {
          console.log(result);
        });

        /*
         * Ajax call to obtain data for line graph 2
         * */
        $.get( "https://marriottssc.secure-app.co.za/api/eid_aging_limits/INDIVIDUAL_AGING/" + EID,
        function(data) {
        }
        )
        .done(function(data) {
          /*
          *If call is successfull, draw the line.
          */
          console.log(data);
          data.forEach(d => {
            d.PERC = +d.PERC;
          });

          var line = d3.line()
          .x(function(d) { 
            return x(d.AGING) + x.bandwidth() / 2;
          })
          .y(function(d) { return y(d.PERC); })

          element.append("path")
          .attr("class", "line")
          .attr("fill", "none")
          .attr("stroke" , "green")
          .attr("stroke-with", "3px")
          .attr("d", line(data)); 


            // Add the scatterplot
            element.selectAll("dot")	
            .data(data)			
            .enter().append("circle")	
            .attr("id",function(d) { return d.AGING} )							
            .attr("r", 3)		
            .attr("cx", function(d) { return x(d.AGING) + x.bandwidth() / 2; })		 
            .attr("cy", function(d) { return y(d.PERC); })	
            .attr("fill", "green")	
            .on("mouseover", function(d) {
              line2_tooltip.transition()		
                    .duration(200)		
                    .style("opacity", .9);		
                    line2_tooltip.html(d.AGING + "<br/>"  + d.PERC + "%")	
                    .style("left", (d3.event.pageX) + 5 + "px")		
                    .style("top", (d3.event.pageY) +5 + "px");	
                })					
            .on("mouseout", function(d) {		
              line2_tooltip.transition()		
                    .duration(500)		
                    .style("opacity", 0);	
            });
        
        })
        .fail(function(result) {
          console.log(result);
        });
       
}  

//Update the Resort Aging table and chart
function updateResortAging() {

  $('#resort_aging_table').DataTable().destroy();


$.get( "https://marriottssc.secure-app.co.za/api/resort_aging/" + $("#filter_date").val(),
function(data) {
  ownerData = data;
  console.log(data);
  reportLoading.style.display = 'none';

  $('#resort_aging_table').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                extension: '.pdf',
                filename: 'AR Account Owners',
                title: 'AR Account Owners'
            },
            'copy',
            {
          extend: 'csvHtml5',
          title: 'AR Account Owners',
          filename: 'AR Account Owners'
      },
      {
          extend: 'excelHtml5',
          filename: 'AR Account Owners',
          title: 'AR Account Owners'
      },
              'print'
        ],
    "paging": false,
    select: 'single',
    searching: false,
    scrollCollapse: true,
       data: ownerData,
       "columns": [
    { "data": "RESORT"  },
    { "data": "AGE_BUCKET1" ,render: $.fn.dataTable.render.number( ',', '.', 2 )},
    { "data": "AGE_BUCKET2" ,render: $.fn.dataTable.render.number( ',', '.', 2 )},
    { "data": "AGE_BUCKET3" ,render: $.fn.dataTable.render.number( ',', '.', 2 )},
    { "data": "AGE_BUCKET456" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
    { "data": "TOTAL" ,render: $.fn.dataTable.render.number( ',', '.', 2 )  },
  ],
    } );

    $("#resort_aging_table_filter input").addClass( "form-control" );
$("#resort_aging_table_filter input").attr("placeholder","Search");

}
)
.done(function(data) {

          reportLoading.style.display = 'none';

        })
.fail(function(xhr, status, error) {
          reportLoading.style.display = 'none';
        })

}


//Update the Resort Aging Percentage table and chart
function updateResortPercAging() {

  $('#resort_aging_perc_table').DataTable().destroy();


$.get( "https://marriottssc.secure-app.co.za/api/resort_aging_perc/" + $("#filter_date").val(),
function(data) {
  ownerData = data;
  console.log(data);
  reportLoading.style.display = 'none';

  $('#resort_aging_perc_table').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                extension: '.pdf',
                filename: 'AR Account Owners',
                title: 'AR Account Owners'
            },
            'copy',
            {
          extend: 'csvHtml5',
          title: 'AR Account Owners',
          filename: 'AR Account Owners'
      },
      {
          extend: 'excelHtml5',
          filename: 'AR Account Owners',
          title: 'AR Account Owners'
      },
              'print'
        ],
    "paging": false,
    select: 'single',
    searching: false,
    scrollCollapse: true,
       data: ownerData,
       "columns": [
    { "data": "RESORT"  },
    { "data": "AGE_BUCKET1_PERC" ,render: $.fn.dataTable.render.percentage() },
    { "data": "AGE_BUCKET2_PERC" ,render: $.fn.dataTable.render.percentage() },
    { "data": "AGE_BUCKET3_PERC" ,render: $.fn.dataTable.render.percentage() },
    { "data": "AGE_BUCKET456_PERC" ,render: $.fn.dataTable.render.percentage() },
  ],
    } );

    $("#resort_aging_perc_table_filter input").addClass( "form-control" );
$("#resort_aging_perc_table_filter input").attr("placeholder","Search");
}
)
.done(function(data) {
          reportLoading.style.display = 'none';
          //draw the graph using the data from ajax call
             //draw the graph using the data from ajax call
             $("#resort_aging_charts_row").empty();
             data.forEach(d => {
               eid_data = pivotData(data,d.RESORT,'RESORT') ;
           
               $( "#resort_aging_charts_row" ).append( '<div id="' + d.RESORT +'" class="col-sm-4" ></div>' );
           
           
                         /*Define for each owner */
           
                         // Define the div for the tooltip
                         var line1_tooltip = d3.select("body").append("div")	
                         .attr("class", "line-tooltip")				
                         .style("opacity", 0);
           
                         var rect_tooltip = d3.select("body").append("div")	
                         .attr("class", "rect-tooltip")				
                         .style("opacity", 0);
           
                         var ownerChart = d3.select("#" + d.RESORT)
                         .append("svg")
                             .attr("width", width + margin.left + margin.right)
                             .attr("height", height + margin.top + margin.bottom)
                               .append("g")
                                 .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");     
                         
               
                             //X label
                             ownerChart.append("text")
                             .attr("class", "x axis-label")
                             .attr("x",width/2)
                             .attr("y", height + 65)
                             .attr("font-size", "12px")
                             .attr("text-anchor", "middle")
                             .text("Aging by User - " + d.RESORT);
               
                             //Y label
                             ownerChart.append("text")
                             .attr("class", "y axis-label")
                             .attr("x", -(height/2))  
                             .attr("y", -60)
                             .attr("font-size", "12px")
                             .attr("text-anchor", "middle")
                             .attr("transform", "rotate(-90)")
                             .text("% Outstanding");  
               
                             // X Axis
                             var xAxisCall = d3.axisBottom(x);
                             var xAxisGroup = ownerChart.append("g")
                                 .attr("class", "x axis")
                                 .attr("transform", "translate(0," + height +")")
                                 .call(xAxisCall);
               
                             // Y Axis
                             var yAxisCall = d3.axisLeft(y)
                                 .tickFormat(function(d){ return  d + "%"; });
                             var yAxisGroup =ownerChart.append("g")
                                 .attr("class", "y axis")
                                .call(yAxisCall);
           
                                var elementColor = d3.scaleOrdinal()
                                .domain([d.RESORT,"Marriott Aging Policy"])
                                .range(["#63abf2","red"])   
           
                                var keys =  [d.RESORT,"Marriott Aging Policy"];
           
                                var legend =   ownerChart.append("g")   
                                          .attr("transform", "translate(-60"  + "," + (height + 25)  + ")")
                                 
                               // Add one dot in the legend for each name.
                               var size = 10
                               legend.selectAll("mydots")
                                 .data(keys)
                                 .enter()
                                 .append("rect")
                                   .attr("x", 0)
                                   .attr("y", function(d,i){ return 0 + i*(size+5)}) // 100 is where the first dot appears. 25 is the distance between dots
                                   .attr("width", size)
                                   .attr("height", size)
                                   .style("fill", function(d){ return elementColor(d)})
           
                                legend.selectAll("legend")  
                                 .data(keys)
                                 .enter()
                                 .append("text")
                                 .attr("x", 0 + size*1.2)
                                 .attr("y", function(d,i){ return 0 + i*(size+5) + (size/2)}) // 100 is where the first dot appears. 25 is the distance between dots
                                 .style("fill", function(d){ return elementColor(d)})
                                 .text(function(d){ return d})
                                 .attr("text-anchor", "left")
                                 .style("alignment-baseline", "middle")        
                                
               
                             /*Define for each owner */
           
               eid_data.forEach(d => {
                 d.PERC = +d.PERC;
               });
               
               updateResortAgingChart(eid_data ,ownerChart, line1_tooltip, rect_tooltip );
              })

})
.fail(function(xhr, status, error) {
          reportLoading.style.display = 'none';
})

}


function updateResortAgingChart(data , element, line1_tooltip,rect_tooltip) { 


// Bars
var rects = element.selectAll(".bar")
    .data(data, function(d) {
      return d.LABEL;
    })

          
 //draw the rectangles of the bar graph   
rects.enter()
    .append("rect")
        .attr("class", "bar")
        .attr("y", function(d){ return y(d.PERC); })
        .attr("x", function(d){ return x(d.LABEL) })
        .attr("height", function(d){ return height - y(d.PERC); })
        .attr("width", x.bandwidth)
        .attr("fill", "#63abf2")
        .on("mouseover", function(d) {
          rect_tooltip.transition()		
                .duration(200)		
                .style("opacity", .9);		
                rect_tooltip.html(d.LABEL + "<br/>"  + d.PERC + "%")	
                .style("left", (d3.event.pageX)  + "px")		
                .style("top", (d3.event.pageY)  + "px");	
            })					
        .on("mouseout", function(d) {		
          rect_tooltip.transition()		
                .duration(500)		
                .style("opacity", 0);	
        });
      

        //ajax call to obtain limits for line graph
        $.get( "https://marriottssc.secure-app.co.za/api/eid_aging_limits",
        function(data) {
        }
        )
        .done(function(data) {
          //if successfull, draw the line
          data.forEach(d => {
            d.PERC = +d.PERC;
          });

          var line = d3.line()
          .x(function(d) { 
            return x(d.AGING) + x.bandwidth() / 2;
          })
          .y(function(d) { return y(d.PERC); })

          element.append("path")
          .attr("class", "line")
          .attr("fill", "none")
          .attr("stroke" , "red")
          .attr("stroke-with", "3px")
          .attr("d", line(data)); 


            // Add the scatterplot
            element.selectAll("dot")	
            .data(data)			
            .enter().append("circle")	
            .attr("id",function(d) { return d.AGING} )							
            .attr("r", 3)		
            .attr("cx", function(d) { return x(d.AGING) + x.bandwidth() / 2; })		 
            .attr("cy", function(d) { return y(d.PERC); })	
            .attr("fill", "red")	
            .on("mouseover", function(d) {
              line1_tooltip.transition()		
                    .duration(200)		
                    .style("opacity", .9);		
                    line1_tooltip.html(d.AGING + "<br/>"  + d.PERC + "%")	
                    .style("left", (d3.event.pageX) + 5 + "px")		
                    .style("top", (d3.event.pageY) +5 + "px");	
                })					
            .on("mouseout", function(d) {		
              line1_tooltip.transition()		
                    .duration(500)		
                    .style("opacity", 0);	
            });
        
        })
        .fail(function(result) {
          console.log(result);
        })
       
} 

//Update SSC Aging report
function updateSSCAging() {
 $('#ssc_aging_table').DataTable().destroy();
 if ( $("#filter_from_date").val().length == 0) {
  filter_from_date = '2000-01-01';
  console.log(filter_from_date);
} else {
  filter_from_date = $("#filter_from_date").val();
}
 
  var apiUrl = "https://marriottssc.secure-app.co.za/api/ssc_aging/" + filter_from_date + "/" + $("#filter_to_date").val() ;
  if ($("#filter_eid").val() != 'ALL')  {
    apiUrl = apiUrl + "/" + $("#filter_eid").val();
   }  else {
    apiUrl = apiUrl + "/ALL" ;
  }
  if ($("#filter_resort").val() != 'ALL')  {
    apiUrl = apiUrl + "/" + $("#filter_resort").val(); 
  } 
//  console.log(apiUrl);

  $.get( apiUrl ,
  function(data) {
    ownerData = data;
    console.log(data);
    reportLoading.style.display = 'none';
  
    $('#ssc_aging_table').DataTable( {
      dom: 'Bfrtip',
      buttons: [
        {
                  extend: 'pdfHtml5',
                  orientation: 'landscape',
                  pageSize: 'LEGAL',
                  extension: '.pdf',
                  filename: 'AR Account Owners',
                  title: 'AR Account Owners'
              },
              'copy',
              {
            extend: 'csvHtml5',
            title: 'AR Account Owners',
            filename: 'AR Account Owners'
        },
        {
            extend: 'excelHtml5',
            filename: 'AR Account Owners',
            title: 'AR Account Owners'
        },
                'print'
          ],
      "paging": false,
      select: 'single',
      searching: false,
      "scrollY": "550px",
      scrollCollapse: true,
         data: ownerData,
         "columns": [
      
      { "data": "ACCOUNT_NAME"  },
      { "data": "AGE_BUCKET1" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
      { "data": "AGE_BUCKET2" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
      { "data": "AGE_BUCKET3" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
      { "data": "AGE_BUCKET4" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
      { "data": "AGE_BUCKET5" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
      { "data": "AGE_BUCKET6" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
      { "data": "TOTAL" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
      { "data": "CREDIT_LIMIT" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
    ],
      } );

  }
  )
  .done(function(result) {
          // console.log(result);
          reportLoading.style.display = 'none';
  })
.fail(function(result) {
          console.log(result)
})

}


/**
 * Build summary table below main table.
 * Initially the totalArray will be empty 
 * until the main table columns totals 
 * have been assigned to it. 
 */
var totalsArray = [];
  $('#summary_table').DataTable({
    "searching": false,
    "paging": false,
    "bInfo" : false,
    columns: [
      {
          name: 'title',
          title: '',
      },
      {
          name: 'Current',
          title: 'Current',
      },
      {
          title: '31 to 60 Days',
      }, 
      {
          title: '61 to 90 Days',
      },
      {
          title: '91 to 120 Days',
      },
      {
          title: '121+ Days',
      },
      {
          title: 'Balance',
      },
  ],
  data: totalsArray,
      rowsGroup: [
        'title:name',
        
        
      ],
  });

/**
 * Function does Ajax call to check if the current period
 * has already been published. If current period exists in
 * table AR_SNAPSHOT_DATES, the background will be highlighted
 * and text will show next to the Publish button. 
 */  
 function alreadyPublished() {
  $.get( "https://marriottssc.secure-app.co.za/api/current_period_snapshot/" + $("#filter_resort").val() ,
  function(data) {
    //Do something
  })
  .done(function( data,status,xhr ) {
    console.log(data);
    if(typeof data !== 'undefined' && data.length > 0) {
      console.log(data);
      $("#publish_text").text("Current period: " + data[0]['SNAPSHOT_DATE'] + " published by " + data[0]['CREATED_USER'] + " at " + data[0]['CREATED_DATE']);
      $("#aging_template_wrapper").css("background","rgb(235, 255, 242)"); 

      if (data[0]['LOCKED_YN'] == "Y" ) {
        $("#publishAgingTempBtn").attr("disabled", true);
        $("#publishAgingTempBtn").text("Locked");
        $("#aging_template_wrapper").css("background","rgb(255, 235, 235)"); 
      }
    } else {
      $("#publish_text").text("");
      $("#publishAgingTempBtn").text("Publish");
      $("#publishAgingTempBtn").attr("disabled", false);
    } 
    
  })
  .fail(function(data,status,xhr) {
    console.log(data);
  })
  .always(function(data,status,xhr) {
    console.log(data);
  }); 
 } 







 

/**
 * Function that does an AJAX call to 
 * api/aging_template route in routes/api.php
 * and builds a table to display the data. 
 * 
 * 
 * 
 * aging_template_data variable will old the data returned 
 * from the AJAX call to be used when Publish button
 * is clicked. 
 */
var aging_template_data = [];

function updateAgingTemplate() {
  totalsArray = [];

  $('#aging_template').DataTable().destroy();
  console.log("Update Aging Template");
   var apiUrl = "https://marriottssc.secure-app.co.za/api/aging_template"  ;
   
   if ($("#filter_resort").val() != 'ALL')  {
     apiUrl = apiUrl + "/" + $("#filter_resort").val(); 
   } 
  console.log(apiUrl);
 
   $.get( apiUrl ,
   function(data) {
    
     ownerData = data;
     console.log(data);
     reportLoading.style.display = 'none';
   
     $('#aging_template').DataTable( {
       dom: 'Bfrtip',
       buttons: [
         {
                   extend: 'pdfHtml5',
                   orientation: 'landscape',
                   pageSize: 'LEGAL',
                   extension: '.pdf',
                   filename: 'AR Account Owners',
                   title: 'AR Account Owners'
               },
               'copy',
               {
             extend: 'csvHtml5',
             title: 'AR Account Owners',
             filename: 'AR Account Owners'
         },
         {
             extend: 'excelHtml5',
             filename: 'AR Account Owners',
             title: 'AR Account Owners'
         },
                 'print'
           ],
       "paging": false,
       select: 'single',
       searching: false,
       "scrollY": "390px",
      
       scrollCollapse: true,
          data: ownerData,
          "columns": [
      { "data": "ACCOUNT_NO" ,"width": "6%" },
       { "data": "ACCOUNT_NAME"  ,"width": "8%"},
       { "data": "CREDIT_LIMIT","width": "6%",render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET1","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET2","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET3","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET4" ,"width": "6%",render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET5" ,"width": "6%",render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET6","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "TOTAL","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "PROVISION","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "PREV120","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "REMARKS","width": "26%"  },
       
     ],
     "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;

      // Remove the formatting to get integer data for summation
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };
      age1 = api
          .column(3 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      age2 = api
          .column(4 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      age3 = api
          .column(5 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      age4 = api
          .column(6 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      age5 = api
          .column(7 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
          age6 = api
          .column(8 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      // Total over all pages
      total = api
          .column(9 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      provision = api
      .column(10 )
      .data()
      .reduce( function (a, b) {
          return intVal(a) + intVal(b);
      }, 0 );
      prev260days = api
      .column(11 )
      .data()
      .reduce( function (a, b) {
          return intVal(a) + intVal(b);
      }, 0 );

      
      /**
       * Assign the columns totals to an array 
       * to be used in building the summary table 
       * below the main table.
       */
      var summary = ['Aging % As per Buckets',age1,age2,age3,age4,age5,age6,total];
      /**
       * Function to divide each column total by the total of 
       * the Balance column to get the precentage of each aging 
       * bracket with regard to the total of all brackets.
       */
      function agingPercentage(value, index, array) {
        if (index > 0) {
          return (value/total)*100;
        } else {return value;}
      }
      /**
       * Calculate the percentages of each total and
       * add totals and percentage to one array. 
       */
      var summary_perc = summary.map(agingPercentage);
      console.log(summary);
      console.log(summary_perc);
      /**
       * A function to format the numbers
       * of an the totalsArray
       */
      function formatAgingNumbers(value, index, array) {
        if (index > 0) {
          return value.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        } else {return value;}
      }
      var summaryFormatted = summary.map(formatAgingNumbers);
      var summary_percFormatted = summary_perc.map(formatAgingNumbers);
      totalsArray.push(summaryFormatted);
      totalsArray.push(summary_percFormatted);

      age1 = $.fn.dataTable.render.number(',', '.', 2).display( age1 );
      age2 = $.fn.dataTable.render.number(',', '.', 2).display( age2 );
      age3 = $.fn.dataTable.render.number(',', '.', 2).display( age3 );
      age4 = $.fn.dataTable.render.number(',', '.', 2).display( age4 );
      age5 = $.fn.dataTable.render.number(',', '.', 2).display( age5 );
      age6 = $.fn.dataTable.render.number(',', '.', 2).display( age6 );
      total = $.fn.dataTable.render.number(',', '.', 2).display( total );
      provision = $.fn.dataTable.render.number(',', '.', 2).display( provision );
      prev260days = $.fn.dataTable.render.number(',', '.', 2).display( prev260days );
      // Update footer
      $( api.column(3).footer() ).html(
        age1
      );
      $( api.column(4 ).footer() ).html(
        age2
      );
      $( api.column(5 ).footer() ).html(
        age3
      );
      $( api.column(6 ).footer() ).html(
        age4
      );
      $( api.column(7 ).footer() ).html(
        age5
      );
      $( api.column(8 ).footer() ).html(
        age6
      );
      $( api.column(9 ).footer() ).html(
          total
      );
      $( api.column(10 ).footer() ).html(
        provision
      );
      $( api.column(11 ).footer() ).html(
          prev260days
      );
  }
       } );
 
   }
   )
   .done(function(data) {

        /**
     * Refresh the summary table with 
     * the new totals of the main table.
     */
    // console.log(totalsArray);
    $('#summary_table').DataTable().clear().draw();
    $('#summary_table').DataTable().rows.add(totalsArray); // Add new data
    $('#summary_table').DataTable().columns.adjust().draw(); // Redraw the DataTable
          aging_template_data = data;
          //  console.log(data);
           reportLoading.style.display = 'none';
           alreadyPublished();
   })
 .fail(function(result) {
           console.log(result)
 })
 
 }

 /**
  * Aging Template Live Data  - When double clicking on a line in the table, 
  * the Edit remarks modal will open where new remarks can be added
  * or existing remarks can be edited by authorised users.
  * 
  * Table cell values are assigned to variables which will be assigned
  *  to the modal form fields. 
  * 
  */

  /**
   * Initialise variables to hold the table cell values 
   * of the double clicked row
   */
  var remarksAccountNo = '' ;
  var remarksAccountName = '';
  var remarks = '' ;

  /**
   * On double clicking the row, assigned the row values
   */

 $('#aging_template').on('dblclick','tr',function(e){
  $('#aging_template').DataTable().row(this).select();
   var $element = $('#aging_template').DataTable().row( this ).data() ;
   remarksAccountNo = $element.ACCOUNT_NO ;
   remarksAccountName = $element.ACCOUNT_NAME;
   remarks = $element.REMARKS ;
      console.log($element.ACCOUNT_NO  );
      $('#remarksModal').modal('show');
    })

/**
 * When the Edit Remarks modal opens, assign the values of 
 * the variables assigned above to the modal fields 
 */

    $('#remarksModal').on('show.bs.modal', function (event) {
      var modal = $(this)
      $("#REMARKS_ACCOUNT_NO").val(remarksAccountNo);
      $("#REMARKS_ACCOUNT_NAME").val(remarksAccountName);
      $("#REMARKS").val(remarks);
    })


/**
 * When clicking on Save Changes in Edit Remarks modal
 * an AJAX call is made to save the remarks in the 
 * AR_AGING_TEMP_REMARKS table ONLY if the Account Number is 
 * populated and the remark is not empty. 
 * 
 */
$('#remarksModalBtn').click(function () {
  var formArray = $("#remarksModalForm").serializeArray();
  console.log(formArray);
  if ($("#REMARKS_ACCOUNT_NO").val().length > 0 && $("#REMARKS").val().length > 0 ) {
   console.log("Update remarks");
  updateRemarksURI = "https://marriottssc.secure-app.co.za/api/update_aging_temp_remarks/" + $("#filter_resort").val();
console.log(updateRemarksURI);
    $.post( updateRemarksURI ,
    $("#remarksModalForm").serializeArray() ,
    function(data) {
      console.log(data);
    }
    )
    .done(function( msg ) {
      console.log(msg);
      /**
       * Refresh the table and close modal 
       */
      updateAgingTemplate();
      $('#remarksModal').modal('hide');
    })
    .fail(function(error) {
      alert("Remarks could not be saved. ");
      console.log(error);
    })
    .always(function(data,status,xhr) {
      // alert("Remarks could not be saved. ");
      console.log(data);
    });
  } else {
    alert("Please complete all the required fields!");
  }
  });

/**
 * When Delete is clicked on Edit Remarks modal,
 * a pop-up window will open to ask the user to confirm.
 */
  $('#remarksDeleteModalBtn').click(function () {
    $('#deleteRemarksModal').modal('show');
    });

/**
 * When Delete is click on the pop-up windows, an AJAX call is made 
 * to delete the record from AR_AGING_TEMP_REMARKS table for the RESORT AND 
 * PMS_ACCOUNT_NO in the Edit Remarks modal. 
 */
  $('#deleteRemarksConfirmBtn').click(function () {
    var ownerArray = $("#remarksModalForm").serializeArray();
    console.log(ownerArray);
      console.log("Delete remarks");
      $.post( "https://marriottssc.secure-app.co.za/api/delete_aging_temp_remarks/" + $("#filter_resort").val(),
      $("#remarksModalForm").serializeArray() ,
      function(data) {
        console.log(data);
      }
      )
      .done(function( msg ) {
        console.log(msg);
        updateAgingTemplate();
        $('#remarksModal').modal('hide');
        $('#deleteRemarksModal').modal('hide');
      })
      .fail(function(error) {
        alert("remarks could not be deleted. Please contact administrator" + error);
      });
    });
/**
 * When Publish button on Aging Template page is clicked,
 * a snapshot is taken of the data in the report and 
 * saved to AR_AGING_TEMP_SNAPSHOTS. 
 */
$('#publishAgingTempBtn').click(function () {
  // var csrf_array = [];
  // csrf_array['csrf_name'] =  $("#getTokenName").val();
  // csrf_array['csrf_value'] = $("#getTokenValue").val() ;
  // aging_template_data.push(csrf_array);
  
   console.log("Publish button clicked");

   $.get( "https://marriottssc.secure-app.co.za/api/publish_aging_template/" + $("#filter_resort").val() ,
    function(data) {
      //Do something
    })
    .done(function( data,status,xhr ) {
      console.log(data);
      // $("#publishAgingTempBtn").attr("disabled", true);
      alreadyPublished();
      
    })
    .fail(function(data,status,xhr) {
      console.log(data);
      
    })
    .always(function(data,status,xhr) {
      console.log(data);
    });
   
  });


  /**
   * Function too loop through the 
   * given array and add values to 
   * Published Date LOV.
   */
  function addSnapshotLOV(value, index, array) {
    console.log(value);
    $('#filter_snapshot_date').append(`<option value="${value.SNAPSHOT_DATE}"> 
        ${value.SNAPSHOT_DATE} 
        </option>`); 
  }

  /**
   * Function that does an Ajax call to get the 
   * snapshot dates for the selected hotel
   * and populate the Published date LOV.
   */
  function getSnapshotDates() {
    $('#filter_snapshot_date')
    .find('option')
    .remove();
    $.get( "https://marriottssc.secure-app.co.za/api/published_snapshots/" + $("#filter_resort_published").val() ,
    function(data) {
      console.log(data.length);
      if(data.length > 0) {
        data.forEach(addSnapshotLOV);
        // console.log(data[0]['SNAPSHOT_DATE']);
        
        updateAgingPublished();
      } else {
        reportLoading.style.display = 'none';
        $("#content").css({ "display": "none" });
        $("#nocontent").css({ "display": "block" });
      } 
    })
    .done(function( data,status,xhr ) {
      console.log(data);
      
    })
    .fail(function(data,status,xhr) {
      console.log(data);
      $("#publishAgingTempBtn").attr("disabled", true);
    })
    .always(function(data,status,xhr) {
      console.log(data);
    });
  }


  /**
 * Aging Published
 * When the Hotel filter is changed,
 * the snapshot dates for that hotel
 * will be obtained and the Published Date
 * LOV will be populated with this date.
 */
$('#filter_resort_published').change(function() {
  $('#aging_published').DataTable().clear()
    .draw();
  getSnapshotDates();
});
/**
 * When the search button is clicked on Aging Published
 * the updateAgingPublished function is called to 
 * obtain the published aging via Ajax and build the table. 
 */
$('#searchPublishedAgingBtn').click(function () {
  updateAgingPublished();
})

$('#lockAgingBtn').click(function () {
  postdata = {
    csrf_name: $("#getTokenName").val(),
    csrf_value:$("#getTokenValue").val() ,
  }

  console.log(postdata);
    console.log("Lock snapshot date");
    $.post( "https://marriottssc.secure-app.co.za/api/lock_snapshot_date/" + $("#filter_resort_published").val() + "/" + $("#filter_snapshot_date").val() ,
    postdata ,
    function(data) {
      console.log(data);
    }
    )
    .done(function( msg ) {
      console.log(msg);
      $("#aging_published_wrapper").css("background","rgb(255, 235, 235)");  
      $("#lockAgingBtn").attr("disabled", true);  
    })
    .fail(function(error) {
      alert("Could not lock aging. Please contact administrator" + error);
    });
  }); 



  /**
 * Function does Ajax call to check if the current period
 * has already been locked. If current period exists in
 * table AR_SNAPSHOT_DATES and LOCKED_YN = Y, the background will be highlighted
 * and text will show next to the Lock button. 
 */  
 function alreadyLocked() {
  $.get( "https://marriottssc.secure-app.co.za/api/current_period_snapshot/" + $("#filter_resort_published").val() ,
  function(data) {
    //Do something
  })
  .done(function( data,status,xhr ) {
    console.log(data);
    if(typeof data !== 'undefined' && data.length > 0) {
      if (data[0]['LOCKED_YN'] == "Y" ) {
        $("#lockAgingBtn").attr("disabled", true);
        $("#lockAgingBtn").text("Locked");
        $("#aging_published_wrapper").css("background","rgb(255, 235, 235)"); 
      } else {
        $("#lockAgingBtn").attr("disabled", false);
        $("#lockAgingBtn").text("Lock");
      }
    }  
    
  })
  .fail(function(data,status,xhr) {
    console.log(data);
  })
  .always(function(data,status,xhr) {
    console.log(data);
  }); 
 } 

  
/**
 * Function that does an AJAX call to 
 * api/aging_published route in routes/api.php
 * and builds a table to display the data. 
 * 
 * 
 * 
 * aging_template_data variable will old the data returned 
 * from the AJAX call to be used when Publish button
 * is clicked. 
 */
function updateAgingPublished() {

  /**
 * Array hold the columns totals
 * which will be used to build the 
 * summary table below the main table.
 */
totalsArray = [];
  

  $('#aging_published').DataTable().destroy();
  console.log("Update Aging Template");
   var apiUrl = "https://marriottssc.secure-app.co.za/api/aging_published/" + $("#filter_resort_published").val() + "/" + $("#filter_snapshot_date").val()  ;
   
  
  console.log(apiUrl);
 
   $.get( apiUrl ,
   function(data) {
     ownerData = data;
     console.log(data);
     reportLoading.style.display = 'none';
   
     $('#aging_published').DataTable( {
       dom: 'Bfrtip',
       buttons: [
         {
                   extend: 'pdfHtml5',
                   orientation: 'landscape',
                   pageSize: 'LEGAL',
                   extension: '.pdf',
                   filename: 'AR Account Owners',
                   title: 'AR Account Owners'
               },
               'copy',
               {
             extend: 'csvHtml5',
             title: 'AR Account Owners',
             filename: 'AR Account Owners'
         },
         {
             extend: 'excelHtml5',
             filename: 'AR Account Owners',
             title: 'AR Account Owners'
         },
                 'print'
           ],
       "paging": false,
       select: 'single',
       searching: false,
       "scrollY": "390px",
       scrollCollapse: true,
          data: ownerData,
          "columns": [
       { "data": "PMS_ACCOUNT_NO" ,"width": "6%" },
       { "data": "ACCOUNT_NAME"  ,"width": "8%"},
       { "data": "CREDIT_LIMIT","width": "6%",render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET1","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET2","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET3","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET4" ,"width": "6%",render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET5" ,"width": "6%",render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "AGE_BUCKET6","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "TOTAL","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "PROVISION","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "PREV120DAYS","width": "6%" ,render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "REMARKS","width": "26%"  },
       
     ],
     "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;

      // Remove the formatting to get integer data for summation
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };
      age1 = api
          .column(3 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      age2 = api
          .column(4 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      age3 = api
          .column(5 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      age4 = api
          .column(6 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      age5 = api
          .column(7 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
          age6 = api
          .column(8 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      // Total over all pages
      total = api
          .column(9 )
          .data()
          .reduce( function (a, b) {
              return intVal(a) + intVal(b);
          }, 0 );
      provision = api
      .column(10 )
      .data()
      .reduce( function (a, b) {
          return intVal(a) + intVal(b);
      }, 0 );
      prev260days = api
      .column(11 )
      .data()
      .reduce( function (a, b) {
          return intVal(a) + intVal(b);
      }, 0 );

      /**
       * Assign the columns totals to an array 
       * to be used in building the summary table 
       * below the main table.
       */
      var summary = ['Aging % As per Buckets',age1,age2,age3,age4,age5,age6,total];
      /**
       * Function to divide each column total by the total of 
       * the Balance column to get the precentage of each aging 
       * bracket with regard to the total of all brackets.
       */
      function agingPercentage(value, index, array) {
        if (index > 0) {
          return (value/total)*100;
        } else {return value;}
      }
      /**
       * Calculate the percentages of each total and
       * add totals and percentage to one array. 
       */
      var summary_perc = summary.map(agingPercentage);
      console.log(summary);
      console.log(summary_perc);
      /**
       * A function to format the numbers
       * of an the totalsArray
       */
      function formatAgingNumbers(value, index, array) {
        if (index > 0) {
          return value.toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        } else {return value;}
      }
      var summaryFormatted = summary.map(formatAgingNumbers);
      var summary_percFormatted = summary_perc.map(formatAgingNumbers);
      totalsArray.push(summaryFormatted);
      totalsArray.push(summary_percFormatted);
      
      age1 = $.fn.dataTable.render.number(',', '.', 2).display( age1 );
      age2 = $.fn.dataTable.render.number(',', '.', 2).display( age2 );
      age3 = $.fn.dataTable.render.number(',', '.', 2).display( age3 );
      age4 = $.fn.dataTable.render.number(',', '.', 2).display( age4 );
      age5 = $.fn.dataTable.render.number(',', '.', 2).display( age5 );
      age6 = $.fn.dataTable.render.number(',', '.', 2).display( age6 );
      total = $.fn.dataTable.render.number(',', '.', 2).display( total );
      provision = $.fn.dataTable.render.number(',', '.', 2).display( provision );
      prev260days = $.fn.dataTable.render.number(',', '.', 2).display( prev260days );
      // Update footer
      $( api.column(3).footer() ).html(
        age1
      );
      $( api.column(4 ).footer() ).html(
        age2
      );
      $( api.column(5 ).footer() ).html(
        age3
      );
      $( api.column(6 ).footer() ).html(
        age4
      );
      $( api.column(7 ).footer() ).html(
        age5
      );
      $( api.column(8 ).footer() ).html(
        age6
      );
      $( api.column(9 ).footer() ).html(
          total
      );
      $( api.column(10 ).footer() ).html(
        provision
      );
      $( api.column(11 ).footer() ).html(
          prev260days
      );



  }
       } );
 
   }
   )
   .done(function(data) {

    /**
     * Refresh the summary table with 
     * the new totals of the main table.
     */
    // console.log(totalsArray);
    $('#summary_table').DataTable().clear().draw();
    $('#summary_table').DataTable().rows.add(totalsArray); // Add new data
    $('#summary_table').DataTable().columns.adjust().draw(); // Redraw the DataTable
    alreadyLocked();
          
          //  console.log(data);
           reportLoading.style.display = 'none';
   })
 .fail(function(result) {
           console.log(result);
           reportLoading.style.display = 'none';
 })
 
 }

 






function removeApos(string) {
  return string.replace("'","");
}
function renameColumns(string) {
var  ret_str = '';
  switch (string) {
    case 'AGE_BUCKET1':
      ret_str = "Up to 15";
      break;
    case 'AGE_BUCKET2':
      ret_str = "16 to 30";
      break;
    case 'AGE_BUCKET3':
      ret_str = "31 to 60";
      break;
    case 'AGE_BUCKET4':
      ret_str = "61 to 90";
      break;
    case 'AGE_BUCKET5':
      ret_str = "91 to 120";
      break;
    case 'AGE_BUCKET6':
      ret_str = "120 and over";
      break;
    case 'TOTAL':
      ret_str = "Total";
      break;
    case 'CREDIT_LIMIT':
      ret_str = "Credit Limit";
      break;
    case 'ACCOUNT_NAME':
      ret_str = "Account";
      break;   
  
    default:
      break;
  } 
  return ret_str;
}



function pivotData(data,filter,datatype) {

console.log(data);
  if (datatype == 'DIM') {
    var user_data =  data.filter(function(rows) {
      return rows.DIM == filter;
    });
  
  } else {
    var user_data =  data.filter(function(rows) {
      return rows.RESORT == filter;
    });
  }

  console.log(user_data);

  var masterArray = [];
  var AGE_BUCKET1_array = [];
  AGE_BUCKET1_array['DIM'] = user_data[0].DIM;
  AGE_BUCKET1_array['LABEL'] = "0 - 30 Days";
  AGE_BUCKET1_array['PERC'] = user_data[0].AGE_BUCKET1_PERC;
  masterArray.push(AGE_BUCKET1_array);
  var AGE_BUCKET2_array = [];
  AGE_BUCKET2_array['DIM'] = user_data[0].DIM;
  AGE_BUCKET2_array['LABEL'] = "31 - 60 Days";
  AGE_BUCKET2_array['PERC'] = user_data[0].AGE_BUCKET2_PERC;
  masterArray.push(AGE_BUCKET2_array);
  var AGE_BUCKET3_array = [];
  AGE_BUCKET3_array['DIM'] = user_data[0].DIM;
  AGE_BUCKET3_array['LABEL'] = "61 - 90 Days";
  AGE_BUCKET3_array['PERC'] = user_data[0].AGE_BUCKET3_PERC;
  masterArray.push(AGE_BUCKET3_array);
  var AGE_BUCKET456_array = [];
  AGE_BUCKET456_array['DIM'] = user_data[0].DIM;
  AGE_BUCKET456_array['LABEL'] = "91 - Older";
  AGE_BUCKET456_array['PERC'] = user_data[0].AGE_BUCKET456_PERC;
  masterArray.push(AGE_BUCKET456_array);

  return masterArray;
}





