var commentLevel = "";
var commentAccountNo = "";
var commentResort = "";
var commentInvoice = "";
var level1Data = "";
var level2Data = "";
var level3Data = "";
var level4Data = "";
var testTableData = "";
var cellValue = "";
var cellIndex = "";

function delay(callback, ms) {
  var timer = 0;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}

reportLoading = document.getElementById("reportLoading");

function formatNumber(num) {
  return num
    .toFixed(2)
    .toString()
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
function accountTypeFormatter() {
  return "Total";
}

function nameFormatter(data) {
  return data.length;
}

function totalFormatter(data) {
  var field = this.field;

  return formatNumber(
    data
      .map(function (row) {
        return +row[field];
      })
      .reduce(function (sum, i) {
        return sum + i;
      }, 0)
  );
}

$(document).ready(function () {
  console.log("Document ready");

  /**
   * Activate/Indent the tab that is clicked on
   */
  if (window.location.pathname.indexOf("level1") > 0) {
    $("#level1").addClass("active");
  }
  if (window.location.pathname.indexOf("level2") > 0) {
    $("#level2").addClass("active");
  }
  if (window.location.pathname.indexOf("level3") > 0) {
    $("#level3").addClass("active");
  }
  if (window.location.pathname.indexOf("level4") > 0) {
    $("#level4").addClass("active");
  }
  if (window.location.pathname.indexOf("mapping") > 0) {
    $("#mapping").addClass("active");
  }
  if (window.location.pathname.indexOf("owners") > 0) {
    $("#owners").addClass("active");
  }
  if (window.location.pathname.indexOf("extract_dates") > 0) {
    $("#extract_dates").addClass("active");
  }
  if (window.location.pathname.indexOf("eid_performance") > 0) {
    $("#eid_performance").addClass("active");
  }
  if (window.location.pathname.indexOf("eid_aging") > 0) {
    $("#eid_aging").addClass("active");
  }
  if (window.location.pathname.indexOf("resort_aging") > 0) {
    $("#resort_aging").addClass("active");
  }
  if (window.location.pathname.indexOf("ssc_aging") > 0) {
    $("#ssc_aging").addClass("active");
  }
  if (window.location.pathname.indexOf("aging_template") > 0) {
    $("#aging_template_link").addClass("active");
  }
  if (window.location.pathname.indexOf("aging_published") > 0) {
    $("#aging_published_link").addClass("active");
  }
  if (window.location.pathname.indexOf("credit_meeting_minutes") > 0) {
    console.log("window location pathname contains credit_meeting_minutes");
    if (Cookies.get("filter_resort_published")) {
      $("#filter_resort_published").val(Cookies.get("filter_resort_published"));
      $(
        "#filter_resort_published option:contains(" +
          $(
            "#filter_resort_published option[value=" +
              Cookies.get("filter_resort_published") +
              "]"
          ).text() +
          ")"
      ).prop({ selected: true });
      $("#filter_resort_published").trigger("change");
    }
    $("#credit_meeting_minutes").addClass("active");
    if ($("#hotel_access").text() == "Y") {
      $("#creditMeetingMinutesFields").attr("disabled", true);
      $("#saveCreditMeetingMinutesBtn").attr("disabled", true);
    }
    console.log("assign property name");
    reportLoading.style.display = "none";
  }

  $("#addMappingBtn").on("click", function () {
    console.log("add new record");
    $("#mappingAddModal").modal("show");
  });

  $("#resortFilter").on("keyup", function () {
    var value = $(this).val().toUpperCase();
    console.log(value);
    $table4.bootstrapTable("refresh", {
      url: "https://marriottssc.secure-app.co.za/level4/" + value,
    });
  });

  $("#comment-btn").click(function () {
    console.log($(this).parents("tr"));
  });

  /**
   * When clicking Save Comments
   * on the Add Comment modal on
   * Cust/Hotel or Invoice tabs.
   * Ajax call:https://marriottssc.secure-app.co.za/commentAdd
   * Data from form will be inserted
   * into AR.AR_COMMENTS
   */
  $("#modalBtn").click(function () {
    // var formArray = $("form").serializeArray();
    console.log($("#commentForm").serializeArray());
    if (commentAccountNo.length > 0) {
      $.post(
        "https://marriottssc.secure-app.co.za/commentAdd",
        $("#commentForm").serializeArray(),
        function (data) {
          console.log(data);
        }
      )
        .done(function (msg) {
          alert("Comments added succesfully");
          location.reload();
        })
        .fail(function (error) {
          console.log(error);
          alert("Comments could not be added. Please contact administrator");
        });
    } else {
      alert("No account selected to update!");
    }

    //  location.reload();
  });
});

$.fn.dataTable.moment("DD-MMM-YY");

(function () {
  "use strict";
  window.addEventListener(
    "load",
    function () {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName("needs-validation");
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function (form) {
        form.addEventListener(
          "submit",
          function (event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add("was-validated");
          },
          false
        );
      });
    },
    false
  );
})();

//////////////////////////////////////////////////START OF Market////////////////////////////////////////////////////////////////

/**
 * Tab->Market
 * Aging Summary by Account Type
 * Table ID:level1Table
 * API call to get data: https://marriottssc.secure-app.co.za/api/level1/invoices
 * Data from AR.AR_AGING_LEVEL1
 * Datatables library used to build tables
 *
 */
if (document.querySelector("table[data-id-table=level1Table]")) {
  console.log("level1Table");

  var $test1Table = $("#level1Table");
  console.log($test1Table);
  var comments = "";

  $.get(
    "https://marriottssc.secure-app.co.za/api/level1/invoices",
    function (data) {
      level1Data = data;
      console.log(data);
      $test1Table.bootstrapTable("destroy");
      // $testTable.bootstrapTable({data: level1Data})
      reportLoading.style.display = "none";

      $("#level1Table").DataTable({
        dom: "Bfrtip",
        buttons: [
          {
            extend: "pdfHtml5",
            orientation: "landscape",
            pageSize: "LEGAL",
            extension: ".pdf",
            filename: "Aging Summary by Account Type",
            title: "Aging Summary by Account Type",
          },
          "copy",
          {
            extend: "csvHtml5",
            title: "Aging Summary by Account Type",
            filename: "Aging Summary by Account Type",
          },
          {
            extend: "excelHtml5",
            filename: "Aging Summary by Account Type",
            title: "Aging Summary by Account Type",
          },
          "print",
        ],
        paging: false,
        select: "single",
        scrollY: "550px",
        scrollCollapse: true,
        pageLength: 50,
        data: level1Data,
        columns: [
          { data: "ACCOUNT_TYPE", orderData: [0] },
          {
            data: "AGE_BUCKET1",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET2",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET3",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET4",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET5",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET6",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          { data: "TOTAL", render: $.fn.dataTable.render.number(",", ".", 2) },
        ],

        // order: [[0, 'asc']],

        footerCallback: function (row, data, start, end, display) {
          var api = this.api(),
            data;

          // Remove the formatting to get integer data for summation
          var intVal = function (i) {
            return typeof i === "string"
              ? i.replace(/[\$,]/g, "") * 1
              : typeof i === "number"
              ? i
              : 0;
          };
          age1 = api
            .column(1)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age2 = api
            .column(2)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age3 = api
            .column(3)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age4 = api
            .column(4)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age5 = api
            .column(5)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age6 = api
            .column(6)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          // Total over all pages
          total = api
            .column(7)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);

          age1 = $.fn.dataTable.render.number(",", ".", 2).display(age1);
          age2 = $.fn.dataTable.render.number(",", ".", 2).display(age2);
          age3 = $.fn.dataTable.render.number(",", ".", 2).display(age3);
          age4 = $.fn.dataTable.render.number(",", ".", 2).display(age4);
          age5 = $.fn.dataTable.render.number(",", ".", 2).display(age5);
          age6 = $.fn.dataTable.render.number(",", ".", 2).display(age6);
          total = $.fn.dataTable.render.number(",", ".", 2).display(total);
          // Update footer
          $(api.column(1).footer()).html(age1);
          $(api.column(2).footer()).html(age2);
          $(api.column(3).footer()).html(age3);
          $(api.column(4).footer()).html(age4);
          $(api.column(5).footer()).html(age5);
          $(api.column(6).footer()).html(age6);
          $(api.column(7).footer()).html(total);
        },
      });

      $("#level1Table_filter input").addClass("form-control");
      $("#level1Table_filter input").attr("placeholder", "Search");
    }
  )
    .done(function (msg) {
      reportLoading.style.display = "none";
    })
    .fail(function (xhr, status, error) {
      reportLoading.style.display = "none";
    });
}

//////////////////////////////////////////////////END OF Market////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////START OF Customers////////////////////////////////////////////////////////////////

/**
 * Tab->Customers
 * Aging Summary by Account
 * Table ID:level1Table
 * API call to get data: https://marriottssc.secure-app.co.za/api/level2/invoices
 * Data from AR.AR_AGING_L2_COMMS
 * Datatables library used to build tables
 *
 */
if (document.querySelector("table[data-id-table=level2TableV2]")) {
  console.log("level2TableV2");
  var $test2Table = $("#level2TableV2");
  console.log($test2Table);
  var comments = "";

  $.get(
    "https://marriottssc.secure-app.co.za/api/level2/invoices",
    function (data) {
      level2V2Data = data;
      console.log(data);
      $test2Table.bootstrapTable("destroy");
      // $testTable.bootstrapTable({data: level2V2Data})
      reportLoading.style.display = "none";

      $("#level2TableV2").DataTable({
        dom: "Bfrtip",
        buttons: [
          {
            extend: "pdfHtml5",
            orientation: "landscape",
            pageSize: "LEGAL",
            extension: ".pdf",
            filename: "Aging Summary by Account",
            title: "Aging Summary by Account",
            customize: function (doc) {
              doc.styles.tableHeader.fontSize = 5;
              doc.defaultStyle.fontSize = 5;
            },
          },
          "copy",
          {
            extend: "csvHtml5",
            filename: "Aging Summary by Account",
            title: "Aging Summary by Account",
          },
          {
            extend: "excelHtml5",
            filename: "Aging Summary by Account",
            title: "Aging Summary by Account",
          },
          "print",
        ],
        paging: false,
        select: "single",
        scrollY: "550px",

        scrollCollapse: true,
        pageLength: 50,
        data: level2V2Data,
        columns: [
          { data: "ACCOUNT_TYPE", orderData: [0, 1, 2] },
          { data: "ACCOUNT_NAME" },
          { data: "ACCOUNT_NO" },
          { data: "EID" },
          {
            data: "CREDIT_LIMIT",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET1",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET2",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET3",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET4",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET5",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET6",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          { data: "TOTAL", render: $.fn.dataTable.render.number(",", ".", 2) },
        ],

        // order: [[0, 'asc']],

        footerCallback: function (row, data, start, end, display) {
          var api = this.api(),
            data;

          // Remove the formatting to get integer data for summation
          var intVal = function (i) {
            return typeof i === "string"
              ? i.replace(/[\$,]/g, "") * 1
              : typeof i === "number"
              ? i
              : 0;
          };
          age1 = api
            .column(5)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age2 = api
            .column(6)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age3 = api
            .column(7)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age4 = api
            .column(8)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age5 = api
            .column(9)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age6 = api
            .column(10)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          // Total over all pages
          total = api
            .column(11)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);

          // // Total over this page
          // pageTotal = api
          //     .column( 14, { page: 'current'} )
          //     .data()
          //     .reduce( function (a, b) {
          //         return intVal(a) + intVal(b);
          //     }, 0 );
          age1 = $.fn.dataTable.render.number(",", ".", 2).display(age1);
          age2 = $.fn.dataTable.render.number(",", ".", 2).display(age2);
          age3 = $.fn.dataTable.render.number(",", ".", 2).display(age3);
          age4 = $.fn.dataTable.render.number(",", ".", 2).display(age4);
          age5 = $.fn.dataTable.render.number(",", ".", 2).display(age5);
          age6 = $.fn.dataTable.render.number(",", ".", 2).display(age6);
          total = $.fn.dataTable.render.number(",", ".", 2).display(total);
          // Update footer
          $(api.column(5).footer()).html(age1);
          $(api.column(6).footer()).html(age2);
          $(api.column(7).footer()).html(age3);
          $(api.column(8).footer()).html(age4);
          $(api.column(9).footer()).html(age5);
          $(api.column(10).footer()).html(age6);
          $(api.column(11).footer()).html(total);
        },

        orderFixed: [0, "asc"],
        rowGroup: {
          endRender: null,
          startRender: function (rows, group) {
            var age1 = rows
              .data()
              .pluck("AGE_BUCKET1")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age1 = $.fn.dataTable.render.number(",", ".", 2).display(age1);

            var age2 = rows
              .data()
              .pluck("AGE_BUCKET2")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age2 = $.fn.dataTable.render.number(",", ".", 2).display(age2);

            var age3 = rows
              .data()
              .pluck("AGE_BUCKET3")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age3 = $.fn.dataTable.render.number(",", ".", 2).display(age3);

            var age4 = rows
              .data()
              .pluck("AGE_BUCKET4")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age4 = $.fn.dataTable.render.number(",", ".", 2).display(age4);

            var age5 = rows
              .data()
              .pluck("AGE_BUCKET5")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age5 = $.fn.dataTable.render.number(",", ".", 2).display(age5);

            var age6 = rows
              .data()
              .pluck("AGE_BUCKET6")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age6 = $.fn.dataTable.render.number(",", ".", 2).display(age6);

            var total = rows
              .data()
              .pluck("TOTAL")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            total = $.fn.dataTable.render.number(",", ".", 2).display(total);

            return $("<tr/>")
              .append('<td colspan="5">' + group + "</td>")
              .append("<td>" + age1 + "</td>")
              .append("<td>" + age2 + "</td>")
              .append("<td>" + age3 + "</td>")
              .append("<td>" + age4 + "</td>")
              .append("<td>" + age5 + "</td>")
              .append("<td>" + age6 + "</td>")

              .append("<td>" + total + "</td>");
          },
          dataSrc: "ACCOUNT_TYPE",
        },
      });

      $("#level2TableV2_filter input").addClass("form-control");
      $("#level2TableV2_filter input").attr("placeholder", "Search");
    }
  )
    .done(function (msg) {
      /**
       * If Ajax call was successful
       */
      console.log(msg);
      reportLoading.style.display = "none";

      /**
       * Filter for level2TableV2 Start
       * Click on any cell and then on the yellow filter icon
       * to filter report by that cell value
       */

      /**
       * Remove .filter class from any cells
       * that have previously been clicked.
       * Add filter to the cell most recently clicked.
       * Record index of cell most recently clicked.
       *
       */

      $("#level2TableV2 tbody").on("click", "td", function (e) {
        var x = document.querySelectorAll(".filter");
        for (i = 0; i < x.length; i++) {
          console.log(x[i].classList.remove("filter"));
        }

        var cell = e.target.innerHTML;
        console.log(e.target.classList.add("filter"));
        cellValue = cell;
        console.log(cell);

        var visIdx = $(this).index();
        var dataIdx = $("#level2TableV2")
          .DataTable()
          .column.index("fromVisible", visIdx);
        cellIndex = dataIdx;
        console.log("Column data index: " + dataIdx);
      });

      /**
       * When yellow filter button clicked, filter report by
       * value of most recently clicked cell.
       */
      $("#filterBtn").on("click", function (e) {
        console.log("Filter column: " + cellIndex + " by value: " + cellValue);
        $("#level2TableV2")
          .DataTable()
          .search("")
          .columns(cellIndex)
          .search(cellValue)
          .draw();
      });

      /**
       * Clear all previous filters
       *  */
      $("#filterClearBtn").on("click", function (e) {
        clearLevel2Search();
      });

      /**
       * Filter for level2TableV2 End
       */
    })
    .fail(function (xhr, status, error) {
      /**
       * If Ajax failed
       */
      console.log(error);
      reportLoading.style.display = "none";
    });
}

/**
 * Function to delete all
 * filters
 */
function clearLevel2Search() {
  $("#level2TableV2").DataTable().search("").columns().search("").draw();
}

/**
 * Double click a row to add comments
 * for that row.
 * Data of selected row will be added to
 * Add Comment Modal
 * Modal will then shown on page
 */
$("#level2TableV2").on("dblclick", "tr", function (e) {
  $("#level2TableV2").DataTable().row(this).select();
  var $element = $("#level2TableV2").DataTable().row(this).data();

  commentAccountNo = $element.ACCOUNT_NO;
  commentResort = $element.RESORT;
  commentInvoice = $element.INVOICE_NO;
  if ($element.INVOICE_NO) {
    console.log("There is an invoice no");
    commentLevel = 4;
  } else {
    if ($element.RESORT) {
      commentLevel = 3;
      $("#MODAL_INVOICE_NO").prop("disabled", true);
    } else {
      commentLevel = 2;
      $("#MODAL_INVOICE_NO").prop("disabled", true);
      $("#MODAL_RESORT").prop("disabled", true);
    }
    console.log("No invoice no");
  }
  console.log($element.INVOICE_NO);
  $("#comment3Modal").modal("show");
});

//////////////////////////////////////////////////END OF Customers////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////START OF Cust/ Hotel////////////////////////////////////////////////////////////////

/**
 * Tab->Cust / Hotel
 * Aging Summary by Account & Hotel
 * Table ID:level3Table
 * API call to get data: https://marriottssc.secure-app.co.za/api/level3/invoices
 * Data from AR.AR_AGING_L3_COMMS
 * Datatables library used to build tables
 *
 */

if (document.querySelector("table[data-id-table=level3Table]")) {
  var $test3Table = $("#level3Table");
  console.log($test3Table);
  var comments = "";

  $.get(
    "https://marriottssc.secure-app.co.za/api/level3/invoices",
    function (data) {}
  )
    .done(function (data) {
      level3Data = data;
      console.log(data);
      $test3Table.bootstrapTable("destroy");
      // $testTable.bootstrapTable({data: level3Data})
      reportLoading.style.display = "none";

      $("#level3Table").DataTable({
        dom: "Bfrtip",
        buttons: [
          {
            extend: "pdfHtml5",
            orientation: "landscape",
            pageSize: "LEGAL",
            extension: ".pdf",
            filename: "Aging Summary by Account and Hotel",
            title: "Aging Summary by Account and Hotel",
            customize: function (doc) {
              doc.styles.tableHeader.fontSize = 4;
              doc.defaultStyle.fontSize = 4;
            },
          },
          "copy",
          {
            extend: "csvHtml5",
            title: "Aging Summary by Account and Hotel",
            filename: "Aging Summary by Account and Hotel",
          },
          {
            extend: "excelHtml5",
            title: "Aging Summary by Account and Hotel",
            filename: "Aging Summary by Account and Hotel",
          },
          "print",
        ],
        paging: false,
        select: "single",
        scrollY: "550px",
        scrollX: "2400px",
        scrollCollapse: true,
        pageLength: 50,
        data: level3Data,
        columns: [
          { data: "ACCOUNT_TYPE", orderData: [0, 1, 4] },
          { data: "ACCOUNT_NAME" },
          { data: "ACCOUNT_NO" },
          { data: "EID" },
          {
            data: "CREDIT_LIMIT",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          { data: "RESORT" },
          { data: "RESORT_NAME" },
          {
            data: "AGE_BUCKET1",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET2",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET3",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET4",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET5",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET6",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          { data: "TOTAL", render: $.fn.dataTable.render.number(",", ".", 2) },
          { data: "RC" },
          { data: "RC_ID_DATE" },
          { data: "EMAIL_CALL_DATE" },
          { data: "L1" },
          { data: "L2" },
          { data: "L3" },
          { data: "CC" },
          { data: "CC_ID_DATE" },
        ],

        // order: [[2, 'asc']],

        footerCallback: function (row, data, start, end, display) {
          var api = this.api(),
            data;

          // Remove the formatting to get integer data for summation
          var intVal = function (i) {
            return typeof i === "string"
              ? i.replace(/[\$,]/g, "") * 1
              : typeof i === "number"
              ? i
              : 0;
          };
          age1 = api
            .column(7)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age2 = api
            .column(8)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age3 = api
            .column(9)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age4 = api
            .column(10)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age5 = api
            .column(11)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          age6 = api
            .column(12)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);
          // Total over all pages
          total = api
            .column(13)
            .data()
            .reduce(function (a, b) {
              return intVal(a) + intVal(b);
            }, 0);

          // // Total over this page
          // pageTotal = api
          //     .column( 14, { page: 'current'} )
          //     .data()
          //     .reduce( function (a, b) {
          //         return intVal(a) + intVal(b);
          //     }, 0 );
          age1 = $.fn.dataTable.render.number(",", ".", 2).display(age1);
          age2 = $.fn.dataTable.render.number(",", ".", 2).display(age2);
          age3 = $.fn.dataTable.render.number(",", ".", 2).display(age3);
          age4 = $.fn.dataTable.render.number(",", ".", 2).display(age4);
          age5 = $.fn.dataTable.render.number(",", ".", 2).display(age5);
          age6 = $.fn.dataTable.render.number(",", ".", 2).display(age6);
          total = $.fn.dataTable.render.number(",", ".", 2).display(total);
          // Update footer
          $(api.column(7).footer()).html(age1);
          $(api.column(8).footer()).html(age2);
          $(api.column(9).footer()).html(age3);
          $(api.column(10).footer()).html(age4);
          $(api.column(11).footer()).html(age5);
          $(api.column(12).footer()).html(age6);
          $(api.column(13).footer()).html(total);
        },

        orderFixed: [2, "asc"],
        rowGroup: {
          endRender: null,
          startRender: function (rows, group) {
            var accounts = rows.data().pluck("ACCOUNT_NAME");

            // .reduce( function (a, b) {
            //     return b ;
            // }, 0) ;
            // const distinctAccount = [...new Set(accounts)];
            var account = accounts[0];

            var age1 = rows
              .data()
              .pluck("AGE_BUCKET1")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age1 = $.fn.dataTable.render.number(",", ".", 2).display(age1);

            var age2 = rows
              .data()
              .pluck("AGE_BUCKET2")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age2 = $.fn.dataTable.render.number(",", ".", 2).display(age2);

            var age3 = rows
              .data()
              .pluck("AGE_BUCKET3")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age3 = $.fn.dataTable.render.number(",", ".", 2).display(age3);

            var age4 = rows
              .data()
              .pluck("AGE_BUCKET4")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age4 = $.fn.dataTable.render.number(",", ".", 2).display(age4);

            var age5 = rows
              .data()
              .pluck("AGE_BUCKET5")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age5 = $.fn.dataTable.render.number(",", ".", 2).display(age5);

            var age6 = rows
              .data()
              .pluck("AGE_BUCKET6")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            age6 = $.fn.dataTable.render.number(",", ".", 2).display(age6);

            var total = rows
              .data()
              .pluck("TOTAL")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            total = $.fn.dataTable.render.number(",", ".", 2).display(total);

            var credit_limit = rows
              .data()
              .pluck("CREDIT_LIMIT")
              .reduce(function (a, b) {
                return a + b * 1;
              }, 0);
            credit_limit = $.fn.dataTable.render
              .number(",", ".", 2)
              .display(credit_limit);

            return $("<tr/>")
              .append('<td colspan="2">' + account + "</td>")
              .append("<td >" + group + "</td>")
              .append("<td ></td>")
              .append("<td >" + credit_limit + "</td>")
              .append("<td ></td>")
              .append("<td ></td>")
              .append("<td>" + age1 + "</td>")
              .append("<td>" + age2 + "</td>")
              .append("<td>" + age3 + "</td>")
              .append("<td>" + age4 + "</td>")
              .append("<td>" + age5 + "</td>")
              .append("<td>" + age6 + "</td>")

              .append("<td>" + total + "</td>")
              .append('<td colspan="8"></td>');
          },
          dataSrc: "ACCOUNT_NO",
        },
      });

      $("#level3Table_filter input").addClass("form-control");
      $("#level3Table_filter input").attr("placeholder", "Search");
      /**
       * if Ajax call was successful
       */
      reportLoading.style.display = "none";

      /**
       * Filter for level3Table Start
       * Click on any cell and then on the yellow filter icon
       * to filter report by that cell value
       */

      /**
       * Remove .filter class from any cells
       * that have previously been clicked.
       * Add filter to the cell most recently clicked.
       * Record index of cell most recently clicked.
       *
       */

      $("#level3Table tbody").on("click", "td", function (e) {
        var x = document.querySelectorAll(".filter");
        for (i = 0; i < x.length; i++) {
          console.log(x[i].classList.remove("filter"));
        }

        var cell = e.target.innerHTML;
        console.log(e.target.classList.add("filter"));
        cellValue = cell;
        console.log(cell);

        var visIdx = $(this).index();
        var dataIdx = $("#level3Table")
          .DataTable()
          .column.index("fromVisible", visIdx);
        cellIndex = dataIdx;
        console.log("Column data index: " + dataIdx);
      });

      /**
       * When yellow filter button clicked, filter report by
       * value of most recently clicked cell.
       */
      $("#filterBtn").on("click", function (e) {
        console.log("Filter column: " + cellIndex + " by value: " + cellValue);
        $("#level3Table")
          .DataTable()
          .search("")
          .columns(cellIndex)
          .search(cellValue)
          .draw();
      });
      /**
       * Clear all previous filters
       */
      $("#filterClearBtn").on("click", function (e) {
        clearLevel3Search();
      });

      /**
       * Filter for level3Table End
       */
    })
    .fail(function (xhr, status, error) {
      console.log("request failed");
      console.log(xhr);
      console.log(error);
      /**
       * If Ajax call failed
       */
      reportLoading.style.display = "none";
    });
}

/**
 * Double click a row to add comments
 * for that row.
 * Data of selected row will be added to
 * Add Comment Modal
 * Modal will then shown on page
 */
$("#level3Table").on("dblclick", "tr", function (e) {
  $("#level3Table").DataTable().row(this).select();
  var $element = $("#level3Table").DataTable().row(this).data();

  commentAccountNo = $element.ACCOUNT_NO;
  commentResort = $element.RESORT;
  commentInvoice = $element.INVOICE_NO;
  if ($element.INVOICE_NO) {
    console.log("There is an invoice no");
    commentLevel = 4;
  } else {
    if ($element.RESORT) {
      commentLevel = 3;
      $("#MODAL_INVOICE_NO").prop("disabled", true);
    } else {
      commentLevel = 2;
      $("#MODAL_INVOICE_NO").prop("disabled", true);
      $("#MODAL_RESORT").prop("disabled", true);
    }
    console.log("No invoice no");
  }
  console.log($element.INVOICE_NO);
  $("#comment3Modal").modal("show");
});

/**
 * Function to delete all
 * filters
 */
function clearLevel3Search() {
  $("#level3Table").DataTable().search("").columns().search("").draw();
}

//////////////////////////////////////////////////END OF Cust/ Hotel////////////////////////////////////////////////////////////////

$("#comment3Modal").on("show.bs.modal", function (event) {
  var modal = $(this);
  modal.find(".modal-title").text("Add Comment");
  // modal.find('.modal-body p').html("Account: " + commentAccountNo + "<br>Resort: " + commentResort)
  $("#MODAL_ACCOUNT_NO").val(commentAccountNo);
  $("#MODAL_RESORT").val(commentResort);
  $("#MODAL_INVOICE_NO").val(commentInvoice);
  $("#MODAL_LEVEL").val(commentLevel);
});

function formatRow(value) {
  return value;
}

function dateFormat(value) {
  //
}

function rowTotalFormatter(value) {
  var floatValue = parseFloat(value);
  return formatNumber(floatValue) == 0 ? null : formatNumber(floatValue);
}

function exportCSV() {
  $("#summaryTable").tableExport({ type: "csv" });
}

function exportCSV() {
  $("#testTable").tableExport({ type: "csv" });
}

function filterAR(filter) {
  $("#summaryTable").bootstrapTable("filterBy", { TYPE: filter });
  $("#testTable").bootstrapTable("filterBy", { TYPE: filter });
}

function getAccnt_NoSelections() {
  return $.map(
    $("#summaryTable").bootstrapTable("getSelections"),
    function (row) {
      return row.ACCOUNT_NO;
    }
  );
}

function getAcctResortSelections() {
  return $.map(
    $("#summaryTable").bootstrapTable("getSelections"),
    function (row) {
      return row.ACCOUNT_NO + "/" + row.RESORT;
      $("#myModal").modal("show");
    }
  );
}

//////////////////////////////////////////////////START OF Invoice////////////////////////////////////////////////////////////////

/**
 * Tab->Invoice
 * Aging Detail by Account & Hotel
 * Table ID:level4test
 * API call to get data: https://marriottssc.secure-app.co.za/api/level4/ownerinvoices
 * Data from AR.AR_AGING_L4_COMMS
 * Datatables library used to build tables
 *
 */

/**
 * Get default EID stored in a cookie
 * in order to default the EID/Owner
 * filter.
 */
if (Cookies.get("eid")) {
  var defaultEID = Cookies.get("eid");
  $("#selectEID option[value='" + Cookies.get("eid") + "']").attr(
    "selected",
    "selected"
  );
} else {
  $("#selectEID option[value='" + $("#session_user") + "']").attr(
    "selected",
    "selected"
  );
  var defaultEID = $("#selectEID").find(":selected").val();
}

/**
 * If the table ID is level4test, run
 * the function to get the data using
 * Ajax call and build table using default
 * EID as filter.
 */
if (document.querySelector("table[data-id-table=level4test]")) {
  console.log("running level4testAjax with " + defaultEID);
  level4testAjax(defaultEID);
}

/**
 * When changing the EID filter,
 * store the new EID in a cookie to
 * be used when user logs in again.
 * Rebuild table using new EID value.
 */
$("#selectEID").change(function () {
  reportLoading.style.display = "block";
  console.log($(this).val());
  Cookies.set("eid", $(this).val(), { expires: 30 });
  $("#level4test").DataTable().destroy();
  level4testAjax($(this).val());
});

/**
 * When selecting a row, mark it as selected
 */
$("#level4test tbody").on("click", "tr", function () {
  $(this).toggleClass("selected");
});

/**
 * When clicking the Add Comments button
 * Count the number of rows selected
 * Add all selected rows to an array
 * Display multi-comment modal
 *
 */
commentsArray = [];
$("#addCommentsBtn").click(function () {
  var accountStr = "";
  var resortStr = "";
  var invoice_noStr = "";
  var thisRow = [];
  console.log($("#level4test").DataTable().rows(".selected").data());
  selectedRows = $("#level4test").DataTable().rows(".selected").data();
  numberOfRows = $("#level4test").DataTable().rows(".selected").data().length;
  selectedRows.map(function ($element, index) {
    thisRow["ACCOUNT_NO"] = $element.ACCOUNT_NO;
    accountStr = accountStr + $element.ACCOUNT_NO + ", ";
    thisRow["RESORT"] = $element.RESORT;
    resortStr = resortStr + $element.RESORT + ", ";
    thisRow["INVOICE_NO"] = $element.INVOICE_NO;
    invoice_noStr = invoice_noStr + $element.INVOICE_NO + ", ";
    commentsArray.push(thisRow);
  });

  commentAccountNo = accountStr;
  commentResort = resortStr;
  commentInvoice = invoice_noStr;

  commentLevel = 4;

  $("#multiCommentModal").modal("show");
});

var csrfTokens = null;
/**
 * when Multi-comment modal displays,
 * show list of invoices that will be updated.
 */
$("#multiCommentModal").on("show.bs.modal", function (event) {
  var modal = $(this);
  modal.find(".modal-title").text("Add Comments");
  $("#MODAL_INVOICE_LIST").text(commentInvoice);
});

/**
 * When Save Comment is clicked on Add Comments
 * modal for multiple invoices
 * Ajax URL:https://marriottssc.secure-app.co.za/commentAdd
 * Call is once made or each selected row
 */
$("#multiCommentModalBtn").click(function () {
  $("#multiCommentModalBtn").attr("disabled", true);

  selectedRows.map(function ($element, index) {
    console.log("index: " + index + " Number or rows: " + numberOfRows);
    $("#MULTI_ACCOUNT_NO").val($element.ACCOUNT_NO);
    $("#MULTI_RESORT").val($element.RESORT);
    $("#MULTI_INVOICE_NO").val($element.INVOICE_NO);
    $("#MULTI_MODAL_LEVEL").val(4);

    var formData = $("#multiCommentModalForm").serializeArray();
    console.log(formData);

    if ($element.ACCOUNT_NO && $element.RESORT && $element.INVOICE_NO) {
      console.log("post comment for invoice " + $element.INVOICE_NO);
      $.post(
        "https://marriottssc.secure-app.co.za/commentAdd",
        formData,
        function (data) {
          console.log(data);
        }
      )
        .done(function (msg) {
          console.log("Comments added succesfully for " + $element.INVOICE_NO);
          if (index + 1 == numberOfRows) {
            location.reload();
          }
        })
        .fail(function (error) {
          console.log(error);
          // alert("Comments could not be added. Please contact administrator" + error);
        });
    } else {
      alert(
        "Cannot add comments for " +
          $element.ACCOUNT_NO +
          " and hotel " +
          $element.RESORT +
          " to this level without an invoice number."
      );
    }
  });
});
/**
 * Tab: Invoice
 * Report: Aging Detail by Account and Hotel
 * Function that does Ajax call
 * to https://marriottssc.secure-app.co.za/api/level4/ownerinvoices
 * and rebuild table with data receive.
 *
 */
function level4testAjax(eid = null) {
  console.log("inside level4testAjax");

  if (1 === 1) {
    // deleteOwnersModal
    console.log("1===1");

    var $testTable = $("#level4test");

    $("#level4test").DataTable().destroy();
    $("#level4test tbody").empty();

    var comments = "";

    eidStr = "";
    if (eid) {
      eidStr = "/" + eid;
    }

    $.get(
      "https://marriottssc.secure-app.co.za/api/level4/ownerinvoices" + eidStr,
      function (data) {
        console.log(
          "running ajax: https://marriottssc.secure-app.co.za/api/level4/ownerinvoices" +
            eidStr
        );
      }
    )
      .done(function (data) {
        // alert( msg );
        console.log("DONE");
        console.log(data);
        //  console.log(result);
        reportLoading.style.display = "none";
        console.log("Starting to build datatable:");
        $("#level4test").DataTable({
          dom: "Bfrtip",
          buttons: [
            {
              extend: "pdfHtml5",
              orientation: "landscape",
              pageSize: "LEGAL",
              extension: ".pdf",
              filename: "Aging Detail by Account and Hotel",
              title: "Aging Detail by Account and Hotel",
              customize: function (doc) {
                doc.styles.tableHeader.fontSize = 4;
                doc.defaultStyle.fontSize = 4;
              },
            },
            "copy",
            {
              extend: "csvHtml5",
              filename: "Aging Detail by Account and Hotel",
              title: "Aging Detail by Account and Hotel",
            },
            {
              extend: "excelHtml5",
              filename: "Aging Detail by Account and Hotel",
              title: "Aging Detail by Account and Hotel",
            },
            "print",
          ],
          paging: false,
          select: "single",
          scrollY: "550px",
          scrollX: "2500px",
          scrollCollapse: true,
          pageLength: 50,
          data: data,
          select: {
            style: "os",
          },
          columns: [
            { data: "ACCOUNT_TYPE" },
            { data: "ACCOUNT_NAME" },
            { data: "ACCOUNT_NO" },
            { data: "EID" },
            {
              data: "CREDIT_LIMIT",
              render: $.fn.dataTable.render.number(",", ".", 2),
            },
            { data: "RESORT" },
            { data: "RESORT_NAME" },
            { data: "INVOICE_NO" },
            { data: "FOLIO_NO" },
            { data: "POST_DATE" },
            {
              data: "AGE_BUCKET1",
              render: $.fn.dataTable.render.number(",", ".", 2),
            },
            {
              data: "AGE_BUCKET2",
              render: $.fn.dataTable.render.number(",", ".", 2),
            },
            {
              data: "AGE_BUCKET3",
              render: $.fn.dataTable.render.number(",", ".", 2),
            },
            {
              data: "AGE_BUCKET4",
              render: $.fn.dataTable.render.number(",", ".", 2),
            },
            {
              data: "AGE_BUCKET5",
              render: $.fn.dataTable.render.number(",", ".", 2),
            },
            {
              data: "AGE_BUCKET6",
              render: $.fn.dataTable.render.number(",", ".", 2),
            },
            {
              data: "TOTAL",
              render: $.fn.dataTable.render.number(",", ".", 2),
            },
            { data: "INVOICE_AGE" },
            { data: "GUEST_NAME" },
            { data: "COMPANY" },
            { data: "MARKET_CODE" },
            { data: "OWNER" },
            { data: "DISPATCH_DATE" },
            { data: "PTP" },
            { data: "RC" },
            { data: "RC_ID_DATE" },
            { data: "EMAIL_CALL_DATE" },
            { data: "L1" },
            { data: "L2" },
            { data: "L3" },
            { data: "CC" },
            { data: "CC_ID_DATE" },
          ],

          rowCallback: function (row, data, index) {
            console.log("rowCallback running");
            var ptp = data.PTP;
            var lastptp = "";
            if (ptp) {
              if (ptp.indexOf("<") > 0) {
                lastptp = ptp.substring(0, ptp.indexOf("<"));
              } else {
                lastptp = ptp;
              }
              var ptpmils = Date.parse(lastptp) + 82800000;
              if (ptpmils < Date.now()) {
                // console.log("PTP: " + ptpmils);
                // console.log("Now: " + Date.now());
                // console.log(row);
                var cols = $("#level4test").DataTable().columns()[0].length;
                // console.log(cols);
                for (var i = 0; i < cols; i++) {
                  $("td:eq(" + i + ")", row).addClass("stale");
                }
              }
            }
          },

          orderFixed: [
            [2, "asc"],
            [5, "asc"],
          ],

          //2022-09-27 footerCallback total calculations removed as it was using too much memory and causing browser to bomb out. Ilana
          // footerCallback: function (row, data, start, end, display) {
          //   console.log("footerCallback running");
          //   var api = this.api(),
          //     data;

          //   // Remove the formatting to get integer data for summation
          //   var intVal = function (i) {
          //     return typeof i === "string"
          //       ? i.replace(/[\$,]/g, "") * 1
          //       : typeof i === "number"
          //       ? i
          //       : 0;
          //   };
          //   age1 = api
          //     .column(10)
          //     .data()
          //     .reduce(function (a, b) {
          //       return intVal(a) + intVal(b);
          //     }, 0);
          //   age2 = api
          //     .column(11)
          //     .data()
          //     .reduce(function (a, b) {
          //       return intVal(a) + intVal(b);
          //     }, 0);
          //   age3 = api
          //     .column(12)
          //     .data()
          //     .reduce(function (a, b) {
          //       return intVal(a) + intVal(b);
          //     }, 0);
          //   age4 = api
          //     .column(13)
          //     .data()
          //     .reduce(function (a, b) {
          //       return intVal(a) + intVal(b);
          //     }, 0);
          //   age5 = api
          //     .column(14)
          //     .data()
          //     .reduce(function (a, b) {
          //       return intVal(a) + intVal(b);
          //     }, 0);
          //   age6 = api
          //     .column(15)
          //     .data()
          //     .reduce(function (a, b) {
          //       return intVal(a) + intVal(b);
          //     }, 0);
          //   // Total over all pages
          //   total = api
          //     .column(16)
          //     .data()
          //     .reduce(function (a, b) {
          //       return intVal(a) + intVal(b);
          //     }, 0);

          //   age1 = $.fn.dataTable.render.number(",", ".", 2).display(age1);
          //   age2 = $.fn.dataTable.render.number(",", ".", 2).display(age2);
          //   age3 = $.fn.dataTable.render.number(",", ".", 2).display(age3);
          //   age4 = $.fn.dataTable.render.number(",", ".", 2).display(age4);
          //   age5 = $.fn.dataTable.render.number(",", ".", 2).display(age5);
          //   age6 = $.fn.dataTable.render.number(",", ".", 2).display(age6);
          //   total = $.fn.dataTable.render.number(",", ".", 2).display(total);
          //   // Update footer
          //   $(api.column(10).footer()).html(age1);
          //   $(api.column(11).footer()).html(age2);
          //   $(api.column(12).footer()).html(age3);
          //   $(api.column(13).footer()).html(age4);
          //   $(api.column(14).footer()).html(age5);
          //   $(api.column(15).footer()).html(age6);
          //   $(api.column(16).footer()).html(total);
          // },

          orderFixed: [2, 5, "desc"],

          rowGroup: {
            endRender: null,
            startRender: function (rows, group) {
              groupDesc = "";
              var account_names = rows.data().pluck("ACCOUNT_NAME");
              var account_name = account_names[0];

              var resort_names = rows.data().pluck("RESORT_NAME");
              var resort_name = resort_names[0];

              var accounts = rows.data().pluck("ACCOUNT_NO");
              var account = accounts[0];

              var resorts = rows.data().pluck("RESORT");
              var resort = resorts[0];

              if (group == resort) {
                groupDesc = resort_name;
              } else {
                groupDesc = account_name;
              }

              var age1 = rows
                .data()
                .pluck("AGE_BUCKET1")
                .reduce(function (a, b) {
                  return a + b * 1;
                }, 0);
              age1 = $.fn.dataTable.render.number(",", ".", 2).display(age1);

              var age2 = rows
                .data()
                .pluck("AGE_BUCKET2")
                .reduce(function (a, b) {
                  return a + b * 1;
                }, 0);
              age2 = $.fn.dataTable.render.number(",", ".", 2).display(age2);

              var age3 = rows
                .data()
                .pluck("AGE_BUCKET3")
                .reduce(function (a, b) {
                  return a + b * 1;
                }, 0);
              age3 = $.fn.dataTable.render.number(",", ".", 2).display(age3);

              var age4 = rows
                .data()
                .pluck("AGE_BUCKET4")
                .reduce(function (a, b) {
                  return a + b * 1;
                }, 0);
              age4 = $.fn.dataTable.render.number(",", ".", 2).display(age4);

              var age5 = rows
                .data()
                .pluck("AGE_BUCKET5")
                .reduce(function (a, b) {
                  return a + b * 1;
                }, 0);
              age5 = $.fn.dataTable.render.number(",", ".", 2).display(age5);

              var age6 = rows
                .data()
                .pluck("AGE_BUCKET6")
                .reduce(function (a, b) {
                  return a + b * 1;
                }, 0);
              age6 = $.fn.dataTable.render.number(",", ".", 2).display(age6);

              var total = rows
                .data()
                .pluck("TOTAL")
                .reduce(function (a, b) {
                  return a + b * 1;
                }, 0);
              total = $.fn.dataTable.render.number(",", ".", 2).display(total);

              return $("<tr/>")
                .append(
                  '<td id="groupByDesc" colspan="10">' +
                    groupDesc +
                    "  -   " +
                    group +
                    "</td>"
                )
                .append("<td>" + age1 + "</td>")
                .append("<td>" + age2 + "</td>")
                .append("<td>" + age3 + "</td>")
                .append("<td>" + age4 + "</td>")
                .append("<td>" + age5 + "</td>")
                .append("<td>" + age6 + "</td>")

                .append("<td>" + total + "</td>")
                .append('<td colspan="14"></td>');
            },
            dataSrc: ["ACCOUNT_NO", "RESORT"],
          },
        });
        console.log("Datatable build completed");
        $("#level4test_filter input").addClass("form-control");
        $("#level4test_filter input").attr("placeholder", "Search");

        /**
         * Filter for level4test Start
         * Click on any cell and then on the yellow filter icon
         * to filter report by that cell value
         */

        /**
         * Remove .filter class from any cells
         * that have previously been clicked.
         * Add filter to the cell most recently clicked.
         * Record index of cell most recently clicked.
         *
         */

        $("#level4test tbody").on("click", "td", function (e) {
          var x = document.querySelectorAll(".filter");
          for (i = 0; i < x.length; i++) {
            // console.log(x[i].classList.remove('filter'));
          }

          var cell = e.target.innerHTML;
          // console.log(e.target.classList.add('filter'));
          cellValue = cell;
          // console.log(cell);

          var visIdx = $(this).index();
          var dataIdx = $("#level4test")
            .DataTable()
            .column.index("fromVisible", visIdx);
          cellIndex = dataIdx;
          // console.log('Column data index: ' + dataIdx);
        });

        /**
         * When yellow filter button clicked, filter report by
         * value of most recently clicked cell.
         */
        $("#filterBtn").on("click", function (e) {
          // console.log("Filter column: " + cellIndex + " by value: " + cellValue);
          $("#level4test")
            .DataTable()
            .search("")
            .columns(cellIndex)
            .search(cellValue)
            .draw();
        });

        /**
         * Delete all previous filters
         */
        $("#filterClearBtn").on("click", function (e) {
          clearLevel4Search();
        });

        /**
         * Filter for level4test end
         */
      })
      .fail(function (result) {
        // alert(error);
        // console.log("failed");
        // console.log(result);
        reportLoading.style.display = "none";
      })
      .always(function (data, textStatus, error) {
        // console.log(data);
        // console.log(textStatus);
        // console.log(error);
        //  console.log("result");
        //  console.log(result);
        reportLoading.style.display = "none";
      });
  }
}

/**
 * Double click a row to add comments
 * for that row.
 * Data of selected row will be added to
 * Add Comment Modal
 * Modal will then shown on page
 */
$("#level4test").on("dblclick", "tr", function (e) {
  $("#level4test").DataTable().row(this).select();
  var $element = $("#level4test").DataTable().row(this).data();

  if ($element) {
    commentAccountNo = $element.ACCOUNT_NO;
    commentResort = $element.RESORT;
    commentInvoice = $element.INVOICE_NO;
    if ($element.INVOICE_NO) {
      // console.log("There is an invoice no");
      commentLevel = 4;
    } else {
      if ($element.RESORT) {
        commentLevel = 3;
        $("#MODAL_INVOICE_NO").prop("disabled", true);
      } else {
        commentLevel = 2;
        $("#MODAL_INVOICE_NO").prop("disabled", true);
        $("#MODAL_RESORT").prop("disabled", true);
      }
      // console.log("No invoice no");
    }
    // console.log($element.INVOICE_NO);
    $("#comment3Modal").modal("show");
  } else {
    // console.log(e.currentTarget.childNodes[0].innerHTML);
  }
});

/**
 * Function to delete all
 * filters
 */
function clearLevel4Search() {
  $("#level4test").DataTable().search("").columns().search("").draw();
}

//////////////////////////////////////////////////END OF Invoice////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////START OF Mapping////////////////////////////////////////////////////////////////

/**
 * Tab: Mapping
 * Report: table shows the mapping
 * from PMS Account Number to
 * central Account number.
 * View: D:\Apache24\htdocs\marriott_ar\src\resources\views\mappingV4.twig
 * API call: https://marriottssc.secure-app.co.za/api/mappingV4
 */

function buildMappingTable() {
  var $mappingV4DataTable = $("#mappingV4DataTable");
  // console.log($mappingV4DataTable);
  var comments = "";

  /**
   * Ajax call to obtain mapping
   * from
   *
   */
  reportLoading.style.display = "block";
  $("#mappingV4DataTable").DataTable().destroy();
  $.get("https://marriottssc.secure-app.co.za/api/mappingV4", function (data) {
    mappingData = data;
    // console.log(data);
    // $mappingV4DataTable.bootstrapTable("destroy");
    // $testTable.bootstrapTable({data: mappingData})

    $("#mappingV4DataTable").DataTable({
      orderCellsTop: true,
      fixedHeader: true,
      dom: "Bfrtip",
      buttons: [
        {
          extend: "pdfHtml5",
          orientation: "landscape",
          pageSize: "LEGAL",
          extension: ".pdf",
          filename: "AR Account Mapping",
          title: "AR Account Mapping",
        },
        "copy",
        {
          extend: "csvHtml5",
          title: "AR Account Mapping",
          filename: "AR Account Mapping",
        },
        {
          extend: "excelHtml5",
          filename: "AR Account Mapping",
          title: "AR Account Mapping",
        },
        "print",
      ],
      paging: false,
      select: "single",
      scrollY: "550px",
      scrollX: "2500px",
      scrollCollapse: true,
      pageLength: 50,
      data: mappingData,
      columns: [
        { data: "RESORT", orderData: [0, 1] },
        { data: "RESORT_NAME" },
        { data: "ACCOUNT_TYPE", visible: false },
        { data: "ACCOUNT_TYPE_DESC" },
        { data: "ACCOUNT_NO_FROM" },
        { data: "ACCOUNT_NO_TO" },
        { data: "ACCOUNT_NAME" },
        { data: "CONTACT" },
        { data: "PAYMENT_METHOD" },
        { data: "EID" },
        { data: "INSERT_DATE" },
        { data: "INSERT_USER" },
        { data: "UPDATE_DATE" },
        { data: "UPDATE_USER" },
        { data: "MAPPED" },
        { data: "REQUIRED_DOCS" },
        { data: "DOCS_UPLOADED" },
      ],

      rowCallback: function (row, data, index) {
        if (data.MAPPED == "UNMAPPED" || data.MAPPED == null) {
          // console.log(row);
          var cols = $("#mappingV4DataTable").DataTable().columns()[0].length;
          console.log(cols);
          for (var i = 0; i < cols; i++) {
            $("td:eq(" + i + ")", row).addClass("stale");
          }
        }
      },
    });

    $("#mappingV4DataTable_filter input").addClass("form-control");
    $("#mappingV4DataTable_filter input").attr("placeholder", "Search");
  })
    .done(function (msg) {
      reportLoading.style.display = "none";

      /**
       * When clicking on any cell inside the mapping
       * table, filter the mapping table by that
       * cell value when clicking on the yellow filter
       * button
       */
      $("#mappingV4DataTable tbody").on("click", "td", function (e) {
        var x = document.querySelectorAll(".filter");
        for (i = 0; i < x.length; i++) {
          console.log(x[i].classList.remove("filter"));
        }

        var cell = e.target.innerHTML;
        console.log(e.target.classList.add("filter"));
        cellValue = cell;
        console.log(cell);

        var visIdx = $(this).index();
        var dataIdx = $("#mappingV4DataTable")
          .DataTable()
          .column.index("fromVisible", visIdx);
        cellIndex = dataIdx;
        console.log("Column data index: " + dataIdx);
      });

      /**
       * When clicking on yellow filter button
       * use value obtained from the functio above
       * to filter the report.
       */
      $("#filterBtn").on("click", function (e) {
        console.log("Filter column: " + cellIndex + " by value: " + cellValue);
        $mappingV4DataTable
          .DataTable()
          .search("")
          .columns(cellIndex)
          .search(cellValue)
          .draw();
      });

      $("#filterClearBtn").on("click", function (e) {
        clearMappingSearch();
      });
    })
    .fail(function (xhr, status, error) {
      reportLoading.style.display = "none";
    });
}

/**
 * If user does not have access to add
 * mapping, button to add mapping
 * will be disabled
 */

if (document.querySelector("table[data-id-table=mappingV4DataTable]")) {
  if ($("#access_allowed").text() == "N") {
    $("#ACCOUNT_NAME").attr("readonly", true);
    $("#TO_ACCOUNT").attr("readonly", true);
    $("#ACCOUNT_TYPE").prop("disabled", "disabled");
    $("#addMappingV4Btn").attr("disabled", true);
    $("#mappingV4DeleteModalBtn").attr("disabled", true);
    // $('#mappingV4ChangeModalBtn').attr('disabled', true);
  }

  buildMappingTable();
}

/**
 * Function to delete all
 * filters
 */
function clearMappingSearch() {
  $mappingV4DataTable.DataTable().search("").columns().search("").draw();
}

/**
 * When double clicking on a row in the mapping table,
 *  assign the values of the row to variables
 * and show the Change Account Mapping modal
 */
$("#mappingV4DataTable").on("dblclick", "tr", function (e) {
  $("#mappingV4DataTable").DataTable().row(this).select();
  var $element = $("#mappingV4DataTable").DataTable().row(this).data();
  mapAccountFrom = $element.ACCOUNT_NO_FROM;
  mapAccountTo = $element.ACCOUNT_NO_TO;
  mapResort = $element.RESORT;
  mapAccountName = $element.ACCOUNT_NAME;
  mapAccountContact = $element.CONTACT;
  mapAccountPayment = $element.PAYMENT_METHOD;
  mapAccountEID = $element.EID;
  mapAccountType = $element.ACCOUNT_TYPE;
  mapRequiredDocs = $element.REQUIRED_DOCS;
  console.log(mapAccountFrom + " to " + mapAccountTo);
  $("#mappingV4ChangeModal").modal("show");
});

/**
 * When the Change Account Mapping modal
 * is made visible, assign the values
 * of the variables(row values) above to the modal
 * form fields.
 *
 */
$("#mappingV4ChangeModal").on("show.bs.modal", function (event) {
  var modal = $(this);
  // modal.find('.modal-body p').html("Account: " + commentAccountNo + "<br>Resort: " + commentResort)
  $("#RESORT").val(mapResort);
  $("#ACCOUNT_TYPE").val(mapAccountType);
  $("#FROM_ACCOUNT").val(mapAccountFrom);
  $("#TO_ACCOUNT").val(mapAccountTo);
  $("#ACCOUNT_NAME").val(mapAccountName);
  $("#CONTACT").val(mapAccountContact);
  $("#PAYMENT").val(mapAccountPayment);
  $("#EID").val(mapAccountEID);
  $("#REQUIRED_DOCS").val(mapRequiredDocs);
});

/**
 * When clicking the Add New Mapping
 * button, open the Add Account Mapping
 * modal.
 */
$("#addMappingV4Btn").on("click", function () {
  console.log("add new record");
  $("#mappingV4AddModal").modal("show");
});

/**
 * When clicking on Save Changes in
 * Add Account Mapping modal,
 * check that all required fields are
 * completed and do ajax call to
 * insert data into AR.AR_ACCOUNT_MAP
 */
$("#mappingV4AddModalBtn").click(function () {
  var form = document.getElementById("mappingV4AddModalForm");
  var formData = new FormData(form);
  // var formArray = $("#mappingV4AddModalForm").serializeArray();
  // console.log(formArray);
  if (
    $("#RESORT_NEW").val().length > 0 &&
    $("#FROM_ACCOUNT_NEW").val().length > 0 &&
    $("#TO_ACCOUNT_NEW").val().length > 0 &&
    $("#ACCOUNT_NAME_NEW").val().length > 0 &&
    $("#CONTACT_NEW").val().length > 0
  ) {
    console.log("Add mapping");

    $.ajax({
      type: "post",
      url: "https://marriottssc.secure-app.co.za/addMappingV4",
      processData: false,
      contentType: false,
      dataType: "json", // and this
      data: formData,
    })
      .done(function (msg) {
        console.log(msg);
        // location.reload();
        buildMappingTable();
        $("#mappingV4AddModal").modal("hide");
        $("#mappingV4AddModalForm").trigger("reset");
      })
      .fail(function (result) {
        console.log(result);
      });
  } else {
    alert("Please complete all the required fields!");
  }

  // location.reload();
});

$("#mappingV4ChangeModalBtn").click(function () {
  // var formArray = $("#mappingV4ChangeModalForm").serializeArray();
  var form = document.getElementById("mappingV4ChangeModalForm");
  var formData = new FormData(form);
  // console.log(formArray);
  if (
    $("#RESORT").val().length > 0 &&
    $("#FROM_ACCOUNT").val().length > 0 &&
    $("#TO_ACCOUNT").val().length > 0 &&
    $("#ACCOUNT_NAME").val().length > 0
  ) {
    console.log("Update mapping");
    // $.post( "https://marriottssc.secure-app.co.za/updateMappingV4",
    // $("#mappingV4ChangeModalForm").serializeArray() ,
    // function(data) {
    //   console.log(data);
    // }
    // )
    // .done(function( msg ) {
    //   console.log(msg);
    //   location.reload();
    // })
    // .fail(function(error) {
    //   alert("Mapping could not be added. Check if there is not already a mapping for this hotel and account number. If you are trying to amend an existing mapping records, double on the record in the table to open the edit window. ");
    //   console.log(error);
    // });
    $.ajax({
      type: "post",
      url: "https://marriottssc.secure-app.co.za/updateMappingV4",
      processData: false,
      contentType: false,
      dataType: "json", // and this
      data: formData,
    })
      .done(function (msg) {
        console.log(msg);
        location.reload();
      })
      .fail(function (result) {
        console.log(result);
      });
  } else {
    alert("Please complete all the required fields!");
  }

  //  location.reload();
});

$("#mappingV4DeleteModalBtn").click(function () {
  $("#deleteMappingV4Modal").modal("show");
});

$("#deleteMappingV4ConfirmBtn").click(function () {
  var formArray = $("#mappingV4ChangeModalForm").serializeArray();
  console.log(formArray);
  console.log("Delete mapping");
  $.post(
    "https://marriottssc.secure-app.co.za/deleteMapping",
    $("#mappingV4ChangeModalForm").serializeArray(),
    function (data) {
      console.log(data);
    }
  )
    .done(function (msg) {
      console.log(msg);
      location.reload();
    })
    .fail(function (error) {
      alert(
        "Comments could not be added. Please contact administrator" + error
      );
    });
});

//////////////////////////////////////////////////END OF Mapping/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////START of Owners/////////////////////////////////////////////////////////////////////
/**
 * Tab->Owners
 * Displays report of owners(EID) assigned to accounts
 * Table ID:ownersTable
 * API call to get data: https://marriottssc.secure-app.co.za/api/owners
 * Data from AR.AR_ACCOUNT_USER_MAP
 * Datatables library used to build tables
 *
 */

/**
 * If user does not have access to amend
 * owners, the button to add new owners
 * or save changes to the owners will be
 * disabled.
 */
if (document.querySelector("table[data-id-table=ownersTable]")) {
  console.log("ownersTable");

  if ($("#owners_access_allowed").text() == "N") {
    $("#OWNER_EID").attr("readonly", true);
    $("#OWNER_ACCOUNT_NAME").attr("readonly", true);
    $("#addOwnerBtn").attr("disabled", true);
    $("#ownerDeleteModalBtn").attr("disabled", true);
    $("#ownerChangeModalBtn").attr("disabled", true);
  }

  var $ownersTable = $("#ownersTable");
  console.log($ownersTable);
  var comments = "";

  /**
   * Ajax call to get owners data from
   * AR.AR_ACCOUNT_USER_MAP
   */
  $.get("https://marriottssc.secure-app.co.za/api/owners", function (data) {
    ownerData = data;
    console.log(data);
    // $ownersTable.bootstrapTable('destroy')
    // $testTable.bootstrapTable({data: ownerData})
    reportLoading.style.display = "none";

    $("#ownersTable").DataTable({
      dom: "Bfrtip",
      buttons: [
        {
          extend: "pdfHtml5",
          orientation: "landscape",
          pageSize: "LEGAL",
          extension: ".pdf",
          filename: "AR Account Owners",
          title: "AR Account Owners",
        },
        "copy",
        {
          extend: "csvHtml5",
          title: "AR Account Owners",
          filename: "AR Account Owners",
        },
        {
          extend: "excelHtml5",
          filename: "AR Account Owners",
          title: "AR Account Owners",
        },
        "print",
      ],
      paging: false,
      select: "single",
      scrollY: "550px",
      scrollCollapse: true,
      pageLength: 50,
      data: ownerData,
      columns: [
        { data: "ACCOUNT_NO", orderData: [1, 2] },
        { data: "ACCOUNT_NAME" },
        { data: "EID" },
        { data: "INSERT_DATE" },
        { data: "INSERT_USER" },
        { data: "UPDATE_DATE" },
        { data: "UPDATE_USER" },
      ],
    });

    $("#ownersTable_filter input").addClass("form-control");
    $("#ownersTable_filter input").attr("placeholder", "Search");
  })
    .done(function (msg) {
      reportLoading.style.display = "none";

      /**
       * When clicking on any cell inside the owners
       * table, filter the owners table by that
       * cell value when clicking on the yellow filter
       * button
       */

      $("#ownersTable tbody").on("click", "td", function (e) {
        var x = document.querySelectorAll(".filter");
        for (i = 0; i < x.length; i++) {
          console.log(x[i].classList.remove("filter"));
        }

        var cell = e.target.innerHTML;
        console.log(e.target.classList.add("filter"));
        cellValue = cell;
        console.log(cell);

        var visIdx = $(this).index();
        var dataIdx = $("#ownersTable")
          .DataTable()
          .column.index("fromVisible", visIdx);
        cellIndex = dataIdx;
        console.log("Column data index: " + dataIdx);
      });

      /**
       * when clicking on the yellow filter
       * button, filter the table by the
       * value of cell obtained in function above
       */
      $("#filterBtn").on("click", function (e) {
        console.log("Filter column: " + cellIndex + " by value: " + cellValue);
        $("#ownersTable")
          .DataTable()
          .search("")
          .columns(cellIndex)
          .search(cellValue)
          .draw();
      });

      $("#filterClearBtn").on("click", function (e) {
        clearOwnerSearch();
      });

      //Filter end
    })
    .fail(function (xhr, status, error) {
      reportLoading.style.display = "none";
    });
}

/**
 * delete all filters
 */
function clearOwnerSearch() {
  $("#ownersTable").DataTable().search("").columns().search("").draw();
}

/**
 * When double clicking on any
 * row in owners table, assign
 * values of the row to variables
 * and show the Change Account Owner modal.
 */
$("#ownersTable").on("dblclick", "tr", function (e) {
  $("#ownersTable").DataTable().row(this).select();
  var $element = $("#ownersTable").DataTable().row(this).data();
  ownerAccountNo = $element.ACCOUNT_NO;
  ownerAccountName = $element.ACCOUNT_NAME;
  ownerEID = $element.EID;
  $("#ownerChangeModal").modal("show");
});

/**
 * When the Change Account Owner
 * modal is made visible, assign
 * the values from the variables
 * above to the modal form fields
 */
$("#ownerChangeModal").on("show.bs.modal", function (event) {
  var modal = $(this);
  $("#OWNER_ACCOUNT").val(ownerAccountNo);
  $("#OWNER_EID").val(ownerEID);
  $("#OWNER_ACCOUNT_NAME").val(ownerAccountName);
});

/**
 * When clicking on Save changes in
 * the Change Account Owner modal,
 * do an Ajax call
 * https://marriottssc.secure-app.co.za/updateOwners
 * to update the AR_ACCOUNT_USER_MAP table
 */
$("#ownerChangeModalBtn").click(function () {
  var ownerArray = $("#ownerChangeModalForm").serializeArray();
  console.log(ownerArray);
  if (
    $("#OWNER_ACCOUNT").val().length > 0 &&
    $("#OWNER_EID").val().length > 0 &&
    $("#OWNER_ACCOUNT_NAME").val().length > 0
  ) {
    $.post(
      "https://marriottssc.secure-app.co.za/updateOwners",
      $("#ownerChangeModalForm").serializeArray(),
      function (data) {
        console.log(data);
      }
    )
      .done(function (msg) {
        console.log(msg);
        location.reload();
      })
      .fail(function (error) {
        alert("Owner could not be added. Please contact administrator" + error);
      });
  } else {
    alert("Please complete all the required fields!");
  }
});

/**
 * When clicking Delete in
 * Change Account Owners modal
 * another modal will prompt the user
 * to confirm if they are sure.
 */
$("#ownerDeleteModalBtn").click(function () {
  $("#deleteOwnersModal").modal("show");
});

/**
 * If Delete is clicked again , the record
 * for the owner and account in the modal form
 * will be deleted from AR_ACCOUNT_USER_MAP.
 */
$("#deleteOwnersConfirmBtn").click(function () {
  var ownerArray = $("#ownerChangeModalForm").serializeArray();
  console.log(ownerArray);
  console.log("Delete owners");
  $.post(
    "https://marriottssc.secure-app.co.za/deleteOwners",
    $("#ownerChangeModalForm").serializeArray(),
    function (data) {
      console.log(data);
    }
  )
    .done(function (msg) {
      console.log(msg);
      location.reload();
    })
    .fail(function (error) {
      alert("Owner could not be deleted. Please contact administrator" + error);
    });
});

/**
 * When clicking the Add New Owner button,
 * show the Add Account Owners modal.
 */
$("#addOwnerBtn").on("click", function () {
  console.log("add new record");
  $("#ownersAddModal").modal("show");
});

/**
 * When clicking Save changes in
 * Add Account Owners modal,
 * Ajax call:
 * https://marriottssc.secure-app.co.za/addOwners
 * will insert form data into AR_ACCOUNT_USER_MAP
 *
 */

$("#ownersAddModalBtn").click(function () {
  var ownerArray = $("#ownersAddModalForm").serializeArray();
  console.log(ownerArray);
  if (
    $("#OWNER_ACCOUNT_NEW").val().length > 0 &&
    $("#OWNER_EID_NEW").val().length > 0 &&
    $("#OWNER_ACCOUNT_NAME_NEW").val().length > 0
  ) {
    console.log("Add owner");
    $.post(
      "https://marriottssc.secure-app.co.za/addOwners",
      $("#ownersAddModalForm").serializeArray(),
      function (data) {
        console.log(data);
      }
    )
      .done(function (msg) {
        console.log(msg);
        location.reload();
      })
      .fail(function (error) {
        alert(
          "Owners could not be added. Please contact administrator" + error
        );
      });
  } else {
    alert("Please complete all the required fields!");
  }
});

//////////////////////////////////////////////////END OF Owners/////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//PERFORMANCE DASHBOARD START//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

testArray = [];
setDate();
setDates();

///////////////////////////////////////////////START HELPER FUNCTIONS//////////////////////////////////////////////////////////////////////////////

function removeApos(string) {
  return string.replace("'", "");
}
function renameColumns(string) {
  var ret_str = "";
  switch (string) {
    case "AGE_BUCKET1":
      ret_str = "Up to 15";
      break;
    case "AGE_BUCKET2":
      ret_str = "16 to 30";
      break;
    case "AGE_BUCKET3":
      ret_str = "31 to 60";
      break;
    case "AGE_BUCKET4":
      ret_str = "61 to 90";
      break;
    case "AGE_BUCKET5":
      ret_str = "91 to 120";
      break;
    case "AGE_BUCKET6":
      ret_str = "120 and over";
      break;
    case "TOTAL":
      ret_str = "Total";
      break;
    case "CREDIT_LIMIT":
      ret_str = "Credit Limit";
      break;
    case "ACCOUNT_NAME":
      ret_str = "Account";
      break;

    default:
      break;
  }
  return ret_str;
}

function pivotData(data, filter, datatype) {
  console.log(data);
  if (datatype == "DIM") {
    var user_data = data.filter(function (rows) {
      return rows.DIM == filter;
    });
  } else {
    var user_data = data.filter(function (rows) {
      return rows.RESORT == filter;
    });
  }

  var masterArray = [];
  var AGE_BUCKET1_array = [];
  AGE_BUCKET1_array["DIM"] = user_data[0].DIM;
  AGE_BUCKET1_array["LABEL"] = "0 - 30 Days";
  AGE_BUCKET1_array["PERC"] = user_data[0].AGE_BUCKET1_PERC;
  masterArray.push(AGE_BUCKET1_array);
  var AGE_BUCKET2_array = [];
  AGE_BUCKET2_array["DIM"] = user_data[0].DIM;
  AGE_BUCKET2_array["LABEL"] = "31 - 60 Days";
  AGE_BUCKET2_array["PERC"] = user_data[0].AGE_BUCKET2_PERC;
  masterArray.push(AGE_BUCKET2_array);
  var AGE_BUCKET3_array = [];
  AGE_BUCKET3_array["DIM"] = user_data[0].DIM;
  AGE_BUCKET3_array["LABEL"] = "61 - 90 Days";
  AGE_BUCKET3_array["PERC"] = user_data[0].AGE_BUCKET3_PERC;
  masterArray.push(AGE_BUCKET3_array);
  var AGE_BUCKET456_array = [];
  AGE_BUCKET456_array["DIM"] = user_data[0].DIM;
  AGE_BUCKET456_array["LABEL"] = "91 - Older";
  AGE_BUCKET456_array["PERC"] = user_data[0].AGE_BUCKET456_PERC;
  masterArray.push(AGE_BUCKET456_array);

  return masterArray;
}

$.fn.dataTable.render.percentage = function () {
  return function (data, type, row) {
    // console.log(data + "%");
    return data + "%";
  };
};

///////////////////////////////////////////////START HELPER FUNCTIONS//////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////START OF DATE SHARED FILTERS /////////////////////////////////////////////////////////////

function setDates() {
  var date = new Date();
  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
  var dd = String(firstDay.getDate()).padStart(2, "0");
  var mm = String(firstDay.getMonth() + 1).padStart(2, "0");
  var yyyy = firstDay.getFullYear();
  dateStr = yyyy + "-" + mm + "-" + dd;
  $("#filter_from_date").val(dateStr);

  var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  var dd = String(lastDay.getDate()).padStart(2, "0");
  var mm = String(lastDay.getMonth() + 1).padStart(2, "0");
  var yyyy = lastDay.getFullYear();
  dateStr = yyyy + "-" + mm + "-" + dd;
  $("#filter_to_date").val(dateStr);

  var today = new Date();
  var dd = String(today.getDate()).padStart(2, "0");
  var mm = String(today.getMonth() + 1).padStart(2, "0");
  var yyyy = today.getFullYear();
  dateStr = yyyy + "-" + mm + "-" + dd;
  $("#filter_from_date").attr("max", dateStr);
  $("#filter_to_date").attr("max", dateStr);
}

function setDate() {
  var date = new Date();
  var dd = String(date.getDate()).padStart(2, "0");
  var mm = String(date.getMonth() + 1).padStart(2, "0");
  var yyyy = date.getFullYear();
  dateStr = yyyy + "-" + mm + "-" + dd;
  $("#filter_date").val(dateStr);
  $("#filter_date").attr("max", dateStr);
}

$("#filter_from_date").change(function () {
  // reportLoading.style.display = 'block';
  // console.log($(this).val());
  if (document.querySelector("table[data-id-table=eid_perf_table]")) {
    updateEIDPerformance(g1, g2);
  }
  if (document.querySelector("table[data-id-table=eid_aging_table]")) {
    updateEIDAging();
  }
  if (document.querySelector("table[data-id-table=eid_aging_perc_table]")) {
    updateEIDAgingPerc();
  }
  if (document.querySelector("table[data-id-table=ssc_aging_table]")) {
    updateSSCAging();
  }
  if (document.querySelector("table[data-id-table=resort_aging_table]")) {
    updateResortAging();
  }
  if (document.querySelector("table[data-id-table=resort_aging_perc_table]")) {
    updateResortPercAging();
  }
});

$("#filter_to_date").change(function () {
  // reportLoading.style.display = 'block';
  // console.log($(this).val());
  if (document.querySelector("table[data-id-table=eid_perf_table]")) {
    updateEIDPerformance(g1, g2);
  }
  if (document.querySelector("table[data-id-table=eid_aging_table]")) {
    updateEIDAging();
  }
  if (document.querySelector("table[data-id-table=eid_aging_perc_table]")) {
    updateEIDAgingPerc();
  }
  if (document.querySelector("table[data-id-table=ssc_aging_table]")) {
    updateSSCAging();
  }
  if (document.querySelector("table[data-id-table=resort_aging_table]")) {
    updateResortAging();
  }
  if (document.querySelector("table[data-id-table=resort_aging_perc_table]")) {
    updateResortPercAging();
  }
});

$("#filter_resort").change(function () {
  console.log("filter resort changed");
  reportLoading.style.display = "block";
  // console.log("filter_resort changed")
  // console.log($(this).val());
  if (document.querySelector("table[data-id-table=ssc_aging_table]")) {
    updateSSCAging();
  }
  /**
   * Aging Template
   * When the Hotel filter is changed,
   * the snapshot dates for that hotel
   * will be obtained and the Period
   * LOV will be populated with this date.
   */
  if (document.querySelector("table[data-id-table=aging_template]")) {
    $("#aging_template").DataTable().clear().draw();
    console.log("get snapshots");
    getSnapshotDates();

    // updateAgingTemplate();
  }
});

$("#filter_eid").change(function () {
  reportLoading.style.display = "block";
  // console.log("filter_eid changed")
  // console.log($(this).val());
  if (document.querySelector("table[data-id-table=ssc_aging_table]")) {
    updateSSCAging();
  }
});

////////////////////////////////////////////////////////START OF DATE SHARED FILTERS/////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////START OF INDIVIDUAL PERFORMANCE///////////////////////////////////////////////////////////

$("#a").change(function () {
  updateEIDPerformance(g1, g2);
});
$("#b").change(function () {
  updateEIDPerformance(g1, g2);
});
$("#c").change(function () {
  updateEIDPerformance(g1, g2);
});

if (document.querySelector("table[data-id-table=eid_perf_table]")) {
  var margin = { left: 100, right: 10, top: 10, bottom: 100 };
  var width = 600 - margin.left - margin.right;
  var height = 400 - margin.top - margin.bottom;
  var barPadding = 0.2;
  var axisTicks = { qty: 5, outerSize: 0, dateFormat: "%m-%d" };
  var t = d3.transition().duration(1000);

  var tooltip = d3
    .select("body")
    .append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

  /*Graph 1 */
  var g1 = d3
    .select("#eid_perf_chart1")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
  /*Graph 1 */

  /*Graph 2 */
  var g2 = d3
    .select("#eid_perf_chart2")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
  /*Graph 2 */

  var xScale0 = d3.scaleBand().range([0, width]).padding(barPadding);
  /*Graph 1 */
  var xScale1 = d3.scaleBand();
  var yScale = d3.scaleLinear().range([height, 0]);
  /*Graph 1 */

  /*Graph 2 */
  var xScale1_g2 = d3.scaleBand();
  var yScale_g2 = d3.scaleLinear().range([height, 0]);
  /*Graph 2 */

  /*Graph 1 */
  // Add the X Axis
  var xAxisGroupG1 = g1
    .append("g")
    .attr("class", "x g1-axis")
    .attr("transform", `translate(0,${height})`);

  // Add the Y Axis
  var yAxisGroupG1 = g1.append("g").attr("class", "y g1-axis");
  /*Graph 1 */

  /*Graph 2 */
  // Add the X Axis
  var xAxisGroupG2 = g2
    .append("g")
    .attr("class", "x g2-axis")
    .attr("transform", `translate(0,${height})`);

  // Add the Y Axis
  var yAxisGroupG2 = g2.append("g").attr("class", "y g2-axis");
  /*Graph 2 */

  console.log("eid_perf_table");
  var $eid_perf_table = $("#eid_perf_table");
  // console.log($eid_perf_table);
  updateEIDPerformance(g1, g2);
}

function updateEIDPerformance(g1, g2) {
  $a = $("#a option:selected").val();
  $b = $("#b option:selected").val();
  $c = $("#c option:selected").val();

  if ($c != "c_null") {
    g1_x_axis_text =
      $("#a option:selected").text() +
      " vs " +
      $("#b option:selected").text() +
      " vs " +
      $("#c option:selected").text();
  } else {
    g1_x_axis_text =
      $("#a option:selected").text() + " vs " + $("#b option:selected").text();
  }

  /*Graph 1 */
  //remove existing labels if any
  $(".x.g1-axis-label").remove();
  $(".y.g1-axis-label").remove();
  //X label
  g1.append("text")
    .attr("class", "x g1-axis-label")
    .attr("x", width / 2)
    .attr("y", height + 80)
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .text(g1_x_axis_text);

  //Y label
  g1.append("text")
    .attr("class", "y g1-axis-label")
    .attr("x", -(height / 2))
    .attr("y", -60)
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .attr("transform", "rotate(-90)")
    .text("No of Invoices");
  /*Graph 1 */

  if ($c != "c_null") {
    g2_x_axis_text =
      $("#a option:selected").text() +
      " vs " +
      $("#c option:selected").text() +
      " % & " +
      $("#b option:selected").text() +
      " vs " +
      $("#c option:selected").text() +
      " % ";
  } else {
    g2_x_axis_text =
      $("#a option:selected").text() +
      " vs " +
      $("#b option:selected").text() +
      " %";
  }

  /*Graph 2 */
  //remove existing labels if any
  $(".x.g2-axis-label").remove();
  $(".y.g2-axis-label").remove();
  //X label
  g2.append("text")
    .attr("class", "x g2-axis-label")
    .attr("x", width / 2)
    .attr("y", height + 80)
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .text(g2_x_axis_text);

  //Y label
  g2.append("text")
    .attr("class", "y g2-axis-label")
    .attr("x", -(height / 2))
    .attr("y", -60)
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .attr("transform", "rotate(-90)")
    .text("%");
  /*Graph 2 */

  /*Graph 1 Legend */
  var elementColor = d3.scaleOrdinal();
  if ($c != "c_null") {
    elementColor
      .domain([
        $("#a option:selected").text(),
        $("#b option:selected").text(),
        $("#c option:selected").text(),
      ])
      .range([
        "rgb(154, 154, 249)",
        "rgb(245, 199, 116)",
        "rgb(245, 166, 166)",
      ]);
    var keys = [
      $("#a option:selected").text(),
      $("#b option:selected").text(),
      $("#c option:selected").text(),
    ];
  } else {
    elementColor
      .domain([$("#a option:selected").text(), $("#b option:selected").text()])
      .range(["rgb(154, 154, 249)", "rgb(245, 199, 116)"]);
    var keys = [$("#a option:selected").text(), $("#b option:selected").text()];
  }

  var legend = g1
    .append("g")
    .attr("transform", "translate(-60" + "," + (height + 25) + ")");

  // Add one dot in the legend for each name.
  $(".g1LegendDots").remove();
  $(".g1LegendText").remove();
  var size = 10;
  legend
    .selectAll("mydots")
    .data(keys)
    .enter()
    .append("rect")
    .attr("class", "g1LegendDots")
    .attr("x", 0)
    .attr("y", function (d, i) {
      return 10 + i * (size + 5);
    })
    .attr("width", size)
    .attr("height", size)
    .style("fill", function (d) {
      return elementColor(d);
    });

  legend
    .selectAll("legend")
    .data(keys)
    .enter()
    .append("text")
    .attr("class", "g1LegendText")
    .attr("x", 0 + size * 1.2)
    .attr("y", function (d, i) {
      return 10 + i * (size + 5) + size / 2;
    })
    .style("fill", function (d) {
      return elementColor(d);
    })
    .text(function (d) {
      return d;
    })
    .attr("text-anchor", "left")
    .style("alignment-baseline", "middle");
  /*Graph 1 Legend */

  /*Graph 2 Legend */
  var g2_elementColor = d3.scaleOrdinal();
  if ($c != "c_null") {
    g2_elementColor
      .domain([
        "% of " +
          $("#a option:selected").text() +
          " vs " +
          $("#c option:selected").text(),
        "% of " +
          $("#b option:selected").text() +
          " vs " +
          $("#c option:selected").text(),
      ])
      .range(["rgb(154, 154, 249)", "rgb(188, 188, 193)"]);
    var keys = [
      "% of " +
        $("#a option:selected").text() +
        " vs " +
        $("#c option:selected").text(),
      "% of " +
        $("#b option:selected").text() +
        " vs " +
        $("#c option:selected").text(),
    ];
  } else {
    g2_elementColor
      .domain([
        "% of " +
          $("#a option:selected").text() +
          " vs " +
          $("#b option:selected").text(),
      ])
      .range(["rgb(154, 154, 249)"]);
    var keys = [
      "% of " +
        $("#a option:selected").text() +
        " vs " +
        $("#b option:selected").text(),
    ];
  }

  var legend = g2
    .append("g")
    .attr("transform", "translate(-60" + "," + (height + 25) + ")");

  // Add one dot in the legend for each name.
  $(".g2LegendDots").remove();
  $(".g2LegendText").remove();
  var size = 10;
  legend
    .selectAll("mydots")
    .data(keys)
    .enter()
    .append("rect")
    .attr("class", "g2LegendDots")
    .attr("x", 0)
    .attr("y", function (d, i) {
      return 10 + i * (size + 5);
    })
    .attr("width", size)
    .attr("height", size)
    .style("fill", function (d) {
      return g2_elementColor(d);
    });

  legend
    .selectAll("legend")
    .data(keys)
    .enter()
    .append("text")
    .attr("class", "g2LegendText")
    .attr("x", 0 + size * 1.2)
    .attr("y", function (d, i) {
      return 10 + i * (size + 5) + size / 2;
    })
    .style("fill", function (d) {
      return g2_elementColor(d);
    })
    .text(function (d) {
      return d;
    })
    .attr("text-anchor", "left")
    .style("alignment-baseline", "middle");
  /*Graph 2 Legend */

  filter = {
    filter_from_date: $("#filter_from_date").val(),
    filter_to_date: $("#filter_to_date").val(),
    csrf_name: $("#getTokenName").val(),
    csrf_value: $("#getTokenValue").val(),
    a: $("#a option:selected").val(),
    b: $("#b option:selected").val(),
    c: $("#c option:selected").val(),
  };
  // console.log(g1);

  $("#eid_perf_table").DataTable().destroy();
  $("#eid_perf_table tbody").empty();

  if ($c != "c_null") {
    if ($("#fact3").length == 0) {
      $("#fact2").after(
        "<th  id='fact3' scope='col' data-sortable='true' data-field='FACT3'>Metric C</th>"
      );
      $("#percentage").after(
        "<th  id='percentage2' scope='col' data-sortable='true' data-field='PERCENTAGE2'>%</th>"
      );
    }

    columns = [
      { data: "DIM" },
      { data: "FACT1" },
      { data: "FACT2" },
      { data: "FACT3" },
      { data: "PERCENTAGE", render: $.fn.dataTable.render.percentage() },
      { data: "PERCENTAGE2", render: $.fn.dataTable.render.percentage() },
    ];
  } else {
    if ($("#fact3").length > 0) {
      $("#fact3").remove();
      $("#percentage2").remove();
    }
    columns = [
      { data: "DIM" },
      { data: "FACT1" },
      { data: "FACT2" },
      { data: "PERCENTAGE", render: $.fn.dataTable.render.percentage() },
    ];
  }

  $.post(
    "https://marriottssc.secure-app.co.za/api/eid_perf",
    filter,
    function (data) {
      console.log(data);
    }
  )
    .done(function (data) {
      // console.log(data);
      $("#eid_perf_table").DataTable({
        dom: "Bfrtip",
        buttons: [
          {
            extend: "pdfHtml5",
            orientation: "landscape",
            pageSize: "LEGAL",
            extension: ".pdf",
            filename: "AR Account Owners",
            title: "AR Account Owners",
          },
          "copy",
          {
            extend: "csvHtml5",
            title: "AR Account Owners",
            filename: "AR Account Owners",
          },
          {
            extend: "excelHtml5",
            filename: "AR Account Owners",
            title: "AR Account Owners",
          },
          "print",
        ],
        paging: false,
        select: "single",
        searching: false,
        scrollCollapse: true,
        data: data,
        columns: columns,
      });

      if ($c != "c_null") {
        $("#percentage").text(
          $("#a option:selected").text() +
            " vs " +
            $("#c option:selected").text() +
            " %"
        );
        $("#percentage2").text(
          $("#b option:selected").text() +
            " vs " +
            $("#c option:selected").text() +
            " %"
        );
      } else {
        $("#percentage").text(
          $("#a option:selected").text() +
            " vs " +
            $("#b option:selected").text() +
            " %"
        );
      }
      $("#fact1").text($("#a option:selected").text());
      $("#fact2").text($("#b option:selected").text());
      $("#fact3").text($("#c option:selected").text());

      $("#eid_perf_table_filter input").addClass("form-control");
      $("#eid_perf_table_filter input").attr("placeholder", "Search");

      data.forEach((d) => {
        d.FACT1 = +d.FACT1;
        d.FACT2 = +d.FACT2;
        d.FACT3 = +d.FACT3;
        d.PERCENTAGE = +d.PERCENTAGE;
        d.PERCENTAGE2 = +d.PERCENTAGE2;
      });

      var dim = data.map(function (d) {
        return d.DIM;
      });

      //graph data
      data.sort(function (a, b) {
        return d3.ascending(a.DIM, b.DIM);
      });
      xScale0.domain(data.map((d) => d.DIM));

      /*Graph 1 */
      if ($c != "c_null") {
        xScale1
          .domain(["FACT1", "FACT2", "FACT3"])
          .range([0, xScale0.bandwidth()]);
        yScale.domain([
          0,
          d3.max(data, (d) => Math.max(d.FACT1, d.FACT2, d.FACT3)),
        ]);
      } else {
        xScale1.domain(["FACT1", "FACT2"]).range([0, xScale0.bandwidth()]);
        yScale.domain([0, d3.max(data, (d) => Math.max(d.FACT1, d.FACT2))]);
      }

      /*Graph 1 */

      /*Graph 2 */
      if ($c != "c_null") {
        xScale1_g2
          .domain(["PERCENTAGE", "PERCENTAGE2"])
          .range([0, xScale0.bandwidth()]);
        yScale_g2.domain([
          0,
          d3.max(data, (d) => Math.max(d.PERCENTAGE, d.PERCENTAGE2)),
        ]);
      } else {
        xScale1_g2.domain(["PERCENTAGE"]).range([0, xScale0.bandwidth()]);
        yScale_g2.domain([0, d3.max(data, (d) => d.PERCENTAGE)]);
      }

      /*Graph 2 */

      /*Graph 1 */
      var xAxisG1 = d3.axisBottom(xScale0).tickSizeOuter(axisTicks.outerSize);
      var yAxisG1 = d3
        .axisLeft(yScale)
        .ticks(axisTicks.qty)
        .tickSizeOuter(axisTicks.outerSize);

      /*Graph 1 */

      /*Graph 2 */
      var xAxisG2 = d3.axisBottom(xScale0).tickSizeOuter(axisTicks.outerSize);
      var yAxisG2 = d3
        .axisLeft(yScale_g2)
        .ticks(axisTicks.qty)
        .tickSizeOuter(axisTicks.outerSize);

      /*Graph 2 */

      updateChart(data, xAxisG1, yAxisG1, xAxisG2, yAxisG2);
      reportLoading.style.display = "none";
    })
    .fail(function (error, result) {
      console.log(error);
      console.log(result);
    })
    .always(function (data, status, result) {
      console.log(data);
      console.log(result);
    });
}

function updateChart(data, xAxisG1, yAxisG1, xAxisG2, yAxisG2) {
  xAxisGroupG1.transition(t).call(xAxisG1);
  yAxisGroupG1.transition(t).call(yAxisG1);
  xAxisGroupG2.transition(t).call(xAxisG2);
  yAxisGroupG2.transition(t).call(yAxisG2);

  //JOIN NEW DATA WITH OLD ELEMENTS
  /*Graph 1 */
  var DIM1 = g1.selectAll(".DIM1").data(data, function (d) {
    return d.DIM;
  });
  console.log(DIM1);

  //EXIT old elements not present in new data
  DIM1.exit().remove();

  //UPDATE old elements present in new data
  var rectGroupsExisting = DIM1.attr(
    "transform",
    (d) => `translate(${xScale0(d.DIM)},0)`
  );

  //ENTER new elements
  var rectGroupsEnter = DIM1.enter()
    .append("g")
    .attr("class", "DIM1")
    .attr("transform", (d) => `translate(${xScale0(d.DIM)},0)`);

  /* Add FACT 1 bars */

  var fact1Rect = rectGroupsEnter.selectAll(".FACT1").data((d) => [d]);

  var fact1RectExisting = rectGroupsExisting
    .selectAll(".FACT1")
    .data((d) => [d]);

  //Remove elements no longer in data
  fact1RectExisting
    .exit()
    .attr("fill", "red")
    // .transition(t)
    .attr("y", yScale(0))
    .attr("height", 0)
    .remove();

  fact1RectExisting
    .transition(t)
    .attr("x", (d) => xScale1("FACT1"))
    .attr("y", (d) => yScale(d.FACT1))
    .attr("width", xScale1.bandwidth())
    .attr("height", (d) => {
      return height - yScale(d.FACT1);
    });
  // console.log(fact1Rect);

  fact1Rect
    .enter()
    .append("rect")
    .attr("class", "FACT1")
    .style("fill", "rgb(154, 154, 249)")
    .attr("x", (d) => xScale1("FACT1"))
    .attr("width", xScale1.bandwidth())
    .attr("y", yScale(0))
    .attr("height", 0)
    // .transition(t)
    .attr("y", (d) => yScale(d.FACT1))
    .attr("height", (d) => {
      return height - yScale(d.FACT1);
    })
    .on("mouseover", function (d) {
      tooltip
        .transition()
        .duration(200)
        .style("background", "rgb(154, 154, 249)")
        .style("color", "rgb(12, 12, 214)")
        .style("opacity", 0.7);
      tooltip
        .html(d.DIM + "<br/>Actioned<br/>" + d.FACT1)
        .style("left", d3.event.pageX + 10 + "px")
        .style("top", d3.event.pageY - 50 + "px");
    })
    .on("mouseout", function (d) {
      tooltip.transition().duration(500).style("opacity", 0);
    });
  // console.log(fact1Rect);

  /* Add fact 2 bars */
  var fact2Rect = rectGroupsEnter.selectAll(".FACT2").data((d) => [d]);
  console.log(fact2Rect);
  var fact2RectExisting = rectGroupsExisting
    .selectAll(".FACT2")
    .data((d) => [d]);

  fact2RectExisting
    .exit()
    .attr("fill", "red")
    .transition(t)
    .attr("y", yScale(0))
    .attr("height", 0)
    .remove();

  fact2RectExisting
    .transition(t)
    .attr("x", (d) => xScale1("FACT2"))
    .attr("y", (d) => yScale(d.FACT2))
    .attr("width", xScale1.bandwidth())
    .attr("height", (d) => {
      return height - yScale(d.FACT2);
    });

  fact2Rect
    .enter()
    .append("rect")
    .attr("class", "FACT2")
    .style("fill", "rgb(245, 199, 116)")
    .attr("x", (d) => xScale1("FACT2"))
    .attr("width", xScale1.bandwidth())
    .attr("y", yScale(0))
    .attr("height", 0)
    // .transition(t)
    .attr("y", (d) => yScale(d.FACT2))
    .attr("height", (d) => {
      return height - yScale(d.FACT2);
    })
    .on("mouseover", function (d) {
      tooltip
        .transition()
        .duration(200)
        .style("background", "rgb(245, 199, 116)")
        .style("color", "rgb(255, 87, 34)")
        .style("opacity", 0.7);
      tooltip
        .html(d.DIM + "<br/>Invoices<br/>" + d.FACT2)
        .style("left", d3.event.pageX + 10 + "px")
        .style("top", d3.event.pageY - 50 + "px");
    })
    .on("mouseout", function (d) {
      tooltip.transition().duration(500).style("opacity", 0);
    });

  /* Add fact 3 bars */

  if ($c != "c_null") {
    var fact3Rect = rectGroupsEnter.selectAll(".FACT3").data((d) => [d]);
    console.log(fact3Rect);
    var fact3RectExisting = rectGroupsExisting
      .selectAll(".FACT3")
      .data((d) => [d]);
    console.log(fact3RectExisting);

    fact3RectExisting
      .exit()
      .attr("fill", "red")
      .transition(t)
      .attr("y", yScale(0))
      .attr("height", 0)
      .remove();

    fact3RectExisting
      .transition(t)
      .attr("x", (d) => xScale1("FACT3"))
      .attr("y", (d) => yScale(d.FACT3))
      .attr("width", xScale1.bandwidth())
      .attr("height", (d) => {
        return height - yScale(d.FACT3);
      });

    fact3RectExisting
      .enter()
      .append("rect")
      .attr("class", "FACT3")
      .style("fill", "rgb(245, 166, 166)")
      .attr("x", (d) => xScale1("FACT3"))
      .attr("width", xScale1.bandwidth())
      .attr("y", yScale(0))
      .attr("height", 0)
      // .transition(t)
      .attr("y", (d) => yScale(d.FACT3))
      .attr("height", (d) => {
        return height - yScale(d.FACT3);
      })
      .on("mouseover", function (d) {
        tooltip
          .transition()
          .duration(200)
          .style("background", "rgb(245, 166, 166)")
          .style("color", "rgb(239, 14, 14)")
          .style("opacity", 0.7);
        tooltip
          .html(d.DIM + "<br/>Invoices<br/>" + d.FACT3)
          .style("left", d3.event.pageX + 10 + "px")
          .style("top", d3.event.pageY - 50 + "px");
      })
      .on("mouseout", function (d) {
        tooltip.transition().duration(500).style("opacity", 0);
      });
  } else {
    $(".FACT3").remove();
  }

  /*Graph 1 */

  /*Graph 2 */

  var DIM2 = g2.selectAll(".DIM2").data(data, function (d) {
    return d.DIM;
  });

  //EXIT old elements not present in new data
  DIM2.exit().remove();

  //UPDATE old elements present in new data
  var rectGroupsExisting2 = DIM2.attr(
    "transform",
    (d) => `translate(${xScale0(d.DIM)},0)`
  );

  //ENTER new elements
  var rectGroupsEnter2 = DIM2.enter()
    .append("g")
    .attr("class", "DIM2")
    .attr("transform", (d) => `translate(${xScale0(d.DIM)},0)`);

  /* Add Percentage count bars */
  var PercentageRect = rectGroupsEnter2
    .selectAll(".bar.PERCENTAGE")
    .data((d) => [d]);
  var PercentageRectExisting = rectGroupsExisting2
    .selectAll(".bar.PERCENTAGE")
    .data((d) => [d]);

  PercentageRectExisting.exit()
    .attr("fill", "red")
    .transition(t)
    .attr("y", yScale_g2(0))
    .attr("height", 0)
    .remove();

  PercentageRectExisting.transition(t)
    .attr("x", (d) => xScale1_g2("PERCENTAGE"))
    .attr("y", (d) => yScale_g2(d.PERCENTAGE))
    .attr("width", xScale1_g2.bandwidth())
    .attr("height", (d) => {
      return height - yScale_g2(d.PERCENTAGE);
    });

  PercentageRect.enter()
    .append("rect")
    .attr("class", "bar PERCENTAGE")
    .style("fill", "rgb(154, 154, 249)")
    .attr("x", (d) => xScale1_g2("PERCENTAGE"))
    .attr("width", xScale1_g2.bandwidth())
    .attr("y", yScale_g2(0))
    .attr("height", 0)
    // .transition(t)
    .attr("y", (d) => yScale_g2(d.PERCENTAGE))
    .attr("height", (d) => {
      return height - yScale_g2(d.PERCENTAGE);
    })
    .on("mouseover", function (d) {
      tooltip
        .transition()
        .duration(200)
        .style("background", "rgb(154, 154, 249)")
        .style("color", "rgb(13, 152, 8)")
        .style("opacity", 0.7);
      tooltip
        .html(d.DIM + "<br/>Actioned<br/>" + d.PERCENTAGE + "%")
        .style("left", d3.event.pageX + 10 + "px")
        .style("top", d3.event.pageY - 50 + "px");
    })
    .on("mouseout", function (d) {
      tooltip.transition().duration(500).style("opacity", 0);
    });

  if ($c != "c_null") {
    /* Add Percentage count bars */
    var PercentageRect2 = rectGroupsEnter2
      .selectAll(".bar.PERCENTAGE2")
      .data((d) => [d]);
    var PercentageRect2Existing = rectGroupsExisting2
      .selectAll(".bar.PERCENTAGE2")
      .data((d) => [d]);

    PercentageRect2Existing.exit()
      .attr("fill", "red")
      .transition(t)
      .attr("y", yScale_g2(0))
      .attr("height", 0)
      .remove();

    PercentageRect2Existing.transition(t)
      .attr("x", (d) => xScale1_g2("PERCENTAGE2"))
      .attr("y", (d) => yScale_g2(d.PERCENTAGE2))
      .attr("width", xScale1_g2.bandwidth())
      .attr("height", (d) => {
        return height - yScale_g2(d.PERCENTAGE2);
      });

    PercentageRect2Existing.enter()
      .append("rect")
      .attr("class", "bar PERCENTAGE2")
      .style("fill", "rgb(188, 188, 193)")
      .attr("x", (d) => xScale1_g2("PERCENTAGE2"))
      .attr("width", xScale1_g2.bandwidth())
      .attr("y", yScale_g2(0))
      .attr("height", 0)
      // .transition(t)
      .attr("y", (d) => yScale_g2(d.PERCENTAGE2))
      .attr("height", (d) => {
        return height - yScale_g2(d.PERCENTAGE2);
      })
      .on("mouseover", function (d) {
        tooltip
          .transition()
          .duration(200)
          .style("background", "rgb(188, 188, 193)")
          .style("color", "rgb(13, 152, 8)")
          .style("opacity", 0.7);
        tooltip
          .html(d.DIM + "<br/>Actioned<br/>" + d.PERCENTAGE2 + "%")
          .style("left", d3.event.pageX + 10 + "px")
          .style("top", d3.event.pageY - 50 + "px");
      })
      .on("mouseout", function (d) {
        tooltip.transition().duration(500).style("opacity", 0);
      });
  } else {
    $(".PERCENTAGE2").remove();
  }

  /*Graph 2 */
}

/////////////////////////////////////////////////////////////END OF INDIVIDUAL PERFORMANCE/////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////START OF INDIVIDUAL/HOTEL AGING/////////////////////////////////////////////////////

$("#editTargetsBtn").click(function () {
  $("#performanceInputEditModal").modal("show");
});
$("#editTargetsModalBtn").click(function () {
  $("#performanceInputEditModal").modal("show");
});

$("#dim_aging").change(function () {
  updateEIDAging();
  updateEIDAgingPerc();
});

$("#target_eid").change(function () {
  updateEIDTargets();
});

$("#target_type").change(function () {
  console.log("target changes");
  console.log($("#target_type").val());

  $(".user-config").remove();
  if (
    $("#target_type").val() == "INDIVIDUAL_AGING" ||
    $("#target_type").val() == "MARRIOTT_AGING_POLICY"
  ) {
    $("#input_targets").append(
      '<div class="col-sm-4 form-group user-config">\
        <div class="form-group">\
        <label for="DIM1" class="col-form-label">0 - 30 Days </label>\
        <input type="number" class="form-control"  id="DIM1" name="DIM1"  value="" min="0" max="100"  >\
        </div>\
    </div>  \
    <div class="col-sm-4 form-group user-config">\
        <div class="form-group">\
        <label for="DIM2" class="col-form-label">31 - 60 Days </label>\
        <input type="number" class="form-control"  id="DIM2" name="DIM2"   value="" min="0" max="100"   >\
        </div>\
    </div> \
    <div class="col-sm-4 form-group user-config">\
        <div class="form-group">\
        <label for="DIM3" class="col-form-label">61 - 90 Days </label>\
        <input type="number" class="form-control"  id="DIM3"  name="DIM3" value="" min="0" max="100"   >\
        </div>\
    </div>\
    <div class="col-sm-4 form-group user-config">\
        <div class="form-group">\
        <label for="DIM4" class="col-form-label">91 - Older</label>\
        <input type="number" class="form-control"  id="DIM4" name="DIM4"  value=""  min="0" max="100"  >\
        </div>\
    </div> '
    );
  }
  updateEIDTargets();
});

function DIM1(target) {
  return target.TARGET_CODE === "DIM1";
}
function DIM2(target) {
  return target.TARGET_CODE === "DIM2";
}
function DIM3(target) {
  return target.TARGET_CODE === "DIM3";
}
function DIM4(target) {
  return target.TARGET_CODE === "DIM4";
}

/**
 * Individual/Hotel Aging
 * When clicking on Save Changes in Edit Targets modal
 *
 */
$("#saveTargetsBtn").click(function () {
  if (
    $("#target_eid").val() != "ALL" &&
    $("#DIM4").val() >= 0 &&
    $("#DIM4").val() <= 100 &&
    $("#DIM3").val() >= 0 &&
    $("#DIM3").val() <= 100 &&
    $("#DIM2").val() >= 0 &&
    $("#DIM2").val() <= 100 &&
    $("#DIM1").val() >= 0 &&
    $("#DIM1").val() <= 100
  ) {
    $("#target_flash").css({ display: "none" });
    console.log($("#targetsForm").serializeArray());
    $.post(
      "https://marriottssc.secure-app.co.za/api/updateTargets/" +
        $("#target_eid").val() +
        "/" +
        $("#target_type").val(),
      $("#targetsForm").serializeArray(),
      function (data) {
        console.log(data);
      }
    )
      .done(function (msg) {
        console.log(msg);
        updateEIDAgingPerc();
        $("#performanceInputEditModal").modal("hide");
      })
      .fail(function (error) {})
      .always(function (data, status, xhr) {
        console.log(data);
        console.log(status);
        console.log(xhr);
      });
  } else {
    $("#target_flash").css({ display: "block" });
  }
});

function updateEIDTargets() {
  $.get(
    "https://marriottssc.secure-app.co.za/api/eid_targets/" +
      $("#target_eid").val() +
      "/" +
      $("#target_type").val(),
    function (data) {}
  )
    .done(function (data) {
      if (data.length > 0) {
        console.log(data.filter(DIM1)[0].TARGET_VALUE);
        $("#DIM1").val(data.filter(DIM1)[0].TARGET_VALUE);
        $("#DIM2").val(data.filter(DIM2)[0].TARGET_VALUE);
        $("#DIM3").val(data.filter(DIM3)[0].TARGET_VALUE);
        $("#DIM4").val(data.filter(DIM4)[0].TARGET_VALUE);
      } else {
        $("#DIM1").val("");
        $("#DIM2").val("");
        $("#DIM3").val("");
        $("#DIM4").val("");
      }
    })
    .fail(function (data) {})
    .always(function (data) {
      console.log(data);
    });
}

if (document.querySelector("table[data-id-table=eid_aging_table]")) {
  // console.log("eid_aging_table");
  var $eid_aging_table = $("#eid_aging_table");
  // console.log($eid_aging_table);
  updateEIDAging();
}

//Update the EID Aging table and chart
function updateEIDAging() {
  $("#eid_aging_table").DataTable().destroy();
  if ($("#filter_from_date").val().length == 0) {
    filter_from_date = "2000-01-01";
    console.log(filter_from_date);
  } else {
    filter_from_date = $("#filter_from_date").val();
  }

  $.get(
    "https://marriottssc.secure-app.co.za/api/eid_aging/" +
      filter_from_date +
      "/" +
      $("#filter_to_date").val() +
      "/" +
      $("#dim_aging").val(),
    function (data) {
      ownerData = data;
      console.log(data);
      reportLoading.style.display = "none";

      $("#eid_aging_table").DataTable({
        dom: "Bfrtip",
        buttons: [
          {
            extend: "pdfHtml5",
            orientation: "landscape",
            pageSize: "LEGAL",
            extension: ".pdf",
            filename: "AR Account Owners",
            title: "AR Account Owners",
          },
          "copy",
          {
            extend: "csvHtml5",
            title: "AR Account Owners",
            filename: "AR Account Owners",
          },
          {
            extend: "excelHtml5",
            filename: "AR Account Owners",
            title: "AR Account Owners",
          },
          "print",
        ],
        paging: false,
        select: "single",
        searching: false,
        scrollCollapse: true,
        data: ownerData,
        columns: [
          { data: "DIM" },
          {
            data: "AGE_BUCKET1",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET2",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET3",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET456",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          { data: "TOTAL", render: $.fn.dataTable.render.number(",", ".", 2) },
        ],
      });

      $("#eid_aging_table_filter input").addClass("form-control");
      $("#eid_aging_table_filter input").attr("placeholder", "Search");
    }
  )
    .done(function (data) {
      reportLoading.style.display = "none";
      //draw the graph using the data from ajax call
    })
    .fail(function (xhr, status, error) {
      reportLoading.style.display = "none";
    })
    .always(function (xhr, status, error) {
      console.log(xhr);
    });
}

if (document.querySelector("table[data-id-table=eid_aging_perc_table]")) {
  /*Define only once for entire page*/
  var margin = { left: 80, right: 10, top: 20, bottom: 80 };
  var width = 340 - margin.left - margin.right;
  var height = 330 - margin.top - margin.bottom;
  var barPadding = 0.2;
  var axisTicks = { qty: 5, outerSize: 0, dateFormat: "%m-%d" };
  var t = d3.transition().duration(1000);

  // X Scale
  var x = d3
    .scaleBand()
    .domain(["0 - 30 Days", "31 - 60 Days", "61 - 90 Days", "91 - Older"])
    .range([0, width])
    .padding(0.2);

  // Y Scale
  var y = d3.scaleLinear().domain([0, 100]).range([height, 0]);

  /*Define only once for entire page*/

  // console.log("eid_aging_perc_table");
  var $eid_aging_perc_table = $("#eid_aging_perc_table");
  // console.log($eid_aging_perc_table);
  updateEIDAgingPerc();
}

/**
 * Update the EID Aging Percentage table and chart
 *  */
function updateEIDAgingPerc() {
  $("#eid_aging_perc_table").DataTable().destroy();
  if ($("#filter_from_date").val().length == 0) {
    filter_from_date = "2000-01-01";
    console.log(filter_from_date);
  } else {
    filter_from_date = $("#filter_from_date").val();
  }

  $.get(
    "https://marriottssc.secure-app.co.za/api/eid_aging_perc/" +
      filter_from_date +
      "/" +
      $("#filter_to_date").val() +
      "/" +
      $("#dim_aging").val(),
    function (data) {
      ownerData = data;
      // console.log(data);

      reportLoading.style.display = "none";

      $("#eid_aging_perc_table").DataTable({
        dom: "Bfrtip",
        buttons: [
          {
            extend: "pdfHtml5",
            orientation: "landscape",
            pageSize: "LEGAL",
            extension: ".pdf",
            filename: "AR Account Owners",
            title: "AR Account Owners",
          },
          "copy",
          {
            extend: "csvHtml5",
            title: "AR Account Owners",
            filename: "AR Account Owners",
          },
          {
            extend: "excelHtml5",
            filename: "AR Account Owners",
            title: "AR Account Owners",
          },
          "print",
        ],
        paging: false,
        select: "single",
        searching: false,
        scrollCollapse: true,
        data: ownerData,
        columns: [
          { data: "DIM" },
          {
            data: "AGE_BUCKET1_PERC",
            render: $.fn.dataTable.render.percentage(),
          },
          {
            data: "AGE_BUCKET2_PERC",
            render: $.fn.dataTable.render.percentage(),
          },
          {
            data: "AGE_BUCKET3_PERC",
            render: $.fn.dataTable.render.percentage(),
          },
          {
            data: "AGE_BUCKET456_PERC",
            render: $.fn.dataTable.render.percentage(),
          },
        ],
      });

      $("#eid_aging_perc_table_filter input").addClass("form-control");
      $("#eid_aging_perc_table_filter input").attr("placeholder", "Search");
    }
  )
    .done(function (data) {
      /**
       * Delete all the chart divs in order to rebuild them
       * with fresh data.
       */
      $("#eid_aging_charts_row").empty();

      /**
       * Repeat for each owner/hotel
       */
      data.forEach((d) => {
        eid_data = pivotData(data, d.DIM, "DIM");
        /**
         * Create a new div for each chart
         */
        $("#eid_aging_charts_row").append(
          '<div id="' + d.DIM + '" class="col-sm-4" ></div>'
        );

        /**
         * Define the div for the tooltips
         */
        var line1_tooltip = d3
          .select("body")
          .append("div")
          .attr("class", "line1-tooltip")
          .style("opacity", 0);

        var line2_tooltip = d3
          .select("body")
          .append("div")
          .attr("class", "line2-tooltip")
          .style("opacity", 0);

        var rect_tooltip = d3
          .select("body")
          .append("div")
          .attr("class", "rect-tooltip")
          .style("opacity", 0);

        /**
         * Draw Chart
         */
        var ownerChart = d3
          .select("#" + d.DIM)
          .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
          .append("g")
          .attr(
            "transform",
            "translate(" + margin.left + ", " + margin.top + ")"
          );

        //X label
        ownerChart
          .append("text")
          .attr("class", "x axis-label")
          .attr("x", width / 2)
          .attr("y", height + 65)
          .attr("font-size", "12px")
          .attr("text-anchor", "middle")
          .text("Aging by User - " + d.DIM);

        //Y label
        ownerChart
          .append("text")
          .attr("class", "y axis-label")
          .attr("x", -(height / 2))
          .attr("y", -60)
          .attr("font-size", "12px")
          .attr("text-anchor", "middle")
          .attr("transform", "rotate(-90)")
          .text("% Outstanding");

        // X Axis
        var xAxisCall = d3.axisBottom(x);
        var xAxisGroup = ownerChart
          .append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxisCall);

        // Y Axis
        var yAxisCall = d3.axisLeft(y).tickFormat(function (d) {
          return d + "%";
        });
        var yAxisGroup = ownerChart
          .append("g")
          .attr("class", "y axis")
          .call(yAxisCall);

        var elementColor = d3
          .scaleOrdinal()
          .domain([d.DIM, "Marriott Aging Policy", "Individual Target"])
          .range(["#63abf2", "red", "green"]);

        var keys = [d.DIM, "Marriott Aging Policy", "Individual Target"];

        var legend = ownerChart
          .append("g")
          .attr("transform", "translate(-60" + "," + (height + 25) + ")");

        /**
         * Add one dot in the legend for each name.
         * */
        var size = 10;
        legend
          .selectAll("mydots")
          .data(keys)
          .enter()
          .append("rect")
          .attr("x", 0)
          .attr("y", function (d, i) {
            return 0 + i * (size + 5);
          })
          .attr("width", size)
          .attr("height", size)
          .style("fill", function (d) {
            return elementColor(d);
          });

        legend
          .selectAll("legend")
          .data(keys)
          .enter()
          .append("text")
          .attr("x", 0 + size * 1.2)
          .attr("y", function (d, i) {
            return 0 + i * (size + 5) + size / 2;
          })
          .style("fill", function (d) {
            return elementColor(d);
          })
          .text(function (d) {
            return d;
          })
          .attr("text-anchor", "left")
          .style("alignment-baseline", "middle");

        eid_data.forEach((d) => {
          d.PERC = +d.PERC;
        });

        updateEIDAgingChart(
          eid_data,
          ownerChart,
          line1_tooltip,
          line2_tooltip,
          rect_tooltip
        );
      });

      reportLoading.style.display = "none";
      //draw the graph using the data from ajax call
    })
    .fail(function (xhr, status, error) {
      console.log(xhr);
      console.log(status);
      console.log(error);
      reportLoading.style.display = "none";
    })
    .always(function (xhr, status, error) {
      console.log(xhr);
      console.log(status);
      console.log(error);

      reportLoading.style.display = "none";
    });
}

function updateEIDAgingChart(
  data,
  element,
  line1_tooltip,
  line2_tooltip,
  rect_tooltip
) {
  var EID = data[0]["DIM"];
  // console.log("x.bandwidth");
  // console.log(x.bandwidth);
  // Bars
  var rects = element.selectAll(".bar").data(data, function (d) {
    return d.LABEL;
  });

  /**
   *  Draw the rectangles of the bar graph
   * */
  rects
    .enter()
    .append("rect")
    .attr("class", "bar")
    .attr("y", function (d) {
      return y(d.PERC);
    })
    .attr("x", function (d) {
      return x(d.LABEL);
    })
    .attr("height", function (d) {
      return height - y(d.PERC);
    })
    .attr("width", x.bandwidth)
    .attr("fill", "#63abf2")
    .on("mouseover", function (d) {
      rect_tooltip.transition().duration(200).style("opacity", 0.9);
      rect_tooltip
        .html(d.LABEL + "<br/>" + d.PERC + "%")
        .style("left", d3.event.pageX + "px")
        .style("top", d3.event.pageY + "px");
    })
    .on("mouseout", function (d) {
      rect_tooltip.transition().duration(500).style("opacity", 0);
    });

  /*
   * Ajax call to obtain data for line graph 1
   * */
  $.get(
    "https://marriottssc.secure-app.co.za/api/eid_aging_limits/MARRIOTT_AGING_POLICY/" +
      EID,
    function (data) {}
  )
    .done(function (data) {
      /*
       *If call is successfull, draw the line.
       */
      console.log(data);
      data.forEach((d) => {
        d.PERC = +d.PERC;
      });

      var line = d3
        .line()
        .x(function (d) {
          return x(d.AGING) + x.bandwidth() / 2;
        })
        .y(function (d) {
          return y(d.PERC);
        });

      element
        .append("path")
        .attr("class", "line")
        .attr("fill", "none")
        .attr("stroke", "red")
        .attr("stroke-with", "3px")
        .attr("d", line(data));

      // Add the scatterplot
      element
        .selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("id", function (d) {
          return d.AGING;
        })
        .attr("r", 3)
        .attr("cx", function (d) {
          return x(d.AGING) + x.bandwidth() / 2;
        })
        .attr("cy", function (d) {
          return y(d.PERC);
        })
        .attr("fill", "red")
        .on("mouseover", function (d) {
          line1_tooltip.transition().duration(200).style("opacity", 0.9);
          line1_tooltip
            .html(d.AGING + "<br/>" + d.PERC + "%")
            .style("left", d3.event.pageX + 5 + "px")
            .style("top", d3.event.pageY + 5 + "px");
        })
        .on("mouseout", function (d) {
          line1_tooltip.transition().duration(500).style("opacity", 0);
        });
    })
    .fail(function (result) {
      console.log(result);
    });

  /*
   * Ajax call to obtain data for line graph 2
   * */
  $.get(
    "https://marriottssc.secure-app.co.za/api/eid_aging_limits/INDIVIDUAL_AGING/" +
      EID,
    function (data) {}
  )
    .done(function (data) {
      /*
       *If call is successfull, draw the line.
       */
      console.log(data);
      data.forEach((d) => {
        d.PERC = +d.PERC;
      });

      var line = d3
        .line()
        .x(function (d) {
          return x(d.AGING) + x.bandwidth() / 2;
        })
        .y(function (d) {
          return y(d.PERC);
        });

      element
        .append("path")
        .attr("class", "line")
        .attr("fill", "none")
        .attr("stroke", "green")
        .attr("stroke-with", "3px")
        .attr("d", line(data));

      // Add the scatterplot
      element
        .selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("id", function (d) {
          return d.AGING;
        })
        .attr("r", 3)
        .attr("cx", function (d) {
          return x(d.AGING) + x.bandwidth() / 2;
        })
        .attr("cy", function (d) {
          return y(d.PERC);
        })
        .attr("fill", "green")
        .on("mouseover", function (d) {
          line2_tooltip.transition().duration(200).style("opacity", 0.9);
          line2_tooltip
            .html(d.AGING + "<br/>" + d.PERC + "%")
            .style("left", d3.event.pageX + 5 + "px")
            .style("top", d3.event.pageY + 5 + "px");
        })
        .on("mouseout", function (d) {
          line2_tooltip.transition().duration(500).style("opacity", 0);
        });
    })
    .fail(function (result) {
      console.log(result);
    });
}
////////////////////////////////////////////////////////END OF INDIVIDUAL/HOTEL AGING////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////START OF SSC AGING////////////////////////////////////////////////////////////////

if (document.querySelector("table[data-id-table=ssc_aging_table]")) {
  // console.log("ssc_aging_table");
  var $ssc_aging_table = $("#ssc_aging_table");
  // console.log($ssc_aging_table);
  updateSSCAging();
}

//Update SSC Aging report
function updateSSCAging() {
  $("#ssc_aging_table").DataTable().destroy();
  if ($("#filter_from_date").val().length == 0) {
    filter_from_date = "2000-01-01";
    console.log(filter_from_date);
  } else {
    filter_from_date = $("#filter_from_date").val();
  }

  var apiUrl =
    "https://marriottssc.secure-app.co.za/api/ssc_aging/" +
    filter_from_date +
    "/" +
    $("#filter_to_date").val();
  if ($("#filter_eid").val() != "ALL") {
    apiUrl = apiUrl + "/" + $("#filter_eid").val();
  } else {
    apiUrl = apiUrl + "/ALL";
  }
  if ($("#filter_resort").val() != "ALL") {
    apiUrl = apiUrl + "/" + $("#filter_resort").val();
  }
  //  console.log(apiUrl);

  $.get(apiUrl, function (data) {
    ownerData = data;
    console.log(data);
    reportLoading.style.display = "none";

    $("#ssc_aging_table").DataTable({
      dom: "Bfrtip",
      buttons: [
        {
          extend: "pdfHtml5",
          orientation: "landscape",
          pageSize: "LEGAL",
          extension: ".pdf",
          filename: "AR Account Owners",
          title: "AR Account Owners",
        },
        "copy",
        {
          extend: "csvHtml5",
          title: "AR Account Owners",
          filename: "AR Account Owners",
        },
        {
          extend: "excelHtml5",
          filename: "AR Account Owners",
          title: "AR Account Owners",
        },
        "print",
      ],
      paging: false,
      select: "single",
      searching: false,
      scrollY: "550px",
      scrollCollapse: true,
      data: ownerData,
      columns: [
        { data: "ACCOUNT_NAME" },
        {
          data: "AGE_BUCKET1",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET2",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET3",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET4",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET5",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET6",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        { data: "TOTAL", render: $.fn.dataTable.render.number(",", ".", 2) },
        {
          data: "CREDIT_LIMIT",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
      ],
    });
  })
    .done(function (result) {
      // console.log(result);
      reportLoading.style.display = "none";
    })
    .fail(function (result) {
      console.log(result);
    });
}

////////////////////////////////////////////////////////END OF SSC AGING////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////START OF AGING TEMPLATE////////////////////////////////////////////////////////////////

if (document.querySelector("table[data-id-table=aging_template]")) {
  var $aging_template = $("#aging_template");
  getSnapshotDates();
}

/**
 *
 * Function does Ajax call to check if the current period
 * has already been published. If current period exists in
 * table AR_PUBLISHED_DATES, the background will be highlighted
 * and text will show next to the Publish button.
 */
function alreadyPublished() {
  $.get(
    "https://marriottssc.secure-app.co.za/api/current_period_snapshot/" +
      $("#filter_resort").val() +
      "/" +
      $("#snapshot_date").val(),
    function (data) {
      //Do something
    }
  )
    .done(function (data, status, xhr) {
      console.log(data);
      if (typeof data !== "undefined" && data.length > 0) {
        console.log(data);
        $("#publish_text").text(
          "Period: " +
            data[0]["SNAPSHOT_DATE"] +
            " published by " +
            data[0]["CREATED_USER"] +
            " at " +
            data[0]["CREATED_DATE"]
        );
        $("#aging_template_wrapper").css("background", "rgb(235, 255, 242)");
        $("#publishAgingTempBtn").text("Re-Publish");

        if (data[0]["LOCKED_YN"] == "Y") {
          $("#publishAgingTempBtn").attr("disabled", true);
          $("#publishAgingTempBtn").text("Locked");
          $("#aging_template_wrapper").css("background", "rgb(255, 235, 235)");
        }
      } else {
        $("#publish_text").text("");
        $("#publishAgingTempBtn").text("Publish");
        $("#publishAgingTempBtn").attr("disabled", false);
      }
    })
    .fail(function (data, status, xhr) {
      console.log(data);
    })
    .always(function (data, status, xhr) {
      console.log(data);
    });
}

/**
 * Aging Template
 * Function too loop through the
 * given array and add values to
 * Period  LOV.
 */
function addSnapshotLOV(value, index, array) {
  console.log(value);
  $("#snapshot_date").append(`<option value="${value.SNAPSHOT_DATE}">
        ${value.SNAPSHOT_DATE}
        </option>`);
}

/**
 * Aging Template
 * Function that does an Ajax call to get the
 * snapshot dates for the selected hotel
 * and populate the Period LOV.
 */
function getSnapshotDates() {
  $("#snapshot_date").find("option").remove();
  $.get(
    "https://marriottssc.secure-app.co.za/api/snapshots/" +
      $("#filter_resort").val(),
    function (data) {
      console.log(data);
      console.log(data.length);
      if (data.length > 0) {
        data.forEach(addSnapshotLOV);

        updateAgingTemplate();
      } else {
        reportLoading.style.display = "none";
      }
    }
  )
    .done(function (data, status, xhr) {
      console.log(data);
    })
    .fail(function (data, status, xhr) {
      console.log(data);
      $("#publishAgingTempBtn").attr("disabled", true);
    })
    .always(function (data, status, xhr) {
      console.log(data);
    });
}

$("#snapshot_date").change(function () {
  console.log("Snapshot date filter changed.");
  updateAgingTemplate();
});

/**
 * Function that does an AJAX call to
 * api/aging_template route in routes/api.php
 * and builds a table to display the data.
 *
 *
 *
 * aging_template_data variable will old the data returned
 * from the AJAX call to be used when Publish button
 * is clicked.
 */
var aging_template_data = [];

function updateAgingTemplate() {
  totalsArray = [];

  $("#aging_template").DataTable().destroy();
  console.log("Update Aging Template");
  var apiUrl =
    "https://marriottssc.secure-app.co.za/api/aging_template/" +
    $("#filter_resort").val() +
    "/" +
    $("#snapshot_date").val();

  //  if ($("#filter_resort").val() != 'ALL')  {
  //    apiUrl = apiUrl + "/" + $("#filter_resort").val();
  //  }
  console.log(apiUrl);

  $.get(apiUrl, function (data) {
    ownerData = data;
    console.log(data);
    reportLoading.style.display = "none";

    $("#aging_template").DataTable({
      dom: "Bfrtip",
      buttons: [
        {
          extend: "pdfHtml5",
          orientation: "landscape",
          pageSize: "LEGAL",
          extension: ".pdf",
          filename: "AR Account Owners",
          title: "AR Account Owners",
        },
        "copy",
        {
          extend: "csvHtml5",
          title: "AR Account Owners",
          filename: "AR Account Owners",
        },
        {
          extend: "excelHtml5",
          filename: "AR Account Owners",
          title: "AR Account Owners",
        },
        "print",
      ],
      paging: false,
      select: "single",
      searching: false,
      scrollY: "390px",

      scrollCollapse: true,
      data: ownerData,
      columns: [
        { data: "PMS_ACCOUNT_NO", width: "6%" },
        { data: "ACCOUNT_NAME", width: "8%" },
        {
          data: "CREDIT_LIMIT",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET1",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET2",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET3",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET4",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET5",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET6",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "TOTAL",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "PROVISION",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "PREV120DAYS",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        { data: "REMARKS", width: "26%" },
      ],
      footerCallback: function (row, data, start, end, display) {
        var api = this.api(),
          data;

        // Remove the formatting to get integer data for summation
        var intVal = function (i) {
          return typeof i === "string"
            ? i.replace(/[\$,]/g, "") * 1
            : typeof i === "number"
            ? i
            : 0;
        };
        age1 = api
          .column(3)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        age2 = api
          .column(4)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        age3 = api
          .column(5)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        age4 = api
          .column(6)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        age5 = api
          .column(7)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        age6 = api
          .column(8)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        // Total over all pages
        total = api
          .column(9)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        provision = api
          .column(10)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        prev260days = api
          .column(11)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);

        /**
         * Assign the columns totals to an array
         * to be used in building the summary table
         * below the main table.
         */
        var summary = [
          "Aging % As per Buckets",
          age1 + age2,
          age3,
          age4,
          age5 + age6,
          total,
        ];
        /**
         * Function to divide each column total by the total of
         * the Balance column to get the precentage of each aging
         * bracket with regard to the total of all brackets.
         */
        function agingPercentage(value, index, array) {
          if (index > 0) {
            return (value / total) * 100;
          } else {
            return value;
          }
        }
        /**
         * Calculate the percentages of each total and
         * add totals and percentage to one array.
         */
        var summary_perc = summary.map(agingPercentage);
        console.log(summary);
        console.log(summary_perc);
        /**
         * A function to format the numbers
         * of an the totalsArray
         */
        function formatAgingNumbers(value, index, array) {
          if (index > 0) {
            return value
              .toFixed(2)
              .toString()
              .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
          } else {
            return value;
          }
        }
        var summaryFormatted = summary.map(formatAgingNumbers);
        var summary_percFormatted = summary_perc.map(formatAgingNumbers);
        totalsArray.push(summaryFormatted);
        totalsArray.push(summary_percFormatted);

        age1 = $.fn.dataTable.render.number(",", ".", 2).display(age1);
        age2 = $.fn.dataTable.render.number(",", ".", 2).display(age2);
        age3 = $.fn.dataTable.render.number(",", ".", 2).display(age3);
        age4 = $.fn.dataTable.render.number(",", ".", 2).display(age4);
        age5 = $.fn.dataTable.render.number(",", ".", 2).display(age5);
        age6 = $.fn.dataTable.render.number(",", ".", 2).display(age6);
        total = $.fn.dataTable.render.number(",", ".", 2).display(total);
        provision = $.fn.dataTable.render
          .number(",", ".", 2)
          .display(provision);
        prev260days = $.fn.dataTable.render
          .number(",", ".", 2)
          .display(prev260days);
        // Update footer
        $(api.column(3).footer()).html(age1);
        $(api.column(4).footer()).html(age2);
        $(api.column(5).footer()).html(age3);
        $(api.column(6).footer()).html(age4);
        $(api.column(7).footer()).html(age5);
        $(api.column(8).footer()).html(age6);
        $(api.column(9).footer()).html(total);
        $(api.column(10).footer()).html(provision);
        $(api.column(11).footer()).html(prev260days);
      },
    });
  })
    .done(function (data) {
      /**
       * Refresh the summary table with
       * the new totals of the main table.
       */
      // console.log(totalsArray);
      $("#summary_table").DataTable().clear().draw();
      $("#summary_table").DataTable().rows.add(totalsArray); // Add new data
      $("#summary_table").DataTable().columns.adjust().draw(); // Redraw the DataTable
      aging_template_data = data;
      //  console.log(data);
      reportLoading.style.display = "none";
      alreadyPublished();
    })
    .fail(function (result) {
      console.log(result);
    });
}

/**
 * Aging Template Live Data  - When double clicking on a line in the table,
 * the Edit remarks modal will open where new remarks can be added
 * or existing remarks can be edited by authorised users.
 *
 * Table cell values are assigned to variables which will be assigned
 *  to the modal form fields.
 *
 */

/**
 * Initialise variables to hold the table cell values
 * of the double clicked row
 */
var remarksAccountNo = "";
var remarksAccountName = "";
var prev120days = "";
var remarks = "";

/**
 * On double clicking the row, assigned the row values
 */

$("#aging_template").on("dblclick", "tr", function (e) {
  $("#aging_template").DataTable().row(this).select();
  var $element = $("#aging_template").DataTable().row(this).data();
  remarksAccountNo = $element.PMS_ACCOUNT_NO;
  remarksAccountName = $element.ACCOUNT_NAME;
  remarks = $element.REMARKS;
  prev120days = $element.PREV120DAYS;
  provision = $element.PROVISION;

  console.log($element.ACCOUNT_NO);
  $("#remarksModal").modal("show");
});

/**
 * When the Edit Remarks modal opens, assign the values of
 * the variables assigned above to the modal fields
 */

$("#remarksModal").on("show.bs.modal", function (event) {
  var modal = $(this);
  $("#REMARKS_ACCOUNT_NO").val(remarksAccountNo);
  $("#REMARKS_ACCOUNT_NAME").val(remarksAccountName);
  $("#REMARKS").val(remarks);
  $("#PREV120DAYS").val(prev120days);
  $("#PROVISION").val(provision);
});

/**
 * When clicking on Save Changes in Edit Remarks modal
 * an AJAX call is made to save the remarks in the
 * AR_AGING_TEMP_REMARKS table ONLY if the Account Number is
 * populated and the remark is not empty.
 *
 */
$("#remarksModalBtn").click(function () {
  var formArray = $("#remarksModalForm").serializeArray();
  console.log(formArray);
  if (
    $("#REMARKS_ACCOUNT_NO").val().length > 0 &&
    $("#REMARKS").val().length > 0
  ) {
    console.log("Update remarks");
    updateRemarksURI =
      "https://marriottssc.secure-app.co.za/api/update_aging_temp_remarks/" +
      $("#filter_resort").val() +
      "/" +
      $("#snapshot_date").val();
    console.log(updateRemarksURI);
    $.post(
      updateRemarksURI,
      $("#remarksModalForm").serializeArray(),
      function (data) {
        console.log(data);
      }
    )
      .done(function (msg) {
        console.log(msg);
        /**
         * Refresh the table and close modal
         */
        updateAgingTemplate();
        $("#remarksModal").modal("hide");
      })
      .fail(function (error) {
        alert("Remarks could not be saved. ");
        console.log(error);
      })
      .always(function (data, status, xhr) {
        // alert("Remarks could not be saved. ");
        console.log(data);
      });
  } else {
    alert("Please complete all the required fields!");
  }
});

/**
 * When Delete is clicked on Edit Remarks modal,
 * a pop-up window will open to ask the user to confirm.
 */
$("#remarksDeleteModalBtn").click(function () {
  $("#deleteRemarksModal").modal("show");
});

/**
 * When Delete is click on the pop-up windows, an AJAX call is made
 * to delete the record from AR_AGING_TEMP_REMARKS table for the RESORT AND
 * PMS_ACCOUNT_NO in the Edit Remarks modal.
 */
$("#deleteRemarksConfirmBtn").click(function () {
  var ownerArray = $("#remarksModalForm").serializeArray();
  console.log(ownerArray);
  console.log("Delete remarks");
  $.post(
    "https://marriottssc.secure-app.co.za/api/delete_aging_temp_remarks/" +
      $("#filter_resort").val(),
    $("#remarksModalForm").serializeArray(),
    function (data) {
      console.log(data);
    }
  )
    .done(function (msg) {
      console.log(msg);
      updateAgingTemplate();
      $("#remarksModal").modal("hide");
      $("#deleteRemarksModal").modal("hide");
    })
    .fail(function (error) {
      alert(
        "remarks could not be deleted. Please contact administrator" + error
      );
    });
});
/**
 * When Publish button on Aging Template page is clicked,
 * a snapshot is taken of the data in the report and
 * saved to AR_AGING_TEMP_SNAPSHOTS.
 */
$("#publishAgingTempBtn").click(function () {
  // var csrf_array = [];
  // csrf_array['csrf_name'] =  $("#getTokenName").val();
  // csrf_array['csrf_value'] = $("#getTokenValue").val() ;
  // aging_template_data.push(csrf_array);

  console.log("Publish button clicked");

  $.get(
    "https://marriottssc.secure-app.co.za/api/publish_aging_template/" +
      $("#filter_resort").val() +
      "/" +
      $("#snapshot_date").val(),
    function (data) {
      //Do something
    }
  )
    .done(function (data, status, xhr) {
      console.log(data);
      // $("#publishAgingTempBtn").attr("disabled", true);
      alreadyPublished();
    })
    .fail(function (data, status, xhr) {
      console.log(data);
    })
    .always(function (data, status, xhr) {
      console.log(data);
    });
});

////////////////////////////////////////////////////////END OF AGING TEMPLATE////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////START OF AGING PUBLISHED////////////////////////////////////////////////////////////////

if (document.querySelector("table[data-id-table=aging_published]")) {
  var $aging_published = $("#aging_published");
  getPublishedDates();
}

/**
 * Aging Published
 * Function too loop through the
 * given array and add values to
 * Published Date LOV.
 */
function addPublishedLOV(value, index, array) {
  console.log(value);
  $("#filter_snapshot_date").append(`<option value="${value.SNAPSHOT_DATE}">
        ${value.SNAPSHOT_DATE}
        </option>`);
}

/**
 * Aging Published
 * Function that does an Ajax call to get the
 * snapshot dates for the selected hotel
 * and populate the Published date LOV.
 */
function getPublishedDates() {
  console.log("Running getPublishedDate function");
  console.log(
    "https://marriottssc.secure-app.co.za/api/published_snapshots/" +
      $("#filter_resort_published").val()
  );
  $("#filter_snapshot_date").find("option").remove();
  $.get(
    "https://marriottssc.secure-app.co.za/api/published_snapshots/" +
      $("#filter_resort_published").val(),
    function (data) {}
  )
    .done(function (data) {
      console.log(data.length);
      if (data.length > 0) {
        data.forEach(addPublishedLOV);
        // console.log(data[0]['SNAPSHOT_DATE']);
        $("#content").css({ display: "block" });
        $("#nocontent").css({ display: "none" });
        if (document.querySelector("table[data-id-table=aging_published]")) {
          updateAgingPublished();
        }
        if (window.location.pathname.indexOf("credit_meeting_minutes") > 0) {
          if ($("#hotel_access").text() == "N") {
            $("#creditMeetingMinutesFields").attr("disabled", false);
            $("#saveCreditMeetingMinutesBtn").attr("disabled", false);
          }

          getCreditMeetingMinutes();
        }
      } else {
        if (window.location.pathname.indexOf("credit_meeting_minutes") > 0) {
          document.getElementById("creditMeetingMinutesForm").reset();
          $(".appended").remove();
          // $("#CR_MEETING_HOTEL").val($("#filter_resort_published option:selected").text());
          $("#creditMeetingMinutesFields").attr("disabled", true);
          $("#saveCreditMeetingMinutesBtn").attr("disabled", true);
        }

        reportLoading.style.display = "none";
        $("#content").css({ display: "none" });
        $("#nocontent").css({ display: "block" });
      }
    })
    .fail(function (data, status, xhr) {
      console.log(data);
      $("#publishAgingTempBtn").attr("disabled", true);
    })
    .always(function (data, status, xhr) {
      console.log(data);
    });
}

/**
 * Aging Published
 * When the Hotel filter is changed,
 * the snapshot dates for that hotel
 * will be obtained and the Published Date
 * LOV will be populated with this date.
 */
$("#filter_resort_published").change(function () {
  Cookies.set("filter_resort_published", $(this).val(), { expires: 30 });
  // $("#CR_MEETING_HOTEL").val($("#filter_resort_published option:selected").text());
  $("#aging_published").DataTable().clear().draw();
  getPublishedDates();
});
/**
 * When the search button is clicked on Aging Published
 * the updateAgingPublished function is called to
 * obtain the published aging via Ajax and build the table.
 */
$("#searchPublishedAgingBtn").click(function () {
  updateAgingPublished();
});

$("#lockAgingBtn").click(function () {
  postdata = {
    csrf_name: $("#getTokenName").val(),
    csrf_value: $("#getTokenValue").val(),
  };

  console.log(postdata);
  console.log("Lock snapshot date");
  $.post(
    "https://marriottssc.secure-app.co.za/api/lock_snapshot_date/" +
      $("#filter_resort_published").val() +
      "/" +
      $("#filter_snapshot_date").val(),
    postdata,
    function (data) {
      console.log(data);
    }
  )
    .done(function (msg) {
      console.log(msg);
      $("#aging_published_wrapper").css("background", "rgb(255, 235, 235)");
      $("#lockAgingBtn").attr("disabled", true);
    })
    .fail(function (error) {
      alert("Could not lock aging. Please contact administrator" + error);
    });
});

/**
 * Function does Ajax call to check if the current period
 * has already been locked. If current period exists in
 * table AR_SNAPSHOT_DATES and LOCKED_YN = Y, the background will be highlighted
 * and text will show next to the Lock button.
 */
function alreadyLocked() {
  $.get(
    "https://marriottssc.secure-app.co.za/api/current_period_snapshot/" +
      $("#filter_resort_published").val() +
      "/" +
      $("#snapshot_date").val(),
    function (data) {
      //Do something
    }
  )
    .done(function (data, status, xhr) {
      console.log(data);
      if (typeof data !== "undefined" && data.length > 0) {
        if (data[0]["LOCKED_YN"] == "Y") {
          $("#lockAgingBtn").attr("disabled", true);
          $("#lockAgingBtn").text("Locked");
          $("#aging_published_wrapper").css("background", "rgb(255, 235, 235)");
        } else {
          $("#lockAgingBtn").attr("disabled", false);
          $("#lockAgingBtn").text("Lock");
        }
      }
    })
    .fail(function (data, status, xhr) {
      console.log(data);
    })
    .always(function (data, status, xhr) {
      console.log(data);
    });
}

/**
 * Function that does an AJAX call to
 * api/aging_published route in routes/api.php
 * and builds a table to display the data.
 *
 *
 *
 * aging_template_data variable will old the data returned
 * from the AJAX call to be used when Publish button
 * is clicked.
 */
function updateAgingPublished() {
  console.log("Inside updateAgingPublished function");
  /**
   * Array hold the columns totals
   * which will be used to build the
   * summary table below the main table.
   */
  totalsArray = [];

  $("#aging_published").DataTable().destroy();
  console.log("Update Aging Template");
  var apiUrl =
    "https://marriottssc.secure-app.co.za/api/aging_published/" +
    $("#filter_resort_published").val() +
    "/" +
    $("#filter_snapshot_date").val();

  console.log(apiUrl);

  $.get(apiUrl, function (data) {
    ownerData = data;
    console.log(data);
    reportLoading.style.display = "none";

    $("#aging_published").DataTable({
      dom: "Bfrtip",
      buttons: [
        {
          extend: "pdfHtml5",
          orientation: "landscape",
          pageSize: "LEGAL",
          extension: ".pdf",
          filename: "AR Account Owners",
          title: "AR Account Owners",
        },
        "copy",
        {
          extend: "csvHtml5",
          title: "AR Account Owners",
          filename: "AR Account Owners",
        },
        {
          extend: "excelHtml5",
          filename: "AR Account Owners",
          title: "AR Account Owners",
        },
        "print",
      ],
      paging: false,
      select: "single",
      searching: false,
      scrollY: "390px",
      scrollCollapse: true,
      data: ownerData,
      columns: [
        { data: "PMS_ACCOUNT_NO", width: "6%" },
        { data: "ACCOUNT_NAME", width: "8%" },
        {
          data: "CREDIT_LIMIT",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET1",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET2",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET3",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET4",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET5",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "AGE_BUCKET6",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "TOTAL",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "PROVISION",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        {
          data: "PREV120DAYS",
          width: "6%",
          render: $.fn.dataTable.render.number(",", ".", 2),
        },
        { data: "REMARKS", width: "26%" },
      ],
      footerCallback: function (row, data, start, end, display) {
        var api = this.api(),
          data;

        // Remove the formatting to get integer data for summation
        var intVal = function (i) {
          return typeof i === "string"
            ? i.replace(/[\$,]/g, "") * 1
            : typeof i === "number"
            ? i
            : 0;
        };
        age1 = api
          .column(3)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        age2 = api
          .column(4)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        age3 = api
          .column(5)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        age4 = api
          .column(6)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        age5 = api
          .column(7)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        age6 = api
          .column(8)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        // Total over all pages
        total = api
          .column(9)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        provision = api
          .column(10)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);
        prev260days = api
          .column(11)
          .data()
          .reduce(function (a, b) {
            return intVal(a) + intVal(b);
          }, 0);

        /**
         * Assign the columns totals to an array
         * to be used in building the summary table
         * below the main table.
         */
        var summary = [
          "Aging % As per Buckets",
          age1 + age2,
          age3,
          age4,
          age5 + age6,
          total,
        ];
        /**
         * Function to divide each column total by the total of
         * the Balance column to get the precentage of each aging
         * bracket with regard to the total of all brackets.
         */
        function agingPercentage(value, index, array) {
          if (index > 0) {
            return (value / total) * 100;
          } else {
            return value;
          }
        }
        /**
         * Calculate the percentages of each total and
         * add totals and percentage to one array.
         */
        var summary_perc = summary.map(agingPercentage);
        console.log(summary);
        console.log(summary_perc);
        /**
         * A function to format the numbers
         * of an the totalsArray
         */
        function formatAgingNumbers(value, index, array) {
          if (index > 0) {
            return value
              .toFixed(2)
              .toString()
              .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
          } else {
            return value;
          }
        }
        var summaryFormatted = summary.map(formatAgingNumbers);
        var summary_percFormatted = summary_perc.map(formatAgingNumbers);
        totalsArray.push(summaryFormatted);
        totalsArray.push(summary_percFormatted);

        age1 = $.fn.dataTable.render.number(",", ".", 2).display(age1);
        age2 = $.fn.dataTable.render.number(",", ".", 2).display(age2);
        age3 = $.fn.dataTable.render.number(",", ".", 2).display(age3);
        age4 = $.fn.dataTable.render.number(",", ".", 2).display(age4);
        age5 = $.fn.dataTable.render.number(",", ".", 2).display(age5);
        age6 = $.fn.dataTable.render.number(",", ".", 2).display(age6);
        total = $.fn.dataTable.render.number(",", ".", 2).display(total);
        provision = $.fn.dataTable.render
          .number(",", ".", 2)
          .display(provision);
        prev260days = $.fn.dataTable.render
          .number(",", ".", 2)
          .display(prev260days);
        // Update footer
        $(api.column(3).footer()).html(age1);
        $(api.column(4).footer()).html(age2);
        $(api.column(5).footer()).html(age3);
        $(api.column(6).footer()).html(age4);
        $(api.column(7).footer()).html(age5);
        $(api.column(8).footer()).html(age6);
        $(api.column(9).footer()).html(total);
        $(api.column(10).footer()).html(provision);
        $(api.column(11).footer()).html(prev260days);
      },
    });
  })
    .done(function (data) {
      /**
       * Refresh the summary table with
       * the new totals of the main table.
       */
      // console.log(totalsArray);
      $("#summary_table").DataTable().clear().draw();
      $("#summary_table").DataTable().rows.add(totalsArray); // Add new data
      $("#summary_table").DataTable().columns.adjust().draw(); // Redraw the DataTable
      alreadyLocked();

      //  console.log(data);
      reportLoading.style.display = "none";
    })
    .fail(function (result) {
      console.log(result);
      reportLoading.style.display = "none";
    });
}

////////////////////////////////////////////////////////END OF AGING PUBLISHED////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////START OF CREDIT MEETING MINUTES///////////////////////////////////////////////////////////////////////

var currentCreditMinutes = [];

function getCreditMeetingMinutes() {
  console.log("running getCreditMeetingMinutes");
  /**
   * Reset the form
   *
   */
  console.log("removing appended data and resetting form");
  document.getElementById("creditMeetingMinutesForm").reset();
  $(".appended").remove();
  /**
   * Ajax call to get the saved minutes
   * from AR_CREDIT_MEETING_MINUTES
   * api call: /api/credit_meeting_minutes/{RESORT}/{SNAPSHOT_DATE}
   */
  for (var i = 1; i < 2; i++) {
    console.log("for loop run once");
  }

  $.get(
    "https://marriottssc.secure-app.co.za/api/credit_meeting_minutes/" +
      $("#filter_resort_published").val() +
      "/" +
      $("#filter_snapshot_date").val(),
    function (data) {
      console.log("inside function(data)");
    }
  )
    .done(function (data) {
      currentCreditMinutes = data;

      console.log("api/credit_meeting_minutes/ call finished");

      if (data.length > 0) {
        console.log("Running .done sequence");

        /**
         * Assign the returned data to the form fields
         */
        if (data[0]["CR_MEETING_DATE"]) {
          $("#CR_MEETING_DATE").val(data[0]["CR_MEETING_DATE"]);
        }
        if (data[0]["CR_MEETING_TIME"]) {
          $("#CR_MEETING_TIME").val(data[0]["CR_MEETING_TIME"]);
        }
        // if (data[0]["CR_MEETING_HOTEL"]) {
        //   $("#CR_MEETING_HOTEL").val(data[0]["CR_MEETING_HOTEL"]);
        // }
        // if (data[0]["CR_MEETING_HOTEL"]) {
        //   $("#CR_MEETING_HOTEL").val(data[0]["CR_MEETING_HOTEL"]);
        // }
        if (data[0]["CR_MEETING_NEXT_DATE"]) {
          $("#CR_MEETING_NEXT_DATE").val(data[0]["CR_MEETING_NEXT_DATE"]);
        }
        if (data[0]["CR_MEETING_PRESENT"]) {
          $("#CR_MEETING_PRESENT").val(data[0]["CR_MEETING_PRESENT"]);
        }
        if (data[0]["CR_MEETING_NOT_PRESENT"]) {
          $("#CR_MEETING_NOT_PRESENT").val(data[0]["CR_MEETING_NOT_PRESENT"]);
        }

        /**
         * Credit Meeting Minutes
         * loop for each of the 8
         * tables on the html to check if
         * there is a value in the database field
         * for the corresponding table/item
         */
        for (var i = 1; i <= 8; i++) {
          // console.log("i");
          // console.log(i);
          /**
           * loop again 6 times
           * to check if there are values
           * for all 6 rows.
           * Only create new rows in html if there
           * is a value in that row in the
           * database. I only created a max of
           * 6 columns for each item in the database
           * table. If more rows are required then more
           * columns need to be added.
           */

          for (j = 1; j < 7; j++) {
            //   console.log("j");
            // console.log(j);

            if (j > 1) {
              if (
                data[0]["ITEM" + i + "_ROW" + j + "_COL2"] ||
                data[0]["ITEM" + i + "_ROW" + j + "_COL3"] ||
                data[0]["ITEM" + i + "_ROW" + j + "_COL4"]
              ) {
                console.log(data[0]["ITEM" + i + "_ROW" + j + "_COL2"]);
                console.log(data[0]["ITEM" + i + "_ROW" + j + "_COL3"]);
                console.log(data[0]["ITEM" + i + "_ROW" + j + "_COL4"]);
                /**
                 * Starting at row 2 since the first row for each item is already there
                 */
                for (x = 2; x < j; x++) {
                  if (!document.querySelector(".appended.class-" + x)) {
                    console.log(x);
                    console.log("does not exist. append ");
                    appendRow(i, "#" + i, x - 1);
                  }
                }

                appendRow(i, "#" + i, j - 1);
              }
            }
            //item 1 for each html table
            if (data[0]["ITEM" + i + "_ROW" + j + "_COL2"]) {
              $("#ITEM" + i + "_ROW" + j + "_COL2").val(
                data[0]["ITEM" + i + "_ROW" + j + "_COL2"]
              );
            }
            if (data[0]["ITEM" + i + "_ROW" + j + "_COL3"]) {
              $("#ITEM" + i + "_ROW" + j + "_COL3").val(
                data[0]["ITEM" + i + "_ROW" + j + "_COL3"]
              );
            }
            if (data[0]["ITEM" + i + "_ROW" + j + "_COL4"]) {
              $("#ITEM" + i + "_ROW" + j + "_COL4").val(
                data[0]["ITEM" + i + "_ROW" + j + "_COL4"]
              );
            }
          }
        }
      }
    })
    .fail(function (xhr, status, error) {
      // console.log(xhr);
    })
    .always(function (xhr, status, error) {
      // console.log(xhr);
      console.log("api/credit_meeting_minutes/ api call .always sequence");
    });
}

/**
 * When clicking on the last row in a section
 * a button will be added to add a new row.
 */
// $('#1').on('click','tr:last', function (event) {

//   tableRef = $("#1").data("item");
//   console.log(tableRef);
//   tableID = $(this)[0]["id"];
//   lastRow = $('#1 tr:last')[0]["rowIndex"];
//   // $('#1').append('<button id="addRowBtn" type="button" class="btn btn-secondary"><i class="fas fa-angle-double-down"></i></button>');
//   // ;
// });

/**
 * When you click on button to add a new row,
 * append a new row
 */
$("#addRowBtn1").on("click", function () {
  console.log("add row button clicked");
  appendRow(1, "#1", $("#1 tr:last")[0]["rowIndex"]);
  if ($("#1 tr:last")[0]["rowIndex"] == 6) {
    $("#addRowBtn1").remove();
  }
});
$("#addRowBtn2").on("click", function () {
  console.log("add row button clicked");
  appendRow(2, "#2", $("#2 tr:last")[0]["rowIndex"]);
  if ($("#2 tr:last")[0]["rowIndex"] == 6) {
    $("#addRowBtn2").remove();
  }
});
$("#addRowBtn3").on("click", function () {
  console.log("add row button clicked");
  appendRow(3, "#3", $("#3 tr:last")[0]["rowIndex"]);
  if ($("#3 tr:last")[0]["rowIndex"] == 6) {
    $("#addRowBtn3").remove();
  }
});
$("#addRowBtn4").on("click", function () {
  console.log("add row button clicked");
  appendRow(4, "#4", $("#4 tr:last")[0]["rowIndex"]);
  if ($("#4 tr:last")[0]["rowIndex"] == 6) {
    $("#addRowBtn4").remove();
  }
});
$("#addRowBtn5").on("click", function () {
  console.log("add row button clicked");
  appendRow(5, "#5", $("#5 tr:last")[0]["rowIndex"]);
  if ($("#5 tr:last")[0]["rowIndex"] == 6) {
    $("#addRowBtn5").remove();
  }
});
$("#addRowBtn6").on("click", function () {
  console.log("add row button clicked");
  appendRow(6, "#6", $("#6 tr:last")[0]["rowIndex"]);
  if ($("#6 tr:last")[0]["rowIndex"] == 6) {
    $("#addRowBtn6").remove();
  }
});
$("#addRowBtn7").on("click", function () {
  console.log("add row button clicked");
  appendRow(7, "#7", $("#7 tr:last")[0]["rowIndex"]);
  if ($("#7 tr:last")[0]["rowIndex"] == 6) {
    $("#addRowBtn7").remove();
  }
});
$("#addRowBtn8").on("click", function () {
  console.log("add row button clicked");
  // if ($('#8 tr:last')[0]["rowIndex"] != 6) {
  appendRow(8, "#8", $("#8 tr:last")[0]["rowIndex"]);
  if ($("#8 tr:last")[0]["rowIndex"] == 6) {
    $("#addRowBtn8").remove();
  }
  // }
});

/**
 * Credit Meeting Minutes
 * Function to append a new row
 *
 */
function appendRow(tableID, tableRef, currentRow) {
  var row = +currentRow + 1;
  console.log("adding row" + row);
  // console.log(row);

  $(tableRef).append(
    '<tr class="appended class-' +
      row +
      '"><th scope="row">' +
      tableID +
      '</th><td> <textarea  class="form-control credit_meeting_minutes_form"  id="ITEM' +
      tableID +
      "_ROW" +
      row +
      '_COL2" name="ITEM' +
      tableID +
      "_ROW" +
      row +
      '_COL2" value="" rows="1" ></textarea> </td>\
  <td> <textarea  class="form-control credit_meeting_minutes_form"  id="ITEM' +
      tableID +
      "_ROW" +
      row +
      '_COL3" name="ITEM' +
      tableID +
      "_ROW" +
      row +
      '_COL3" value="" rows="1" ></textarea></td>\
  <td> <textarea  class="form-control credit_meeting_minutes_form"  id="ITEM' +
      tableID +
      "_ROW" +
      row +
      '_COL4" name="ITEM' +
      tableID +
      "_ROW" +
      row +
      '_COL4" value="" rows="1" ></textarea></td></td>\
</tr>'
  );
}

/**
 * Credit Meeting Minutes
 * Function to save the input
 * of the form
 */
function updateCreditMeetingMinutes() {
  console.log($("#creditMeetingMinutesForm").serializeArray());
  $.post(
    "https://marriottssc.secure-app.co.za//api/update_credit_meeting_minutes/" +
      $("#filter_resort_published").val() +
      "/" +
      $("#filter_snapshot_date").val(),
    $("#creditMeetingMinutesForm").serializeArray(),
    function (data) {
      console.log(data);
    }
  )
    .done(function (msg) {
      console.log(msg);
    })
    .fail(function (error) {
      console.log(error);
    })
    .always(function (error) {
      console.log(error);
    });
}

$("#saveCreditMeetingMinutesBtn").on("click", function () {
  // console.log("Save minutes");
  updateCreditMeetingMinutes();
  // location.reload();

  setTimeout(getCreditMeetingMinutes, 500);
});

$("#exportCreditMeetingMinutesBtn").on("click", function () {
  showCreditMinutesModal();
});

function showCreditMinutesModal() {
  $("#credit_minutes_modal").modal("show");
  $("#credit_minutes_modal_table").DataTable({
    dom: "Bfrtip",
    buttons: [
      {
        extend: "pdfHtml5",
        orientation: "landscape",
        pageSize: "LEGAL",
        extension: ".pdf",
        filename: "AR Credit Meeting Minutes",
        title: "AR Credit Meeting Minutes",
      },
      "copy",
      {
        extend: "csvHtml5",
        title: "AR Credit Meeting Minutes",
        filename: "AR Credit Meeting Minutes",
      },
      {
        extend: "excelHtml5",
        filename: "AR Credit Meeting Minutes",
        title: "AR Credit Meeting Minutes",
      },
      "print",
    ],
    paging: false,
    select: "single",
    scrollY: "600px",
    scrollX: "600px",
    searching: false,
    scrollCollapse: true,
    pageLength: 50,
    data: currentCreditMinutes,
    columns: [
      { data: "SNAPSHOT_DATE" },
      { data: "RESORT" },
      { data: "CR_MEETING_DATE" },
      { data: "CR_MEETING_TIME" },
      { data: "CR_MEETING_NEXT_DATE" },
      { data: "CR_MEETING_PRESENT" },
      { data: "CR_MEETING_NOT_PRESENT" },
      { data: "ITEM1_ROW1_COL2" },
      { data: "ITEM1_ROW1_COL3" },
      { data: "ITEM1_ROW1_COL4" },
      { data: "ITEM1_ROW2_COL2" },
      { data: "ITEM1_ROW2_COL3" },
      { data: "ITEM1_ROW2_COL4" },
      { data: "ITEM1_ROW3_COL2" },
      { data: "ITEM1_ROW3_COL3" },
      { data: "ITEM1_ROW3_COL4" },
      { data: "ITEM1_ROW4_COL2" },
      { data: "ITEM1_ROW4_COL3" },
      { data: "ITEM1_ROW4_COL4" },
      { data: "ITEM1_ROW5_COL2" },
      { data: "ITEM1_ROW5_COL3" },
      { data: "ITEM1_ROW5_COL4" },
      { data: "ITEM1_ROW6_COL2" },
      { data: "ITEM1_ROW6_COL3" },
      { data: "ITEM1_ROW6_COL4" },
      { data: "ITEM2_ROW1_COL2" },
      { data: "ITEM2_ROW1_COL3" },
      { data: "ITEM2_ROW1_COL4" },
      { data: "ITEM2_ROW2_COL2" },
      { data: "ITEM2_ROW2_COL3" },
      { data: "ITEM2_ROW2_COL4" },
      { data: "ITEM2_ROW3_COL2" },
      { data: "ITEM2_ROW3_COL3" },
      { data: "ITEM2_ROW3_COL4" },
      { data: "ITEM2_ROW4_COL2" },
      { data: "ITEM2_ROW4_COL3" },
      { data: "ITEM2_ROW4_COL4" },
      { data: "ITEM2_ROW5_COL2" },
      { data: "ITEM2_ROW5_COL3" },
      { data: "ITEM2_ROW5_COL4" },
      { data: "ITEM2_ROW6_COL2" },
      { data: "ITEM2_ROW6_COL3" },
      { data: "ITEM2_ROW6_COL4" },
      { data: "ITEM3_ROW1_COL2" },
      { data: "ITEM3_ROW1_COL3" },
      { data: "ITEM3_ROW1_COL4" },
      { data: "ITEM3_ROW2_COL2" },
      { data: "ITEM3_ROW2_COL3" },
      { data: "ITEM3_ROW2_COL4" },
      { data: "ITEM3_ROW3_COL2" },
      { data: "ITEM3_ROW3_COL3" },
      { data: "ITEM3_ROW3_COL4" },
      { data: "ITEM3_ROW4_COL2" },
      { data: "ITEM3_ROW4_COL3" },
      { data: "ITEM3_ROW4_COL4" },
      { data: "ITEM3_ROW5_COL2" },
      { data: "ITEM3_ROW5_COL3" },
      { data: "ITEM3_ROW5_COL4" },
      { data: "ITEM3_ROW6_COL2" },
      { data: "ITEM3_ROW6_COL3" },
      { data: "ITEM3_ROW6_COL4" },
      { data: "ITEM4_ROW1_COL2" },
      { data: "ITEM4_ROW1_COL3" },
      { data: "ITEM4_ROW1_COL4" },
      { data: "ITEM4_ROW2_COL2" },
      { data: "ITEM4_ROW2_COL3" },
      { data: "ITEM4_ROW2_COL4" },
      { data: "ITEM4_ROW3_COL2" },
      { data: "ITEM4_ROW3_COL3" },
      { data: "ITEM4_ROW3_COL4" },
      { data: "ITEM4_ROW4_COL2" },
      { data: "ITEM4_ROW4_COL3" },
      { data: "ITEM4_ROW4_COL4" },
      { data: "ITEM4_ROW5_COL2" },
      { data: "ITEM4_ROW5_COL3" },
      { data: "ITEM4_ROW5_COL4" },
      { data: "ITEM4_ROW6_COL2" },
      { data: "ITEM4_ROW6_COL3" },
      { data: "ITEM4_ROW6_COL4" },
      { data: "ITEM5_ROW1_COL2" },
      { data: "ITEM5_ROW1_COL3" },
      { data: "ITEM5_ROW1_COL4" },
      { data: "ITEM5_ROW2_COL2" },
      { data: "ITEM5_ROW2_COL3" },
      { data: "ITEM5_ROW2_COL4" },
      { data: "ITEM5_ROW3_COL2" },
      { data: "ITEM5_ROW3_COL3" },
      { data: "ITEM5_ROW3_COL4" },
      { data: "ITEM5_ROW4_COL2" },
      { data: "ITEM5_ROW4_COL3" },
      { data: "ITEM5_ROW4_COL4" },
      { data: "ITEM5_ROW5_COL2" },
      { data: "ITEM5_ROW5_COL3" },
      { data: "ITEM5_ROW5_COL4" },
      { data: "ITEM5_ROW6_COL2" },
      { data: "ITEM5_ROW6_COL3" },
      { data: "ITEM5_ROW6_COL4" },
      { data: "ITEM6_ROW1_COL2" },
      { data: "ITEM6_ROW1_COL3" },
      { data: "ITEM6_ROW1_COL4" },
      { data: "ITEM6_ROW2_COL2" },
      { data: "ITEM6_ROW2_COL3" },
      { data: "ITEM6_ROW2_COL4" },
      { data: "ITEM6_ROW3_COL2" },
      { data: "ITEM6_ROW3_COL3" },
      { data: "ITEM6_ROW3_COL4" },
      { data: "ITEM6_ROW4_COL2" },
      { data: "ITEM6_ROW4_COL3" },
      { data: "ITEM6_ROW4_COL4" },
      { data: "ITEM6_ROW5_COL2" },
      { data: "ITEM6_ROW5_COL3" },
      { data: "ITEM6_ROW5_COL4" },
      { data: "ITEM6_ROW6_COL2" },
      { data: "ITEM6_ROW6_COL3" },
      { data: "ITEM6_ROW6_COL4" },
      { data: "ITEM7_ROW1_COL2" },
      { data: "ITEM7_ROW1_COL3" },
      { data: "ITEM7_ROW1_COL4" },
      { data: "ITEM7_ROW2_COL2" },
      { data: "ITEM7_ROW2_COL3" },
      { data: "ITEM7_ROW2_COL4" },
      { data: "ITEM7_ROW3_COL2" },
      { data: "ITEM7_ROW3_COL3" },
      { data: "ITEM7_ROW3_COL4" },
      { data: "ITEM7_ROW4_COL2" },
      { data: "ITEM7_ROW4_COL3" },
      { data: "ITEM7_ROW4_COL4" },
      { data: "ITEM7_ROW5_COL2" },
      { data: "ITEM7_ROW5_COL3" },
      { data: "ITEM7_ROW5_COL4" },
      { data: "ITEM7_ROW6_COL2" },
      { data: "ITEM7_ROW6_COL3" },
      { data: "ITEM7_ROW6_COL4" },
      { data: "ITEM8_ROW1_COL2" },
      { data: "ITEM8_ROW1_COL3" },
      { data: "ITEM8_ROW1_COL4" },
      { data: "ITEM8_ROW2_COL2" },
      { data: "ITEM8_ROW2_COL3" },
      { data: "ITEM8_ROW2_COL4" },
      { data: "ITEM8_ROW3_COL2" },
      { data: "ITEM8_ROW3_COL3" },
      { data: "ITEM8_ROW3_COL4" },
      { data: "ITEM8_ROW4_COL2" },
      { data: "ITEM8_ROW4_COL3" },
      { data: "ITEM8_ROW4_COL4" },
      { data: "ITEM8_ROW5_COL2" },
      { data: "ITEM8_ROW5_COL3" },
      { data: "ITEM8_ROW5_COL4" },
      { data: "ITEM8_ROW6_COL2" },
      { data: "ITEM8_ROW6_COL3" },
      { data: "ITEM8_ROW6_COL4" },
    ],
  });
}

////////////////////////////////////////////////////////END OF CREDIT MEETING MINUTES///////////////////////////////////////////////////////////////////////

if (document.querySelector("table[data-id-table=resort_aging_table]")) {
  /*Define only once for entire page*/
  var margin = { left: 80, right: 10, top: 20, bottom: 80 };
  var width = 340 - margin.left - margin.right;
  var height = 330 - margin.top - margin.bottom;
  var barPadding = 0.2;
  var axisTicks = { qty: 5, outerSize: 0, dateFormat: "%m-%d" };
  var t = d3.transition().duration(1000);

  // X Scale
  var x = d3
    .scaleBand()
    .domain(["0 - 30 days", "31 - 60 days", "61 - 90 days", "91 - over"])
    .range([0, width])
    .padding(0.2);

  // Y Scale
  var y = d3.scaleLinear().domain([0, 100]).range([height, 0]);

  /*Define only once for entire page*/
  // console.log("resort_aging_table");
  var $resort_aging_table = $("#resort_aging_table");
  // console.log($resort_aging_table);
  updateResortAging();
}

if (document.querySelector("table[data-id-table=resort_aging_perc_table]")) {
  // console.log("resort_aging_perc_table");
  var $resort_aging_perc_table = $("#resort_aging_perc_table");
  // console.log($resort_aging_perc_table);
  updateResortPercAging();
}

//Update the Resort Aging table and chart
function updateResortAging() {
  $("#resort_aging_table").DataTable().destroy();

  $.get(
    "https://marriottssc.secure-app.co.za/api/resort_aging/" +
      $("#filter_date").val(),
    function (data) {
      ownerData = data;
      console.log(data);
      reportLoading.style.display = "none";

      $("#resort_aging_table").DataTable({
        dom: "Bfrtip",
        buttons: [
          {
            extend: "pdfHtml5",
            orientation: "landscape",
            pageSize: "LEGAL",
            extension: ".pdf",
            filename: "AR Account Owners",
            title: "AR Account Owners",
          },
          "copy",
          {
            extend: "csvHtml5",
            title: "AR Account Owners",
            filename: "AR Account Owners",
          },
          {
            extend: "excelHtml5",
            filename: "AR Account Owners",
            title: "AR Account Owners",
          },
          "print",
        ],
        paging: false,
        select: "single",
        searching: false,
        scrollCollapse: true,
        data: ownerData,
        columns: [
          { data: "RESORT" },
          {
            data: "AGE_BUCKET1",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET2",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET3",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          {
            data: "AGE_BUCKET456",
            render: $.fn.dataTable.render.number(",", ".", 2),
          },
          { data: "TOTAL", render: $.fn.dataTable.render.number(",", ".", 2) },
        ],
      });

      $("#resort_aging_table_filter input").addClass("form-control");
      $("#resort_aging_table_filter input").attr("placeholder", "Search");
    }
  )
    .done(function (data) {
      reportLoading.style.display = "none";
    })
    .fail(function (xhr, status, error) {
      reportLoading.style.display = "none";
    });
}

//Update the Resort Aging Percentage table and chart
function updateResortPercAging() {
  $("#resort_aging_perc_table").DataTable().destroy();

  $.get(
    "https://marriottssc.secure-app.co.za/api/resort_aging_perc/" +
      $("#filter_date").val(),
    function (data) {
      ownerData = data;
      console.log(data);
      reportLoading.style.display = "none";

      $("#resort_aging_perc_table").DataTable({
        dom: "Bfrtip",
        buttons: [
          {
            extend: "pdfHtml5",
            orientation: "landscape",
            pageSize: "LEGAL",
            extension: ".pdf",
            filename: "AR Account Owners",
            title: "AR Account Owners",
          },
          "copy",
          {
            extend: "csvHtml5",
            title: "AR Account Owners",
            filename: "AR Account Owners",
          },
          {
            extend: "excelHtml5",
            filename: "AR Account Owners",
            title: "AR Account Owners",
          },
          "print",
        ],
        paging: false,
        select: "single",
        searching: false,
        scrollCollapse: true,
        data: ownerData,
        columns: [
          { data: "RESORT" },
          {
            data: "AGE_BUCKET1_PERC",
            render: $.fn.dataTable.render.percentage(),
          },
          {
            data: "AGE_BUCKET2_PERC",
            render: $.fn.dataTable.render.percentage(),
          },
          {
            data: "AGE_BUCKET3_PERC",
            render: $.fn.dataTable.render.percentage(),
          },
          {
            data: "AGE_BUCKET456_PERC",
            render: $.fn.dataTable.render.percentage(),
          },
        ],
      });

      $("#resort_aging_perc_table_filter input").addClass("form-control");
      $("#resort_aging_perc_table_filter input").attr("placeholder", "Search");
    }
  )
    .done(function (data) {
      reportLoading.style.display = "none";
      //draw the graph using the data from ajax call
      //draw the graph using the data from ajax call
      $("#resort_aging_charts_row").empty();
      data.forEach((d) => {
        eid_data = pivotData(data, d.RESORT, "RESORT");

        $("#resort_aging_charts_row").append(
          '<div id="' + d.RESORT + '" class="col-sm-4" ></div>'
        );

        /*Define for each owner */

        // Define the div for the tooltip
        var line1_tooltip = d3
          .select("body")
          .append("div")
          .attr("class", "line-tooltip")
          .style("opacity", 0);

        var rect_tooltip = d3
          .select("body")
          .append("div")
          .attr("class", "rect-tooltip")
          .style("opacity", 0);

        var ownerChart = d3
          .select("#" + d.RESORT)
          .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
          .append("g")
          .attr(
            "transform",
            "translate(" + margin.left + ", " + margin.top + ")"
          );

        //X label
        ownerChart
          .append("text")
          .attr("class", "x axis-label")
          .attr("x", width / 2)
          .attr("y", height + 65)
          .attr("font-size", "12px")
          .attr("text-anchor", "middle")
          .text("Aging by User - " + d.RESORT);

        //Y label
        ownerChart
          .append("text")
          .attr("class", "y axis-label")
          .attr("x", -(height / 2))
          .attr("y", -60)
          .attr("font-size", "12px")
          .attr("text-anchor", "middle")
          .attr("transform", "rotate(-90)")
          .text("% Outstanding");

        // X Axis
        var xAxisCall = d3.axisBottom(x);
        var xAxisGroup = ownerChart
          .append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxisCall);

        // Y Axis
        var yAxisCall = d3.axisLeft(y).tickFormat(function (d) {
          return d + "%";
        });
        var yAxisGroup = ownerChart
          .append("g")
          .attr("class", "y axis")
          .call(yAxisCall);

        var elementColor = d3
          .scaleOrdinal()
          .domain([d.RESORT, "Marriott Aging Policy"])
          .range(["#63abf2", "red"]);

        var keys = [d.RESORT, "Marriott Aging Policy"];

        var legend = ownerChart
          .append("g")
          .attr("transform", "translate(-60" + "," + (height + 25) + ")");

        // Add one dot in the legend for each name.
        var size = 10;
        legend
          .selectAll("mydots")
          .data(keys)
          .enter()
          .append("rect")
          .attr("x", 0)
          .attr("y", function (d, i) {
            return 0 + i * (size + 5);
          }) // 100 is where the first dot appears. 25 is the distance between dots
          .attr("width", size)
          .attr("height", size)
          .style("fill", function (d) {
            return elementColor(d);
          });

        legend
          .selectAll("legend")
          .data(keys)
          .enter()
          .append("text")
          .attr("x", 0 + size * 1.2)
          .attr("y", function (d, i) {
            return 0 + i * (size + 5) + size / 2;
          }) // 100 is where the first dot appears. 25 is the distance between dots
          .style("fill", function (d) {
            return elementColor(d);
          })
          .text(function (d) {
            return d;
          })
          .attr("text-anchor", "left")
          .style("alignment-baseline", "middle");

        /*Define for each owner */

        eid_data.forEach((d) => {
          d.PERC = +d.PERC;
        });

        updateResortAgingChart(
          eid_data,
          ownerChart,
          line1_tooltip,
          rect_tooltip
        );
      });
    })
    .fail(function (xhr, status, error) {
      reportLoading.style.display = "none";
    });
}

function updateResortAgingChart(data, element, line1_tooltip, rect_tooltip) {
  // Bars
  var rects = element.selectAll(".bar").data(data, function (d) {
    return d.LABEL;
  });

  //draw the rectangles of the bar graph
  rects
    .enter()
    .append("rect")
    .attr("class", "bar")
    .attr("y", function (d) {
      return y(d.PERC);
    })
    .attr("x", function (d) {
      return x(d.LABEL);
    })
    .attr("height", function (d) {
      return height - y(d.PERC);
    })
    .attr("width", x.bandwidth)
    .attr("fill", "#63abf2")
    .on("mouseover", function (d) {
      rect_tooltip.transition().duration(200).style("opacity", 0.9);
      rect_tooltip
        .html(d.LABEL + "<br/>" + d.PERC + "%")
        .style("left", d3.event.pageX + "px")
        .style("top", d3.event.pageY + "px");
    })
    .on("mouseout", function (d) {
      rect_tooltip.transition().duration(500).style("opacity", 0);
    });

  //ajax call to obtain limits for line graph
  $.get(
    "https://marriottssc.secure-app.co.za/api/eid_aging_limits",
    function (data) {}
  )
    .done(function (data) {
      //if successfull, draw the line
      data.forEach((d) => {
        d.PERC = +d.PERC;
      });

      var line = d3
        .line()
        .x(function (d) {
          return x(d.AGING) + x.bandwidth() / 2;
        })
        .y(function (d) {
          return y(d.PERC);
        });

      element
        .append("path")
        .attr("class", "line")
        .attr("fill", "none")
        .attr("stroke", "red")
        .attr("stroke-with", "3px")
        .attr("d", line(data));

      // Add the scatterplot
      element
        .selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("id", function (d) {
          return d.AGING;
        })
        .attr("r", 3)
        .attr("cx", function (d) {
          return x(d.AGING) + x.bandwidth() / 2;
        })
        .attr("cy", function (d) {
          return y(d.PERC);
        })
        .attr("fill", "red")
        .on("mouseover", function (d) {
          line1_tooltip.transition().duration(200).style("opacity", 0.9);
          line1_tooltip
            .html(d.AGING + "<br/>" + d.PERC + "%")
            .style("left", d3.event.pageX + 5 + "px")
            .style("top", d3.event.pageY + 5 + "px");
        })
        .on("mouseout", function (d) {
          line1_tooltip.transition().duration(500).style("opacity", 0);
        });
    })
    .fail(function (result) {
      console.log(result);
    });
}

/**
 * Build summary table below main table.
 * Initially the totalArray will be empty
 * until the main table columns totals
 * have been assigned to it.
 */
var totalsArray = [];
$("#summary_table").DataTable({
  searching: false,
  paging: false,
  bInfo: false,
  dom: "Bfrtip",
  buttons: [
    {
      extend: "pdfHtml5",
      orientation: "landscape",
      pageSize: "LEGAL",
      extension: ".pdf",
      filename: "AR Account Owners",
      title: "AR Account Owners",
    },
    "copy",
    {
      extend: "csvHtml5",
      title: "AR Account Owners",
      filename: "AR Account Owners",
    },
    {
      extend: "excelHtml5",
      filename: "AR Account Owners",
      title: "AR Account Owners",
    },
    "print",
  ],
  columns: [
    {
      name: "title",
      title: "",
    },
    {
      name: "Current",
      title: "Current",
    },
    {
      title: "31 to 60 Days",
    },
    {
      title: "61 to 90 Days",
    },
    {
      title: "Over 90 Days",
    },
    {
      title: "Balance",
    },
  ],
  data: totalsArray,
  rowsGroup: ["title:name"],
});

// $('#filter_resort_published').change(function() {
//   console.log("Aging Template resort changed.");
//   $('#aging_template').DataTable().clear()
//     .draw();
//   getSnapshotDates();
// });

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//PERFORMANCE DASHBOARD END//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
