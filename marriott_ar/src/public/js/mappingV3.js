 ///////////////start of mapping///////////////

var cellValue = '';
var cellIndex = '';

function clearMappingSearch() {
  $mappingV3DataTable.DataTable()
 .search( '' )
 .columns().search( '' )
 .draw();
}


if (document.querySelector("table[data-id-table=mappingV3DataTable]")) {



if ($('#access_allowed').text() == 'N') {
  $('#ACCOUNT_NAME').attr('readonly', true);
  $('#TO_ACCOUNT').attr('readonly', true);
   $('#ACCOUNT_TYPE').prop('disabled', 'disabled');
  $('#addMappingV3Btn').attr('disabled', true);
  $('#mappingV3DeleteModalBtn').attr('disabled', true);
  $('#mappingV3ChangeModalBtn').attr('disabled', true);
}

console.log("mappingV3DataTable");

var $mappingV3DataTable = $('#mappingV3DataTable')
console.log($mappingV3DataTable);
var comments = '';


$.get( "https://marriottssc.secure-app.co.za/api/mappingV3",
function(data) {
  mappingData = data;
  console.log(data);
  $mappingV3DataTable.bootstrapTable('destroy')
  // $testTable.bootstrapTable({data: mappingData})
  reportLoading.style.display = 'none';

  $('#mappingV3DataTable').DataTable( {
    "orderCellsTop": true,
    fixedHeader: true,
    dom: 'Bfrtip',
    buttons: [
      {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                extension: '.pdf',
                filename: 'AR Account Mapping',
                title: 'AR Account Mapping'
            },
            'copy',
            {
          extend: 'csvHtml5',
          title: 'AR Account Mapping',
          filename: 'AR Account Mapping'
      },
      {
          extend: 'excelHtml5',
          filename: 'AR Account Mapping',
          title: 'AR Account Mapping'
      },
              'print'
        ],
    "paging": false,
    select: 'single',
    "scrollY": "550px",
    scrollCollapse: true,
    "pageLength": 50,
       data: mappingData,
       "columns": [
    { "data": "RESORT",orderData: [ 0,1 ] },
    { "data": "ACCOUNT_NAME" },
    { "data": "ACCOUNT_TYPE" , "visible": false },
    { "data": "ACCOUNT_TYPE_DESC" },
    { "data": "ACCOUNT_NO_FROM" },
    { "data": "ACCOUNT_NO_TO" },
    { "data": "ACCOUNT_NAME" },
    { "data": "CONTACT" },
     { "data": "PAYMENT_METHOD" },
     { "data": "EID" },
    { "data": "INSERT_DATE" },
    { "data": "INSERT_USER" },
    { "data": "UPDATE_DATE" },
    { "data": "UPDATE_USER" },
    { "data": "MAPPED" },
    { "data": "REQUIRED_DOCS" },

  ],

    "rowCallback": function( row, data, index ) {
    if ( data.MAPPED == "UNMAPPED" || data.MAPPED == null ) {
      // console.log(row);
      var cols = $('#mappingV3DataTable').DataTable().columns()[0].length;
      console.log(cols);
      for (var i = 0; i < cols; i++) {
        $('td:eq('+ i +')', row).addClass('stale');
      }
      
    }
  }



    } );

    $("#mappingV3DataTable_filter input").addClass( "form-control" );
$("#mappingV3DataTable_filter input").attr("placeholder","Search");



}
)
.done(function(msg) {
          reportLoading.style.display = 'none';


$('#mappingV3DataTable tbody').on( 'click', 'td', function (e) {

var x = document.querySelectorAll(".filter");
for (i = 0; i < x.length; i++) {
console.log(x[i].classList.remove('filter'));

};

var cell = e.target.innerHTML;
console.log(e.target.classList.add('filter'));
cellValue = cell;
console.log(cell);

   var visIdx = $(this).index();
   var dataIdx = $('#mappingV3DataTable').DataTable().column.index( 'fromVisible', visIdx );
   cellIndex = dataIdx;
console.log('Column data index: '+dataIdx);

} );


$('#filterBtn').on('click',function(e){
  console.log("Filter column: " + cellIndex + " by value: " + cellValue );
  $mappingV3DataTable.DataTable()
 .search( '' )
 .columns(cellIndex).search( cellValue )
 .draw();
     
    });

    $('#filterClearBtn').on('click',function(e){
  clearMappingSearch();
    });

    
        })
.fail(function(xhr, status, error) {
          reportLoading.style.display = 'none';
        })



}

$('#mappingV3DataTable').on('dblclick','tr',function(e){
  $('#mappingV3DataTable').DataTable().row(this).select();
   var $element = $('#mappingV3DataTable').DataTable().row( this ).data() ;
      mapAccountFrom = $element.ACCOUNT_NO_FROM ;
      mapAccountTo = $element.ACCOUNT_NO_TO ;
      mapResort = $element.RESORT ;
      mapAccountName = $element.ACCOUNT_NAME ;
      mapAccountContact = $element.CONTACT ;
      mapAccountPayment = $element.PAYMENT_METHOD ;
      mapAccountEID = $element.EID ;
      mapAccountType = $element.ACCOUNT_TYPE ;
      mapRequiredDocs = $element.REQUIRED_DOCS ;
      console.log(mapAccountFrom + " to " +mapAccountTo );
      $('#mappingV3ChangeModal').modal('show');
    })


    $('#mappingV3ChangeModal').on('show.bs.modal', function (event) {
      var modal = $(this)
      // modal.find('.modal-body p').html("Account: " + commentAccountNo + "<br>Resort: " + commentResort)
      $("#RESORT").val(mapResort);
      $("#ACCOUNT_TYPE").val(mapAccountType);
      $("#FROM_ACCOUNT").val(mapAccountFrom);
      $("#TO_ACCOUNT").val(mapAccountTo);
      $("#ACCOUNT_NAME").val(mapAccountName);
      $("#CONTACT").val(mapAccountContact);
      $("#PAYMENT").val(mapAccountPayment);
      $("#EID").val(mapAccountEID);
      $("#REQUIRED_DOCS").val(mapRequiredDocs);


    })



  $("#addMappingV3Btn").on('click',function() {
    console.log("add new record");
    $('#mappingV3AddModal').modal('show');
  });



      $('#mappingV3AddModalBtn').click(function () {
        var formArray = $("#mappingV3AddModalForm").serializeArray();
        console.log(formArray);
        if ($("#RESORT_NEW").val().length > 0 &&  $("#FROM_ACCOUNT_NEW").val().length > 0 && $("#TO_ACCOUNT_NEW").val().length > 0 &&  $("#ACCOUNT_NAME_NEW").val().length > 0 &&  $("#CONTACT_NEW").val().length > 0 ) {
         console.log("Add mapping");
          $.post( "https://marriottssc.secure-app.co.za/addMappingV3",
          $("#mappingV3AddModalForm").serializeArray() ,
          function(data) {
            console.log(data);
          }
          )
          .done(function( msg ) {
            console.log(msg);
            location.reload();
          })
          .fail(function(error) {
            alert("Comments could not be added. Please contact administrator" + error);
          });
        } else {
          alert("Please complete all the required fields!");
        }

      //  location.reload();

        });


          $('#mappingV3ChangeModalBtn').click(function () {
      var formArray = $("#mappingV3ChangeModalForm").serializeArray();
      console.log(formArray);
      if ($("#RESORT").val().length > 0 &&  $("#FROM_ACCOUNT").val().length > 0 && $("#TO_ACCOUNT").val().length > 0 &&  $("#ACCOUNT_NAME").val().length > 0 ) {
       console.log("Update mapping");
        $.post( "https://marriottssc.secure-app.co.za/updateMappingV3",
        $("#mappingV3ChangeModalForm").serializeArray() ,
        function(data) {
          console.log(data);
        }
        )
        .done(function( msg ) {
          console.log(msg);
          location.reload();
        })
        .fail(function(error) {
          alert("Mapping could not be added. Check if there is not already a mapping for this hotel and account number. If you are trying to amend an existing mapping records, double on the record in the table to open the edit window. ");
          console.log(error);
        });
      } else {
        alert("Please complete all the required fields!");
      }

    //  location.reload();

      });



          $('#mappingV3DeleteModalBtn').click(function () {
        $('#deleteMappingV3Modal').modal('show');
        });

          $('#deleteMappingV3ConfirmBtn').click(function () {
          var formArray = $("#mappingV3ChangeModalForm").serializeArray();
          console.log(formArray);
           console.log("Delete mapping");
            $.post( "https://marriottssc.secure-app.co.za/deleteMapping",
            $("#mappingV3ChangeModalForm").serializeArray() ,
            function(data) {
              console.log(data);
            }
            )
            .done(function( msg ) {
              console.log(msg);
              location.reload();
            })
            .fail(function(error) {
              alert("Comments could not be added. Please contact administrator" + error);
            });
          });









          ///////////////end of mapping///////////////