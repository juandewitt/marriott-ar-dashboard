 ///////////////start of mapping///////////////

var cellValue = '';
var cellIndex = '';

function clearMappingSearch() {
  $mappingV4DataTable.DataTable()
 .search( '' )
 .columns().search( '' )
 .draw();
}


if (document.querySelector("table[data-id-table=mappingV4DataTable]")) {



if ($('#access_allowed').text() == 'N') {
  $('#ACCOUNT_NAME').attr('readonly', true);
  $('#TO_ACCOUNT').attr('readonly', true);
   $('#ACCOUNT_TYPE').prop('disabled', 'disabled');
  $('#addMappingV4Btn').attr('disabled', true);
  $('#mappingV4DeleteModalBtn').attr('disabled', true);
  // $('#mappingV4ChangeModalBtn').attr('disabled', true);
}

console.log("mappingV4DataTable");

var $mappingV4DataTable = $('#mappingV4DataTable')
console.log($mappingV4DataTable);
var comments = '';


$.get( "https://marriottssc.secure-app.co.za/api/mappingV4",
function(data) {
  mappingData = data;
  console.log(data);
  $mappingV4DataTable.bootstrapTable('destroy')
  // $testTable.bootstrapTable({data: mappingData})
  reportLoading.style.display = 'none';

  $('#mappingV4DataTable').DataTable( {
    "orderCellsTop": true,
    fixedHeader: true,
    dom: 'Bfrtip',
    buttons: [
      {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                extension: '.pdf',
                filename: 'AR Account Mapping',
                title: 'AR Account Mapping'
            },
            'copy',
            {
          extend: 'csvHtml5',
          title: 'AR Account Mapping',
          filename: 'AR Account Mapping'
      },
      {
          extend: 'excelHtml5',
          filename: 'AR Account Mapping',
          title: 'AR Account Mapping'
      },
              'print'
        ],
    "paging": false,
    select: 'single',
           "scrollY": "550px",
       "scrollX": "2500px",
    scrollCollapse: true,
    "pageLength": 50,
       data: mappingData,
       "columns": [
    { "data": "RESORT",orderData: [ 0,1 ] },
    { "data": "ACCOUNT_NAME" },
    { "data": "ACCOUNT_TYPE" , "visible": false },
    { "data": "ACCOUNT_TYPE_DESC" },
    { "data": "ACCOUNT_NO_FROM" },
    { "data": "ACCOUNT_NO_TO" },
    { "data": "ACCOUNT_NAME" },
    { "data": "CONTACT" },
     { "data": "PAYMENT_METHOD" },
     { "data": "EID" },
    { "data": "INSERT_DATE" },
    { "data": "INSERT_USER" },
    { "data": "UPDATE_DATE" },
    { "data": "UPDATE_USER" },
    { "data": "MAPPED" },
    { "data": "REQUIRED_DOCS" },
    { "data": "DOCS_UPLOADED" },

  ],

    "rowCallback": function( row, data, index ) {
    if ( data.MAPPED == "UNMAPPED" || data.MAPPED == null ) {
      // console.log(row);
      var cols = $('#mappingV4DataTable').DataTable().columns()[0].length;
      console.log(cols);
      for (var i = 0; i < cols; i++) {
        $('td:eq('+ i +')', row).addClass('stale');
      }
      
    }
  }



    } );

    $("#mappingV4DataTable_filter input").addClass( "form-control" );
$("#mappingV4DataTable_filter input").attr("placeholder","Search");



}
)
.done(function(msg) {
          reportLoading.style.display = 'none';


$('#mappingV4DataTable tbody').on( 'click', 'td', function (e) {

var x = document.querySelectorAll(".filter");
for (i = 0; i < x.length; i++) {
console.log(x[i].classList.remove('filter'));

};

var cell = e.target.innerHTML;
console.log(e.target.classList.add('filter'));
cellValue = cell;
console.log(cell);

   var visIdx = $(this).index();
   var dataIdx = $('#mappingV4DataTable').DataTable().column.index( 'fromVisible', visIdx );
   cellIndex = dataIdx;
console.log('Column data index: '+dataIdx);

} );


$('#filterBtn').on('click',function(e){
  console.log("Filter column: " + cellIndex + " by value: " + cellValue );
  $mappingV4DataTable.DataTable()
 .search( '' )
 .columns(cellIndex).search( cellValue )
 .draw();
     
    });

    $('#filterClearBtn').on('click',function(e){
  clearMappingSearch();
    });

    
        })
.fail(function(xhr, status, error) {
          reportLoading.style.display = 'none';
        })



}

$('#mappingV4DataTable').on('dblclick','tr',function(e){
  $('#mappingV4DataTable').DataTable().row(this).select();
   var $element = $('#mappingV4DataTable').DataTable().row( this ).data() ;
      mapAccountFrom = $element.ACCOUNT_NO_FROM ;
      mapAccountTo = $element.ACCOUNT_NO_TO ;
      mapResort = $element.RESORT ;
      mapAccountName = $element.ACCOUNT_NAME ;
      mapAccountContact = $element.CONTACT ;
      mapAccountPayment = $element.PAYMENT_METHOD ;
      mapAccountEID = $element.EID ;
      mapAccountType = $element.ACCOUNT_TYPE ;
      mapRequiredDocs = $element.REQUIRED_DOCS ;
      console.log(mapAccountFrom + " to " +mapAccountTo );
      $('#mappingV4ChangeModal').modal('show');
    })


    $('#mappingV4ChangeModal').on('show.bs.modal', function (event) {
      var modal = $(this)
      // modal.find('.modal-body p').html("Account: " + commentAccountNo + "<br>Resort: " + commentResort)
      $("#RESORT").val(mapResort);
      $("#ACCOUNT_TYPE").val(mapAccountType);
      $("#FROM_ACCOUNT").val(mapAccountFrom);
      $("#TO_ACCOUNT").val(mapAccountTo);
      $("#ACCOUNT_NAME").val(mapAccountName);
      $("#CONTACT").val(mapAccountContact);
      $("#PAYMENT").val(mapAccountPayment);
      $("#EID").val(mapAccountEID);
      $("#REQUIRED_DOCS").val(mapRequiredDocs);


    })



  $("#addMappingV4Btn").on('click',function() {
    console.log("add new record");
    $('#mappingV4AddModal').modal('show');
  });



      $('#mappingV4AddModalBtn').click(function () {
        var form = document.getElementById('mappingV4AddModalForm');
        var formData = new FormData(form);
        // var formArray = $("#mappingV4AddModalForm").serializeArray();
        console.log(formArray);
        if ($("#RESORT_NEW").val().length > 0 
          &&  $("#FROM_ACCOUNT_NEW").val().length > 0 
          && $("#TO_ACCOUNT_NEW").val().length > 0 
          &&  $("#ACCOUNT_NAME_NEW").val().length > 0 
          &&  $("#CONTACT_NEW").val().length > 0 ) {
         console.log("Add mapping");

          $.ajax({
            type: "post",
            url: "https://marriottssc.secure-app.co.za/addMappingV4",
            processData: false,
            contentType: false,
            dataType: "json", // and this
            data: formData
          })
            .done(function( msg ) {
              console.log( msg );
              location.reload();
            })
            .fail(function(result) {
              console.log(result);
            })
            ;
        } else {
          alert("Please complete all the required fields!");
        }

       // location.reload();

        });


          $('#mappingV4ChangeModalBtn').click(function () {
      // var formArray = $("#mappingV4ChangeModalForm").serializeArray();
        var form = document.getElementById('mappingV4ChangeModalForm');
        var formData = new FormData(form);
      // console.log(formArray);
      if ($("#RESORT").val().length > 0 
        &&  $("#FROM_ACCOUNT").val().length > 0 
        && $("#TO_ACCOUNT").val().length > 0 
        &&  $("#ACCOUNT_NAME").val().length > 0 ) {
       console.log("Update mapping");
        // $.post( "https://marriottssc.secure-app.co.za/updateMappingV4",
        // $("#mappingV4ChangeModalForm").serializeArray() ,
        // function(data) {
        //   console.log(data);
        // }
        // )
        // .done(function( msg ) {
        //   console.log(msg);
        //   location.reload();
        // })
        // .fail(function(error) {
        //   alert("Mapping could not be added. Check if there is not already a mapping for this hotel and account number. If you are trying to amend an existing mapping records, double on the record in the table to open the edit window. ");
        //   console.log(error);
        // });
        $.ajax({
            type: "post",
            url: "https://marriottssc.secure-app.co.za/updateMappingV4",
            processData: false,
            contentType: false,
            dataType: "json", // and this
            data: formData
          })
            .done(function( msg ) {
              console.log( msg );
              location.reload();
            })
            .fail(function(result) {
              console.log(result);
            })
            ;
      } else {
        alert("Please complete all the required fields!");
      }

    //  location.reload();

      });



          $('#mappingV4DeleteModalBtn').click(function () {
        $('#deleteMappingV4Modal').modal('show');
        });

          $('#deleteMappingV4ConfirmBtn').click(function () {
          var formArray = $("#mappingV4ChangeModalForm").serializeArray();
          console.log(formArray);
           console.log("Delete mapping");
            $.post( "https://marriottssc.secure-app.co.za/deleteMapping",
            $("#mappingV4ChangeModalForm").serializeArray() ,
            function(data) {
              console.log(data);
            }
            )
            .done(function( msg ) {
              console.log(msg);
              location.reload();
            })
            .fail(function(error) {
              alert("Comments could not be added. Please contact administrator" + error);
            });
          });









          ///////////////end of mapping///////////////