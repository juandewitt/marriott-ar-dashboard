// Cookies.set($('#session_user').text());


// var defaultEID = $('#session_user').text();
//  $("#selectEID option[value='" + defaultEID +"']").attr("selected", "selected");
commentsArray = [];
 if (Cookies.get('eid')) {
      var defaultEID =Cookies.get('eid');
      $("#selectEID option[value='" + Cookies.get('eid') +"']").attr("selected", "selected");
    } else {
      $("#selectEID option[value='" + $('#session_user') +"']").attr("selected", "selected");
      var defaultEID = $('#selectEID').find(":selected").val();
    }


    level4testAjax(defaultEID);
 


  $('#selectEID').change(function() {
  reportLoading.style.display = 'block';
  console.log($(this).val());
  Cookies.set('eid', $(this).val(), { expires: 30 });
  $("#level4test").DataTable().destroy();
  level4testAjax($(this).val());
  });

    // var table = $('#level4test').DataTable();

    $('#level4test tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );
 
    $('#addCommentsBtn').click( function () {
      var accountStr = '';
      var resortStr = '';
      var invoice_noStr = '';
      var thisRow = [];
        console.log( $('#level4test').DataTable().rows('.selected').data()  );
        selectedRows = $('#level4test').DataTable().rows('.selected').data();
        numberOfRows = $('#level4test').DataTable().rows('.selected').data().length;
        selectedRows.map(function ($element, index) {
      
      thisRow['ACCOUNT_NO'] = $element.ACCOUNT_NO ;
      accountStr = accountStr  +  $element.ACCOUNT_NO  + ", ";
      thisRow['RESORT'] = $element.RESORT ;
      resortStr = resortStr  +  $element.RESORT  + ", ";
      thisRow['INVOICE_NO'] = $element.INVOICE_NO ;
      invoice_noStr = invoice_noStr  +  $element.INVOICE_NO + ", " ;

      commentsArray.push(thisRow);

        });

      commentAccountNo = accountStr ;
      commentResort = resortStr ;
      commentInvoice = invoice_noStr ;

      commentLevel = 4;
      
        $('#multiCommentModal').modal('show');

    } );

var csrfTokens = null;
        $('#multiCommentModal').on('show.bs.modal', function (event) {
      var modal = $(this)
      modal.find('.modal-title').text('Add Comments')
      // modal.find('.modal-body p').html("Account: " + commentAccountNo + "<br>Resort: " + commentResort)
      // $("#MODAL_ACCOUNT_LIST").text(commentAccountNo);
      // $("#MODAL_RESORT_LIST").text(commentResort);
      $("#MODAL_INVOICE_LIST").text(commentInvoice);
      // $("#MODAL_LEVEL").val(commentLevel);
    });



    $('#multiCommentModalBtn').click(function () {

                      selectedRows.map(function ($element, index) {
                        
                          // console.log(index);
                          $("#MULTI_ACCOUNT_NO").val($element.ACCOUNT_NO);
                          $("#MULTI_RESORT").val($element.RESORT);
                          $("#MULTI_INVOICE_NO").val($element.INVOICE_NO);
                          $("#MULTI_MODAL_LEVEL").val(4);


                          var formData = $("#multiCommentModalForm").serializeArray();
                          console.log(formData);


                        if ($element.ACCOUNT_NO && $element.RESORT && $element.INVOICE_NO ) {
                          console.log("post comment for invoice " + $element.INVOICE_NO);
                        $.post( "https://marriottssc.secure-app.co.za/commentAdd",  formData ,
                        function(data) {
                          console.log(data);
                        }
                        )
                        .done(function( msg ) {
                          console.log("Comments added succesfully for " + $element.INVOICE_NO);
                          
                        })
                        .fail(function(error) {
                          console.log(error);
                          // alert("Comments could not be added. Please contact administrator" + error);
                        });
                      } else {
                        alert("Cannot add comments for " + $element.ACCOUNT_NO + " and hotel " + $element.RESORT +  " to this level without an invoice number."  );
                      }


                          });

                      location.reload();

    });

function level4testAjax(eid = null) {

  // if (document.querySelector("table[data-id-table=level4test]")) {
    if (1===1) { 


    // if ( $.fn.DataTable.isDataTable('#level4test') ) {
    //   $('#level4test').DataTable().destroy();
    // }
    // $('#level4test tbody').empty();


   var $testTable = $('#level4test')


   // console.log($testTable);
   var comments = '';

   eidStr = "";
   if (eid) {
     eidStr = "/" + eid;
   }

   $.get( "https://marriottssc.secure-app.co.za/api/level4/ownerinvoices" +  eidStr,
   function(data) {
     level4testData = data;
     // console.log(data);
     // $testTable.bootstrapTable('destroy')
     // $testTable.bootstrapTable({data: level4testData})
     reportLoading.style.display = 'none';

     $('#level4test').DataTable( {
      dom: 'Bfrtip',
    buttons: [
      {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize : 'LEGAL',
                extension: '.pdf',
                filename: 'Aging Detail by Account and Hotel',
                title: 'Aging Detail by Account and Hotel',
                customize: function (doc) {
                  doc.styles.tableHeader.fontSize = 4;
                  doc.defaultStyle.fontSize = 4;
              }
            },
            'copy',
            {
          extend: 'csvHtml5',
          filename: 'Aging Detail by Account and Hotel',
          title: 'Aging Detail by Account and Hotel'
      },
            {
          extend: 'excelHtml5',
          filename: 'Aging Detail by Account and Hotel',
          title: 'Aging Detail by Account and Hotel'
      },  'print'
        ],
       "paging": false,
       select: 'single',
       "scrollY": "550px",
       "scrollX": "2500px",
       scrollCollapse: true,
       "pageLength": 50,
          data: level4testData,
        select: {
            style: 'os'
        },
          "columns": [
                      
       { "data": "ACCOUNT_TYPE" },
       { "data": "ACCOUNT_NAME" },
       { "data": "ACCOUNT_NO" },
       { "data": "EID" },
       { "data": "CREDIT_LIMIT", render: $.fn.dataTable.render.number( ',', '.', 2 ) },
       { "data": "RESORT" },
       { "data": "RESORT_NAME" },
       { "data": "INVOICE_NO" },
       { "data": "FOLIO_NO" },
       { "data": "POST_DATE" },
       { "data": "AGE_BUCKET1" , render: $.fn.dataTable.render.number( ',', '.', 2 )},
      { "data": "AGE_BUCKET2" , render: $.fn.dataTable.render.number( ',', '.', 2 )},
      { "data": "AGE_BUCKET3" , render: $.fn.dataTable.render.number( ',', '.', 2 )},
      { "data": "AGE_BUCKET4" , render: $.fn.dataTable.render.number( ',', '.', 2 )},
      { "data": "AGE_BUCKET5" , render: $.fn.dataTable.render.number( ',', '.', 2 )},
      { "data": "AGE_BUCKET6" , render: $.fn.dataTable.render.number( ',', '.', 2 )},
      { "data": "TOTAL" , render: $.fn.dataTable.render.number( ',', '.', 2 )},
      { "data": "INVOICE_AGE" },
       { "data": "OWNER" },
       { "data": "DISPATCH_DATE" },
       { "data": "RC" },
       { "data": "RC_ID_DATE" },
       { "data": "EMAIL_CALL_DATE" },
       { "data": "L1" },
       { "data": "L2" },
       { "data": "L3" },
       { "data": "CC" },
       { "data": "CC_ID_DATE" }
     ],
     
           orderFixed: [[ 2, 'asc' ], [ 5, 'asc' ]],

           "footerCallback": function ( row, data, start, end, display ) {
               var api = this.api(), data;

               // Remove the formatting to get integer data for summation
               var intVal = function ( i ) {
                   return typeof i === 'string' ?
                       i.replace(/[\$,]/g, '')*1 :
                       typeof i === 'number' ?
                           i : 0;
               };
               age1 = api
                   .column(10 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
               age2 = api
                   .column(11 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
               age3 = api
                   .column(12 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
               age4 = api
                   .column(13 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
               age5 = api
                   .column(14 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
                   age6 = api
                   .column(15 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );
               // Total over all pages
               total = api
                   .column(16 )
                   .data()
                   .reduce( function (a, b) {
                       return intVal(a) + intVal(b);
                   }, 0 );

               // // Total over this page
               // pageTotal = api
               //     .column( 14, { page: 'current'} )
               //     .data()
               //     .reduce( function (a, b) {
               //         return intVal(a) + intVal(b);
               //     }, 0 );
               age1 = $.fn.dataTable.render.number(',', '.', 2).display( age1 );
               age2 = $.fn.dataTable.render.number(',', '.', 2).display( age2 );
               age3 = $.fn.dataTable.render.number(',', '.', 2).display( age3 );
               age4 = $.fn.dataTable.render.number(',', '.', 2).display( age4 );
               age5 = $.fn.dataTable.render.number(',', '.', 2).display( age5 );
               age6 = $.fn.dataTable.render.number(',', '.', 2).display( age6 );
               total = $.fn.dataTable.render.number(',', '.', 2).display( total );
               // Update footer
               $( api.column(10 ).footer() ).html(
                 age1
               );
               $( api.column(11 ).footer() ).html(
                 age2
               );
               $( api.column(12 ).footer() ).html(
                 age3
               );
               $( api.column(13 ).footer() ).html(
                 age4
               );
               $( api.column(14 ).footer() ).html(
                 age5
               );
               $( api.column(15 ).footer() ).html(
                 age6
               );
               $( api.column(16 ).footer() ).html(
                   total
               );
           },

          // orderFixed: [2,5, 'desc'],


           rowGroup: {
               endRender: null,
               startRender: function ( rows, group ) {
               	groupDesc = '';
               	var account_names = rows
                    .data()
                    .pluck('ACCOUNT_NAME')
            	var account_name = account_names[0];

            	var resort_names = rows
                    .data()
                    .pluck('RESORT_NAME')
            	var resort_name = resort_names[0];

            	var accounts = rows
                    .data()
                    .pluck('ACCOUNT_NO')
            	var account = accounts[0];

            	var resorts = rows
                    .data()
                    .pluck('RESORT')
            	var resort = resorts[0];

            	if (group == resort) {
            		groupDesc = resort_name;
            	} else {
            		groupDesc = account_name;
            	}



                   var age1 = rows
                       .data()
                       .pluck('AGE_BUCKET1')
                       .reduce( function (a, b) {
                           return a + b*1;
                       }, 0) ;
                       age1 = $.fn.dataTable.render.number(',', '.', 2).display( age1 );

                       var age2 = rows
                       .data()
                       .pluck('AGE_BUCKET2')
                       .reduce( function (a, b) {
                           return a + b*1;
                       }, 0) ;
                       age2 = $.fn.dataTable.render.number(',', '.', 2).display( age2 );

                       var age3 = rows
                       .data()
                       .pluck('AGE_BUCKET3')
                       .reduce( function (a, b) {
                           return a + b*1;
                       }, 0) ;
                       age3 = $.fn.dataTable.render.number(',', '.', 2).display( age3 );

                       var age4 = rows
                       .data()
                       .pluck('AGE_BUCKET4')
                       .reduce( function (a, b) {
                           return a + b*1;
                       }, 0) ;
                       age4 = $.fn.dataTable.render.number(',', '.', 2).display( age4 );

                       var age5 = rows
                       .data()
                       .pluck('AGE_BUCKET5')
                       .reduce( function (a, b) {
                           return a + b*1;
                       }, 0) ;
                       age5 = $.fn.dataTable.render.number(',', '.', 2).display( age5 );

                       var age6 = rows
                       .data()
                       .pluck('AGE_BUCKET6')
                       .reduce( function (a, b) {
                           return a + b*1;
                       }, 0) ;
                       age6 = $.fn.dataTable.render.number(',', '.', 2).display( age6 );

                       var total = rows
                       .data()
                       .pluck('TOTAL')
                       .reduce( function (a, b) {
                           return a + b*1;
                       }, 0) ;
                       total = $.fn.dataTable.render.number(',', '.', 2).display( total );




                   return $('<tr/>')
                    .append( '<td id="groupByDesc" colspan="10">' + groupDesc + '  -	 ' + group +'</td>' )
                    .append( '<td>'+age1+'</td>' )
                    .append( '<td>'+age2+'</td>' )
                    .append( '<td>'+age3+'</td>' )
                    .append( '<td>'+age4+'</td>' )
                    .append( '<td>'+age5+'</td>' )
                    .append( '<td>'+age6+'</td>' )

                    .append( '<td>'+total+'</td>' )
                    .append( '<td colspan="11"></td>' );
               },
               dataSrc: ["ACCOUNT_NO","RESORT"]
           }
       } );

       $("#level4test_filter input").addClass( "form-control" );
   $("#level4test_filter input").attr("placeholder","Search");







   }
   )
   .done(function(msg) {
             // alert( msg );
             reportLoading.style.display = 'none';
           })
           .fail(function(xhr, status, error) {
             // alert(error);
             reportLoading.style.display = 'none';
           })
           .always(function() {
             // alert( "finished" );
             reportLoading.style.display = 'none';
           });




}


   }



   $('#level4test').on('dblclick','tr',function(e){

  $('#level4test').DataTable().row(this).select();
   var $element = $('#level4test').DataTable().row( this ).data() ;
   
   if ($element) {
      commentAccountNo = $element.ACCOUNT_NO ;
      commentResort = $element.RESORT ;
      commentInvoice = $element.INVOICE_NO ;
      if ($element.INVOICE_NO) {
        console.log("There is an invoice no");
        commentLevel = 4;
      } else {
        if ($element.RESORT) {
          commentLevel = 3;
          $("#MODAL_INVOICE_NO").prop('disabled', true);
        } else {
          commentLevel = 2;
          $("#MODAL_INVOICE_NO").prop('disabled', true);
          $("#MODAL_RESORT").prop('disabled', true);
        }
        console.log("No invoice no");
      }
      console.log($element.INVOICE_NO );
      $('#comment3Modal').modal('show');
   } else {
    console.log(e.currentTarget.childNodes[0].innerHTML);
   }

      
    })






  
          
    
                 
   
