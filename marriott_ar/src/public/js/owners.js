

//Owners
if (document.querySelector("table[data-id-table=ownersTable]")) {
console.log("ownersTable");

var $ownersTable = $('#ownersTable')
console.log($ownersTable);
var comments = '';



$.get( "https://marriottssc.secure-app.co.za/api/owners",
function(data) {
  ownerData = data;
  console.log(data);
  // $ownersTable.bootstrapTable('destroy')
  // $testTable.bootstrapTable({data: ownerData})
  reportLoading.style.display = 'none';

  $('#ownersTable').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                extension: '.pdf',
                filename: 'AR Account Mapping',
                title: 'AR Account Mapping'
            },
            'copy',
            {
          extend: 'csvHtml5',
          title: 'AR Account Mapping',
          filename: 'AR Account Mapping'
      },
      {
          extend: 'excelHtml5',
          filename: 'AR Account Mapping',
          title: 'AR Account Mapping'
      },
              'print'
        ],
    "paging": false,
    select: 'single',
    "scrollY": "550px",
    scrollCollapse: true,
    "pageLength": 50,
       data: ownerData,
       "columns": [
    { "data": "ACCOUNT_NO",orderData: [ 1,2 ] },
    { "data": "ACCOUNT_NAME" },
     { "data": "EID" },
    { "data": "INSERT_DATE" },
    { "data": "INSERT_USER" },
    { "data": "UPDATE_DATE" },
    { "data": "UPDATE_USER" }

  ],



    } );

    $("#ownersTable_filter input").addClass( "form-control" );
$("#ownersTable_filter input").attr("placeholder","Search");


}
)
.done(function(msg) {
          reportLoading.style.display = 'none';
        })
.fail(function(xhr, status, error) {
          reportLoading.style.display = 'none';
        })



}

$('#ownersTable').on('dblclick','tr',function(e){
  $('#ownersTable').DataTable().row(this).select();
   var $element = $('#ownersTable').DataTable().row( this ).data() ;
      ownerAccountNo = $element.ACCOUNT_NO ;
      ownerAccountName = $element.ACCOUNT_NAME ;
      ownerEID = $element.EID ;
      $('#ownerChangeModal').modal('show');
    })




$("#addOwnerBtn").on('click',function() {
    console.log("add new record");
    $('#ownersAddModal').modal('show');
  });


    $('#ownerChangeModal').on('show.bs.modal', function (event) {
      var modal = $(this)
      $("#OWNER_ACCOUNT").val(ownerAccountNo);
      $("#OWNER_EID").val(ownerEID);
      $("#OWNER_ACCOUNT_NAME").val(ownerAccountName);

    })


    $('#ownerChangeModalBtn').click(function () {
      var ownerArray = $("#ownerChangeModalForm").serializeArray();
      console.log(ownerArray);
      if ($("#OWNER_ACCOUNT").val().length > 0 &&  $("#OWNER_EID").val().length > 0 && $("#OWNER_ACCOUNT_NAME").val().length > 0  ) {
        $.post( "https://marriottssc.secure-app.co.za/updateOwners",
        $("#ownerChangeModalForm").serializeArray() ,
        function(data) {
          console.log(data);
        }
        )
        .done(function( msg ) {
          console.log(msg);
          location.reload();
        })
        .fail(function(error) {
          alert("Owner could not be added. Please contact administrator" + error);
        });
      } else {
        alert("Please complete all the required fields!");
      }



      });

      $('#ownerDeleteModalBtn').click(function () {
        $('#deleteModal').modal('show');
        });

        $('#deleteOwnerConfirmBtn').click(function () {
          var ownerArray = $("#ownerChangeModalForm").serializeArray();
          console.log(ownerArray);
           console.log("Delete owners");
            $.post( "https://marriottssc.secure-app.co.za/deleteOwners",
            $("#ownerChangeModalForm").serializeArray() ,
            function(data) {
              console.log(data);
            }
            )
            .done(function( msg ) {
              console.log(msg);
              location.reload();
            })
            .fail(function(error) {
              alert("Owner could not be deleted. Please contact administrator" + error);
            });
          });




      $('#ownersAddModalBtn').click(function () {
        var ownerArray = $("#ownersAddModalForm").serializeArray();
        console.log(ownerArray);
        if ($("#OWNER_ACCOUNT").val().length > 0 &&  $("#OWNER_EID").val().length > 0 && $("#OWNER_ACCOUNT_NAME").val().length > 0  ) {
         console.log("Add owner");
          $.post( "https://marriottssc.secure-app.co.za/addOwners",
          $("#ownersAddModalForm").serializeArray() ,
          function(data) {
            console.log(data);
          }
          )
          .done(function( msg ) {
            console.log(msg);
            location.reload();
          })
          .fail(function(error) {
            alert("Owners could not be added. Please contact administrator" + error);
          });
        } else {
          alert("Please complete all the required fields!");
        }


        });


