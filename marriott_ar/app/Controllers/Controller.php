<?php

namespace App\Controllers;

use App\Models\HConnect;
use App\Models\HConnectv2;
use App\Models\HConnectError;
use Interop\Container\ContainerInterface;

abstract class Controller
{

    protected $c;

    public function __construct(ContainerInterface $c){

        $this->c = $c;

	}


	/**
 * Consumes the HCONNECT service
 * @param string $resource The resource identifier
 * @param array $parameters
 * @param type $method HTTP method
 * @return mixed Array of data or instance of HConnectError
 */
function consumeHConnect($resource, array $parameters = array(), $method = NULL) {
	$service = new HConnect($this->c->settings['hconnect']['HCONNECT_HOST'], $this->c->settings['hconnect']['HCONNECT_USER'], $this->c->settings['hconnect']['HCONNECT_PASS'], $resource, $parameters, $method);
	$data = $service->consume();
	if ($service->lastResponseIsError()) {
		return new HConnectError($data);
	}
	return $data;
}

    	/**
	 * Consumes an HConnect resource based on application configuration
	 * and returns the data or FALSE in case of an error.
	 * @param string $resource HConnect resource
	 * @param array $parameters Array of resource parameters, only used for POST-method resources
	 * @param type $errorData Reference to an array variable that will be populated with the error information if an error was returned
	 * @param type $cacheIdentifier If supplied, the resources data will be retrieved from (and updated to) the application cache
	 * @param type $cacheLifeTime The cache lifetime (defaults to one day)
	 * @return mixed
	 */
	public function consumeHConnectv1($resource, array $parameters = array(), &$errorData = NULL, $cacheIdentifier = NULL, $cacheLifeTime = 86400) {
		$errorData = NULL;
		$service = new HConnect($resource, 'onlinebookings', 'I300k1ng5', $parameters, 'http://197.85.184.59');
		if ($cacheIdentifier) {
			$cache = $this->cache->getCache($cacheIdentifier);
			$cacheData = $cache->get();
			if ($cache->isMiss()) {
				$cacheData = $service->consume();
				if ($service->lastResponseIsError()) {
					$errorData = $cacheData;
				} elseif ($cacheData !== FALSE) {
					$cache->set($cacheData, $cacheLifeTime);
				}
			}
			return $cacheData;
		}
		$data = $service->consume();
		if ($service->lastResponseIsError()) {
			$errorData = $data;
		}
		return $data;
    }

}




?>