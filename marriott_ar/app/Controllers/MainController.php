<?php

namespace App\Controllers;

Use PDO;
use App\Models\HConnect;
use App\Models\HConnectv2;
use App\Models\HConnectError;

class MainController extends Controller
{



    public function signin($request, $response, $args) {
        return $this->c->view->render($response, 'signin.twig');
		}
/**
 * Authenticates a user
 * @param string $username
 * @param string $password
 * @return mixed \HConnectError or array
 */
function login($request, $response, $args) {

	$params = array(
		'username' => $request->getParam('user'),
		'password' => $request->getParam('pass')
	);
	if ($request->getParam('raw')) {
		$params['_raw'] = 1;
	}
	$payload =$this->consumeHConnect('user/login.json', $params);
	$this->c->logger->addInfo('user/login payload: ' . json_encode($payload) );
	// Persist user credentials in session
	if (session_id() && (!$payload instanceof HConnectError) && !empty($payload['user'])) {
		$_SESSION['user'] = $request->getParam('user');
		$_SESSION['userID']= $payload['user'];
		$hotelAccess = $this->hasAccess($_SESSION['user'],'HOTEL');
		if ($hotelAccess  == 'Y') {
			return $response->withRedirect($this->c->router->pathFor('aging_published'));
		} else{
			return $response->withRedirect($this->c->router->pathFor('level1'));
		}

	} else {
		if ($payload->hasIdentifier()) {
			$errorInfo = $payload->getIdentifier();
		}
		$this->c->flash->addMessage('invalidLogin','Log in failed. ' . $payload->getMessage() . " - " .$errorInfo);
		return $response->withRedirect($this->c->router->pathFor('signin'));
	}
// var_dump($payload);
	return $payload;

}


		/**
 * Updates a user's account password
 * @param string $username
 * @param string $password
 * @param string $newpassword
 * @return mixed \HConnectError or array
 */
function updateUserPassword($request, $response, $args) {
	$params = array(
		'username' => $request->getParam('user'),
		'password' => $request->getParam('pass'),
		'newpassword' => $request->getParam('newpass'),
	);
	$payload = $this->consumeHConnect('user/updatepassword.json', $params);
	$errorInfo ="";
	var_dump($payload);
	if ( (!$payload instanceof HConnectError) && !empty($payload['username'])) {
		$this->c->flash->addMessage('changedPassword','Password changed successfully. Log in with new password.');
		return $response->withRedirect($this->c->router->pathFor('signin'));
	} else {

		if ($payload->hasIdentifier()) {
			$errorInfo = $payload->getIdentifier();
		}
		$this->c->flash->addMessage('changedPasswordFail',$payload->getMessage() . " - " .$errorInfo );
		return $response->withRedirect($this->c->router->pathFor('changepassword'));
	}
	return $payload;
}

function addFlash($flash,$msg) {
	$this->c->flash->addMessage($flash,$msg);
}



  //   public function login($request, $response, $args) {
	// 	$params =  array('username' => $request->getParam('user'));
	// 	$params['password'] = $request->getParam('pass');
	// 	// $cacheIdentifier = self::_CACHE_IDENT_USER.$user;
	// 	$auth = $this->consumeHConnect('/user/login.json', $params, $exception, $cacheIdentifier, $cacheLifeTime);
	// 	if (!$exception) {
	// 		if ($auth['user']) {
	// 			// var_dump($auth['user']);
	// 			// die();
	// 			$_SESSION['userID'] = $auth['user'];
	// 			$_SESSION['user'] = $request->getParam('user');
	// 			return $response->withRedirect($this->c->router->pathFor('level1'));
	// 		} else {
	// 			// $this->c->logger->addInfo('Login failed for username' . $request->getParam('user') );
	// 			return $response->withRedirect($this->c->router->pathFor('signin'));
	// 		}
	// 	} else {
	// 		return $response->withRedirect($this->c->router->pathFor('signin'));
	// 	}
	// 	return null;
	// }


	public function logout($request, $response, $args){
				// remove all session variables
		session_unset();

		// destroy the session
		session_destroy();
		return $response->withRedirect($this->c->router->pathFor('signin'));
	}

	public function uploadfile($request, $response, $args){

		$errors = [];
		$path = 'uploads/mapping/';
		$extensions = ['pdf'];
		$file_name = $_FILES['files']['name'][0];
		$file_tmp = $_FILES['files']['tmp_name'][0];
		$file_type = $_FILES['files']['type'][0];
		$file_size = $_FILES['files']['size'][0];
		$file_ext = strtolower(end(explode('.', $_FILES['files']['name'][0])));
		$file = $path . $file_name;
		if (!in_array($file_ext, $extensions)) {
    		$errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
		}

		if ($file_size > 2097152) {
		    $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
		}
		if (empty($errors)) {
		    move_uploaded_file($file_tmp, $file);
		}
		if ($errors) print_r($errors);
		// return $response->withRedirect($this->c->router->pathFor('uploadfile'));

	}



	function hasAccess($eid,$function) {
		$sql = "select EID from AR_EID_ACCESS
		where function = :function
		and active_yn = 'Y'
		and UPPER(eid) = UPPER(:eid)  ";
		$stmt = $this->c->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$stmt->bindParam(':eid', $eid, PDO::PARAM_STR);
		$stmt->bindParam(':function', $function, PDO::PARAM_STR);
		$stmt->execute();
		$access = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$accessYN = $access ? 'Y' : 'N';
		return $accessYN ;

	}

	function getHotel($eid) {
		// $sql = "SELECT C.DESCRIPTION HOTEL_CODE,D.DESCRIPTION HOTEL_DESC FROM
		// AR_CONFIGURATION C
		// INNER JOIN AR_CONFIGURATION D
		// ON (C.DESCRIPTION = D.CODE
		// AND D.CONFIG_TYPE = 'RESORT_NAME')
		// WHERE C.CONFIG_TYPE = 'HOTEL_ACCESS'
		// AND lower(C.CODE) = lower(:EID)  ";
		$sql = "select A.RESORT HOTEL_CODE,B.DESCRIPTION HOTEL_DESC
        from ar_resort_access A
        INNER JOIN ar_configuration B
        ON(A.RESORT =B.CODE
        AND B.CONFIG_TYPE = 'RESORT_NAME')
        WHERE LOWER(A.EID) = LOWER(:EID)";
		$stmt = $this->c->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$stmt->bindParam(':EID', $eid, PDO::PARAM_STR);
		$stmt->execute();
		$access = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
		return $access ;

	}

	function getHotelTest($eid) {
		// $sql = "SELECT C.DESCRIPTION HOTEL_CODE,D.DESCRIPTION HOTEL_DESC FROM
		// AR_CONFIGURATION C
		// INNER JOIN AR_CONFIGURATION D
		// ON (C.DESCRIPTION = D.CODE
		// AND D.CONFIG_TYPE = 'RESORT_NAME')
		// WHERE C.CONFIG_TYPE = 'HOTEL_ACCESS'
		// AND lower(C.CODE) = lower(:EID)  ";
		$sql = "select A.RESORT HOTEL_CODE,B.DESCRIPTION HOTEL_DESC
        from ar_resort_access A
        INNER JOIN ar_configuration B
        ON(A.RESORT =B.CODE
        AND B.CONFIG_TYPE = 'RESORT_NAME')
        WHERE LOWER(A.EID) = LOWER(:EID)";
		$stmt = $this->c->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$stmt->bindParam(':EID', $eid, PDO::PARAM_STR);
		$stmt->execute();
		$access = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $access ;

	}


}


?>