<?php


namespace App\Models;
/**
 * @name HConnectError
 * @desc HCONNECT API Error Wrapper
 * @author Werner Mollentze <support@hsolutions.co.za>
 */

class HConnectError {

	/**
	 * @var array
	 */
	private $data = array();

	/**
	 * @var int
	 */
	private $code = 0;

	/**
	 * @var string
	 */
	private $message = 0;

	/**
	 * Constructor
	 * @param array $errorData Error object returned from HConnect service
	 */
	public function __construct($errorData) {
		$this->data = $errorData;
		if (!empty($errorData['code'])) {
			$this->code = $errorData['code'];
		}
		if (!empty($errorData['message'])) {
			$this->message = $errorData['message'];
		}
	}

	/**
	 * Retrieves the error code
	 * @return int
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * Retrieves the error message
	 * @return string
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * Whether the error is a user (4**) error
	 * @return bool
	 */
	public function isUserError() {
		return $this->code > 399 && $this->code < 500;
	}

	/**
	 * Whether the error is a server (5**) error
	 * @return bool
	 */
	public function isServerError() {
		return $this->code > 499 && $this->code < 600;
	}

	/**
	 * Whether the error is a parameter error
	 * @return bool
	 */
	public function isParameterError() {
		$data = $this->data;
		return !empty($data['error']['parameters']);
	}

	/**
	 * Whether the error contains a remote system error identifier
	 * @return bool
	 */
	public function hasIdentifier() {
		$data = $this->data;
		return !empty($data['error']['identifier']);
	}

	/**
	 * Retrieves the remote system error identifier
	 * @return string
	 */
	public function getIdentifier() {
		$data = $this->data;
		if(!empty($data['error']['identifier'])) {
			return $data['error']['identifier'];
		}
		return NULL;
	}


}