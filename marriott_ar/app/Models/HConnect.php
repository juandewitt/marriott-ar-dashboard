<?php

namespace App\Models;
/**
 * @name HConnect
 * @desc Interface to the HCONNECT API
 * @author Werner Mollentze <support@hsolutions.co.za>
 */

class HConnect {

	/**
	 * @var string
	 */
	public $service = 'http://hconnect.hsolutions.co.za';


	/**
	 * @var string
	 */
	public $username = NULL;

	/**
	 * @var string
	 */
	public $password = NULL;

	/**
	 * @var string
	 */
	public $resource = NULL;

	/**
	 * @var array
	 */
	public $parameters = array();


	/**
	 * @var mixed
	 */
	public $lastResponse = NULL;

	/**
	 * @var string
	 */
	public $method = NULL;

	/**
	 * Create a new HCONNECT resource consumption object
	 * @param string $service The HCONNECT service URL
	 * @param string $resource The HCONNECT resource identifier
	 * @param string $username HCONNECT API account username
	 * @param string $password HCONNECT API account password
	 * @param array $parameters Resource parameters (for POST-method resources)
	 * @param string $method Request method
	 * @return \HConnect
	 */
	public function __construct($service, $username = NULL, $password = NULL, $resource = NULL, array $parameters = array(), $method = NULL) {
		$this->service = $service;
		if ($username) {
			$this->username = $username;
		}
		if ($password) {
			$this->password = $password;
		}
		if ($resource) {
			$this->resource = $resource;
		}
		if ($parameters) {
			$this->parameters = $parameters;
		}
		if (!empty($method)) {
			$this->method = $method;
		}
		return $this;
	}

	/**
	 * Consumes the specific HCONNECT resource and returns
	 * either the response data as an array or FALSE in case
	 * of failure.
	 * @return mixed
	 */

	public function consume() {

		$url = trim($this->service, '/') . '/' . trim($this->resource, '/');

		$headers[] = "Content-Type: application/json; charset=utf-8";
		$headers[] = "Authorization: Basic " . base64_encode("{$this->username}:{$this->password}");
		$header = implode("\r\n", $headers) . "\r\n";

		// Construct the required request parameters. If no parameters are
		// supplied, a $_GET method is assumed.

		if ($this->method) {
			$method = $this->method;
		} else {
			if ($this->parameters) {
				$method = 'POST';
			} else {
				$method = 'GET';
			}
		}

		$contextHttp = array(
			'method'  => $method,
			'header'  => $header,
			'timeout' => 60,
			'ignore_errors' => TRUE // To ensure error responses are returned
		);

		if ($this->parameters) {
			$content = json_encode($this->parameters);
			$contextHttp['content'] = $content;
		}

		$context = stream_context_create(array(
			'http' => $contextHttp
		));

		$response = @file_get_contents($url, FALSE, $context);

		$this->lastResponse = $response;
		if (!empty($http_response_header)) {
			$this->lastResponseHeaders = $http_response_header;
		}

		if ($response) {
			if ($responseData = @json_decode($response, TRUE)) {
				return $responseData;
			}
		}

		return FALSE;

	}

	/**
	 * Determines whether the last response was an error response
	 * @return bool
	 */
	public function lastResponseIsError() {
		$responseStatus = NULL;
		if (!empty($this->lastResponseHeaders)) {
			foreach ($this->lastResponseHeaders as $responseHeader) {
				$responseHeaderParts = explode(' ', $responseHeader);
				if ($responseHeaderParts[0] == 'HTTP/1.1') {
					$responseStatus = (int) $responseHeaderParts[1];
					break;
				}
			}
		}
		return $responseStatus != 200;
	}


}